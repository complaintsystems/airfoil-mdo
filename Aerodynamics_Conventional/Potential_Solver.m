% Two-dimensional potential flow solver


function [cl,cd,cmac,xp,yp,cp,cpx,cpy,XB,YB,COSINE] = Potential_Solver(p,alpha)


% Obtain values from PARSEC

[XB, YB] = Airfoil_Paramterization(p);
XB = flip(XB);
YB = flip(YB);
XB = XB';
YB = YB';


ALPHA = alpha;

     
% Execute flow solver

[cl,cd,cmac,xp,yp,cp,cpx,cpy,COSINE]=solver(XB,YB,ALPHA);
figure
plot(xp,cp,'lineWidth',2)
xlabel 'X/C'
ylabel 'Cp'
grid minor

% Transpose coordinates for structural analysis

XB = XB';
YB = YB';






  

  
 

 


