%%Example: (NACA0012 airfoil)
tic

     clear all
     close all
     clc
     global X_max;
     global X_min;
     
     global Y_max;
     global Y_min;
     
     disp('Meshing routine initialized')
     
%      P:        Node positions (Nx2)
%      T:         Triangle indices (NTx3)
%      FD:        Distance function d(x,y)
%      FH:        Scaled edge length function h(x,y)
%      H0:        Initial edge length
%      BBOX:      Bounding box [xmin,ymin; xmax,ymax]
%      PFIX:      Fixed node positions (NFIXx2)
%      FPARAMS:   Additional parameters passed to FD and FH

     
     %% SOurce NACA0012 code
 
%----------------------------------------------------------------------------------------%     
%      hlead=0.01; htrail=0.04; hmax=2; circx=2; circr=4;
%      a=.12/.2*[0.2969,-0.1260,-0.3516,0.2843,-0.1036];
% 
%      fd=@(p) ddiff(dcircle(p,circx,0,circr),(abs(p(:,2))-polyval([a(5:-1:2),0],p(:,1))).^2-a(1)^2*p(:,1));
%      fh=@(p) min(min(hlead+0.3*dcircle(p,0,0,0),htrail+0.3*dcircle(p,1,0,0)),hmax);
% 
%      fixx=1-htrail*cumsum(1.3.^(0:4)');
%      fixy=a(1)*sqrt(fixx)+polyval([a(5:-1:2),0],fixx);
%      fix=[[circx+[-1,1,0,0]*circr; 0,0,circr*[-1,1]]'; 0,0; 1,0; fixx,fixy; fixx,-fixy];
%      box=[circx-circr,-circr; circx+circr,circr]; [x_min, y_min; x_max,
%      y_max]
%      h0=min([hlead,htrail,hmax]);
% 
%      [p,t]=distmesh2d(fd,fh,h0,box,fix);

%----------------------------------------------------------------------------------------%

     %% NACA 0012 internal mesh : test routine :: Successful!    
     hlead=0.005; htrail=0.01; hmax=2; circx=1; circr=1;
     
%      P=
%      [0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];
%      0012
%     [0.0146    0.2968    0.0889   -0.4928    0.3016    0.0311   -0.4812   -0.0039    0.0101   -2.7791    9.2496]

     P= [0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];
    a = parsec(P);
  
        fd  = @(p) ddiff(distance(p),dcircle(p,0.25,0.01,0.02));

     fixx=1-htrail*cumsum(1.3.^(0:4)');
     
     
     fixyp = (a(2)*fixx.^(1.5)+a(3)*fixx.^(2.5)+a(4)*fixx.^(3.5)+a(5)*fixx.^(4.5)+a(6)*fixx.^(5.5))...
                +a(1)*fixx.^0.5;
     fixym = -(a(7)*fixx.^0.5+a(8)*fixx.^(1.5)+a(9)*fixx.^(2.5)+a(10)*fixx.^(3.5)+a(11)*fixx.^(4.5)+a(12)*fixx.^(5.5));
%      fixy = polyval([a(6)+a(12),a(5)+a(11),a(4)+a(10),a(3)+a(9),a(2)+a(8),0],...
%                 fixx).*sqrt(fixx)+(a(1)+a(7))*sqrt(fixx);
%      fixy=a(1)*sqrt(fixx)+polyval([a(5:-1:2),0],fixx);
     
     
     fix=[[circx+[-1,1,0,0]*circr; 0,0,circr*[-1,1]]'; 0,0; 1,0; fixx,fixyp; fixx,fixym];
     box=[circx-circr,-circr; circx+circr,circr];
     h0=min([hlead,htrail,hmax]);
     
     [p,t]=distmesh2d(fd,@huniform,0.01,box,fix);
      

     disp('Meshing routine complete')
  
     toc
     