Distmesh2D is an implicit solver that uses a signed distance function to define the location of interior nodes and a delaunay triangulation scheme to generate the triangles. 

The OML is defined using an explicit function (PARSEC polynomial). This is defined and solved on line 32. 

More information on this mesh generation tool can be found here : http://webcache.googleusercontent.com/search?q=cache:https://popersson.github.io/distmesh/


%      N:         Number of nodes
%      P:         Node positions (Nx2)
%      T:         Triangle indices (NTx3)
%      FD:        Distance function d(x,y)
%      FH:        Scaled edge length function h(x,y)
%      H0:        Initial edge length
%      BBOX:      Bounding box [xmin,ymin; xmax,ymax]
%      PFIX:      Fixed node positions (NFIXx2)
%      FPARAMS:   Additional parameters passed to FD and FH

Look at example : NACA0012 airfoil. The exterior finite element mesh is convered to a mesh with internal triangles in the current version of code on the repo. 

OBJECTIVE --> Define a polynomial that the implicit solver can input and generate a mesh.
