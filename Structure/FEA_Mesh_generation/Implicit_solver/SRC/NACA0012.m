%%Example: (NACA0012 airfoil)
tic

     clear all
     close all
     clc
     global X_max;
     global X_min;
     
     global Y_max;
     global Y_min;
     
     disp('Meshing routine initialized')
     
%      P:        Node positions (Nx2)
%      T:         Triangle indices (NTx3)
%      FD:        Distance function d(x,y)
%      FH:        Scaled edge length function h(x,y)
%      H0:        Initial edge length
%      BBOX:      Bounding box [xmin,ymin; xmax,ymax]
%      PFIX:      Fixed node positions (NFIXx2)
%      FPARAMS:   Additional parameters passed to FD and FH

     
     %% SOurce NACA0012 code
 
%----------------------------------------------------------------------------------------%     
%      hlead=0.01; htrail=0.04; hmax=2; circx=2; circr=4;
%      a=.12/.2*[0.2969,-0.1260,-0.3516,0.2843,-0.1036];
% 
%      fd=@(p) ddiff(dcircle(p,circx,0,circr),(abs(p(:,2))-polyval([a(5:-1:2),0],p(:,1))).^2-a(1)^2*p(:,1));
%      fh=@(p) min(min(hlead+0.3*dcircle(p,0,0,0),htrail+0.3*dcircle(p,1,0,0)),hmax);
% 
%      fixx=1-htrail*cumsum(1.3.^(0:4)');
%      fixy=a(1)*sqrt(fixx)+polyval([a(5:-1:2),0],fixx);
%      fix=[[circx+[-1,1,0,0]*circr; 0,0,circr*[-1,1]]'; 0,0; 1,0; fixx,fixy; fixx,-fixy];
%      box=[circx-circr,-circr; circx+circr,circr]; [x_min, y_min; x_max,
%      y_max]
%      h0=min([hlead,htrail,hmax]);
% 
%      [p,t]=distmesh2d(fd,fh,h0,box,fix);

%----------------------------------------------------------------------------------------%

     %% NACA 0012 internal mesh : test routine :: Successful!    
     hlead=0.005; htrail=0.01; hmax=2; circx=2; circr=4;
     
     
     a=.12/.2*[0.2969,-0.1260,-0.3516,0.2843,-0.1036];
   
     
   %fd  = @(p) ddiff((abs(p(:,2))-polyval([a(5:-1:2),0],p(:,1))).^2-a(1)^2*p(:,1),drectangle(p, 0.25,0.35,-0.02,0.02));
   
  fd  = @(p) ddiff((abs(p(:,2))-polyval([a(5:-1:2),0],p(:,1))).^2-a(1)^2*p(:,1),dcircle(p,0.25,0,0.05));
  % fh=@(p) min(min(hlead+0.3*dcircle(p,0,0,0),htrail+0.3*dcircle(p,1,0,0)),hmax);
     
     fixx=1-htrail*cumsum(1.3.^(0:4)');
     fixy=a(1)*sqrt(fixx)+polyval([a(5:-1:2),0],fixx);
     fix=[[circx+[-1,1,0,0]*circr; 0,0,circr*[-1,1]]'; 0,0; 1,0; fixx,fixy; fixx,-fixy];
     box=[circx-circr,-circr; circx+circr,circr];
     h0=min([hlead,htrail,hmax]);
     
     [p,t]=distmesh2d(fd,@huniform,0.01,box,fix);
      

     disp('Meshing routine complete')
  
     toc
     