% Script to generate uniform mesh on unit circle

% efault syntax

%function [p,t]  =distmesh2d(fd,fh,h0,bbox,pfix,varargin)

%      P:         Node positions (Nx2)
%      T:         Triangle indices (NTx3)
%      FD:        Distance function d(x,y)
%      FH:        Scaled edge length function h(x,y)
%      H0:        Initial edge length
%      BBOX:      Bounding box [xmin,ymin; xmax,ymax]
%      PFIX:      Fixed node positions (NFIXx2)
%      FPARAMS:   Additional parameters passed to FD and FH


%%   Example: (Uniform Mesh on Unit Circle)

clear all
close all
clc

disp('Mesh generation initialized')
 
fd=@(p) sqrt(sum(p.^2,2))-1;
[p,t]=distmesh2d(fd,@huniform,0.05,[-1,-1;1,1],[]);
            
disp('Mesh generation complete!')

