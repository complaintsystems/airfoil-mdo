
    close all
    clear all
    clc
    
    libpath();
    
    %% Nomenclature
    
    %{
    
    tria :: Node connectivity matri
    vert :: Node coordinates
    %}

    


%% Assign file path to external libraries

    filename = mfilename('fullpath');
    filepath = fileparts( filename );
%% Assign mesh file <name>.msh
    meshfile = ...
        [filepath,'/poly-data/airfoil.txt'];

%% Use triread.m to extract node and edge information    
    
   [node,edge] = triread( meshfile );
 
%% Sizing function

olfs.dhdx = +0.15;
 
   [vlfs,tlfs, ...
    hlfs] = lfshfn2(node,edge, ...
                    []  ,olfs) ;
    
   [slfs] = idxtri2(vlfs,tlfs) ;
   
       figure(1);
    patch('faces',tlfs(:,1:3),'vertices',vlfs , ...
        'facevertexcdata' , hlfs, ...
        'facecolor','interp', ...
        'edgecolor','k') ;
    colormap jet
    colorbar('southoutside')
    hold on; axis image off;
    title(['MESH-SIZE: KIND=DELFRONT, |TRIA|=', ...
        num2str(size(tlfs,1))]) ;
 
%% Define triangle generation algorithm : DELFRONT
 hfun = @trihfn2;
   
   [vert,etri, ...
    tria,tnum] = refine2(node,edge,[],[],hfun , ...
                    vlfs,tlfs,slfs,hlfs);

%     opts.kind = 'delaunay';
%     opts.rho2 = +1.00 ;
%    
%    [vert,etri, ...
%     tria,tnum] = refine2(node,edge,[]  ,opts);


%% Mesh optimization/smoothing

 [vnew,enew, ...
    tnew,tnum] = smooth2(vert,etri,tria,tnum) ;


%% Plot Finite element mesh
    figure(2)
    patch('faces',tria(:,1:3),'vertices',vert, ...
        'facecolor','w', ...
        'edgecolor',[.2,.2,.2]) ;
    hold on; axis image off;
    patch('faces',edge(:,1:2),'vertices',node, ...
        'facecolor','w', ...
        'edgecolor',[.1,.1,.1], ...
        'linewidth',1.5) ;
    hold on; axis image off;
    title(['TRIA-MESH: KIND=DELFRONT, |TRIA|=', ...
        num2str(size(tria,1))]) ;
    
    figure(3)
    
     patch('faces',tnew(:,1:3),'vertices',vnew, ...
        'facecolor','w', ...
        'edgecolor',[.2,.2,.2]) ;
    hold on; axis image off;
    patch('faces',edge(:,1:2),'vertices',node, ...
        'facecolor','w', ...
        'edgecolor',[.1,.1,.1], ...
        'linewidth',1.5) ;
    title(['MESH-OPT.: KIND=DELFRONT, |TRIA|=', ...
        num2str(size(tnew,1))]) ;
    
    hvrt = trihfn2(vert,vlfs,tlfs,slfs,hlfs) ;
    hnew = trihfn2(vnew,vlfs,tlfs,slfs,hlfs) ;
    
    tricost(vert,etri,tria,tnum,hvrt) ;
    tricost(vnew,enew,tnew,tnum,hnew) ;    
    drawnow;
 %-----------------------------------------------------------------------%
 %#######################################################################%
  %-----------------------------------------------------------------------%
 
% %% Define triangle generation algorithm : Delfront
%     opts.kind = 'delfront';
%     opts.rho2 = +1.00 ;
%    
%    [vert,etri, ...
%     tria,tnum] = refine2(node,edge,[]  ,opts) ;
% 
% 
% %% Plot finite element mesh
%     figure;
%     patch('faces',tria(:,1:3),'vertices',vert, ...
%         'facecolor','w', ...
%         'edgecolor',[.2,.2,.2]) ;
%     hold on; axis image off;
%     patch('faces',edge(:,1:2),'vertices',node, ...
%         'facecolor','w', ...
%         'edgecolor',[.1,.1,.1], ...
%         'linewidth',1.5) ;
%     title(['TRIA-MESH: KIND=DELFRONT, |TRIA|=', ...
%         num2str(size(tria,1))]) ;
%     
%     drawnow;
%     
%     set(figure(1),'units','normalized', ...
%         'position',[.05,.50,.30,.35]) ;
%     set(figure(2),'units','normalized', ...
%         'position',[.35,.50,.30,.35]) ;





