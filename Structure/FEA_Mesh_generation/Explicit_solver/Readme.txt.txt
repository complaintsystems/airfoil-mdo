This is an explicit mesh generation tool that requires a set of X and Y coordinates that define the OML. 
In addition to this, edge informaion for these coordinates is also required. Once these coordinates and
their respective edge information is provided, tool uses a triangulation scheme ( Delaunay or Delfront )
to generate inner triangles. 

Code description --->

tridemo :: Main executable for creating mesh

<name>.msh :: nesh file with OML coordinates and edge information

triread :: Extracts the OML node locaion coordinates and edge information

Note --> The code generates the mesh using both Delfront and Delaunay methods. Suppress one for convienience at this stage. 

OBJECTIVE >> Modify .msh file in a way that optimized airfoil coordinates and their edge locations are read from the .msh file. 