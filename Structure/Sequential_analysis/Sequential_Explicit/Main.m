clear all
clc 
close all

addpath('poly-data') 


%% Perform Airfoil shape optimization
CDS = main_optimizer;

%% Perform FEA mesh generation

% Define OML nodes
nodeGen(CDS);
% Generate mesh
[T, p] = meshGen; 



