function [f,g] = func(parameters)
cl_star = 0.4428
cd_star = 0.00619

CC = interface(parameters);      % CC = [CL,CD]
G = zeros(1,2);
g = zeros(1,4);
f = CC(1)/(cd_star)                                                         % minimise f at fixed CD
for i = 1:4
    I = [parameters,i];
G = grad(I);
G = (G-CC)/0.0001;
end
%d_cddx = g(1);
%d_cldx = g(2);
pause(3)
end