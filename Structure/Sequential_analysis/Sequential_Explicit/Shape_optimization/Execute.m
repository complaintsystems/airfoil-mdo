%% Script to compute NACA0012 airfoil coordinates using PARSEC


clear all
clc
close all



%% Define native NACA0012 PARSEC paramteters 
p=  [0.0146    0.2968    0.0889   -0.4928    0.3016    0.0311   -0.4812   -0.0039    0.0101   -2.7791    9.2496]


% Compute shape coefficients

a = parsec(p);

point=100;
n=(1/point);

% Enable higher resolution at LE
nn=n/5; 

% suction surface coordinates
x1=1:-n:0.1;
x2=0.1:-nn:0;

% pressure surface coordinates

x3=0:nn:0.1;
x4=0.1:n:1;

xu(1:length(x1))=x1;
xu(length(x1)+1:length(x1)+length(x2))=x2;

xl(1:length(x3))=x3;
xl(length(x3)+1:length(x3)+length(x4))=x4;

X(1:length(xu))=xu;
X(length(xu)+1:length(xu)+length(xl))=xl;

X=X';

yu=a(1)*xu.^.5+a(2)*xu.^(1.5)+a(3)*xu.^(2.5)+a(4)*xu.^(3.5)+a(5)*xu.^(4.5)+a(6)*xu.^(5.5);
yl=-(a(7)*xl.^.5+a(8)*xl.^(1.5)+a(9)*xl.^(2.5)+a(10)*xl.^(3.5)+a(11)*xl.^(4.5)+a(12)*xl.^(5.5));

Y(1:length(yu),1)=yu;
Y(length(yu)+1:length(yu)+length(yl),1)=yl;

%% Round X & Y coordinates to 'nice' values


Y=round(Y,7);
X=round(X,7);

Coord = [X Y];

plot(X,Y, 'lineWidth', 2)
axis equal
grid minor
xlabel 'X/C'
ylabel 'Y/C'
title 'NACA 0012 airfoil : PARSEC '


file = fopen('airfoil_coordinates.txt','w');
fprintf(file,'%d %d\n',Coord');
fclose(file);




