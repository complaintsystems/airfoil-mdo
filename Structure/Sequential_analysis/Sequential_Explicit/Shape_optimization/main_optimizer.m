function CDS = main_optimizer

clc
clear all
close all

% parameters=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];
 parameters = [0.0146,0.3025,0.06,-0.4928];

 
 %% Specify initial cl and cd values
CL0 = 0.5;
CD0 = 0.00651;


%options = optimoptions(@fmincon,'OutputFcn','outfun', ...
%    'Display','iter','Algorithm','interior-point','SpecifyObjectiveGradient',true,'PlotFcn', {@optimplotfval},'FunctionTolerance',1e-6);


options = optimoptions(@fmincon,'Algorithm','active-set','Display','iter');
options = optimoptions(options,'SpecifyObjectiveGradient',true);
options = optimoptions(options,'FunctionTolerance',1e-6,'PlotFcn', {@optimplotfval});% 


[xopt,fopt] = fmincon('func',parameters,[],[],[],[],[],[],[],options);

output = cdcl_calc(xopt);

CL = output(1);
CD = output(2);
fprintf('Cl = %f \n', CL)
fprintf('Cd = %f \n', CD)
cdsavings = 100*(1-(CD/CD0));
fprintf('Cd reduced by %f percent \n', cdsavings)


%% Specify optimal parsec parameters

p = [xopt 0.3016 0.06 -0.4848 -0.0039 0.0101 -2.7791 9.2496];


coord = airfoil_coord_calc(p);

LE = find(coord(:,1) ==0);
X_up = coord(1:LE(1),1);
Y_up = coord(1:LE(1),2);
X_low = coord(LE(1)+1:end,1);
Y_low = coord(LE(1)+1:end,2);

X_up  =flip(X_up);
Y_up = flip(Y_up);

X_low  =flip(X_low);
Y_low = flip(Y_low);

X = vertcat(X_up,X_low);
Y = vertcat(Y_up,Y_low);
Z = zeros(size(X));
CDS  =[X Y Z];
end
%% Outpit airfoil coordinates

%  airfoil_plot(xopt);
%  plots(xopt);
%  plotCP(xopt);

