%% Script to compute NACA0012 airfoil coordinates using PARSEC


%clear all
%clc
%close all



%% Define native NACA0012 PARSEC paramteters 
% Compute shape coefficients
function res = cdcl_calc(x)
x = [0.0146,0.3025    0.1017   -0.4928    0.3016    0.0600   -0.4848   -0.0039    0.0101   -2.7791    9.2496]
a = parsec(x);

point=100;
n=(1/point);

% Enable higher resolution at LE
nn=n/5; 

% suction surface coordinates
x1=1:-n:0.1;
x2=0.1:-nn:0;

% pressure surface coordinates

x3=0:nn:0.1;
x4=0.1:n:1;

xu(1:length(x1))=x1;
xu(length(x1)+1:length(x1)+length(x2))=x2;

xl(1:length(x3))=x3;
xl(length(x3)+1:length(x3)+length(x4))=x4;

X(1:length(xu))=xu;
X(length(xu)+1:length(xu)+length(xl))=xl;

X=X';
yu=a(1)*xu.^.5+a(2)*xu.^(1.5)+a(3)*xu.^(2.5)+a(4)*xu.^(3.5)+a(5)*xu.^(4.5)+a(6)*xu.^(5.5);
yl=-(a(7)*xl.^.5+a(8)*xl.^(1.5)+a(9)*xl.^(2.5)+a(10)*xl.^(3.5)+a(11)*xl.^(4.5)+a(12)*xl.^(5.5));

Y(1:length(yu),1)=yu;
Y(length(yu)+1:length(yu)+length(yl),1)=yl;

%% Round X & Y coordinates to 'nice' values


Y=round(Y,7);
X=round(X,7);

Coord = [X Y];

file = fopen('airfoil_coordinates.txt','w');
fprintf(file,'%d %d\n',Coord');
fclose(file);

%% Input airfoil coordinates into X-Foil to calculate Cl and Cd

re         = '3000000';                                                     % Reynold's number
iter       = '1000';                                                         % Number of iterations (min 200)
filename   = 'Save_Airfoil.txt';                                            % Name of output file
AoA        = '4';                                                          % Angle of Attack
aseq_step  = '1';

fid = fopen('xfoil_parsec_input.txt','w');
fprintf(fid,'load\n' );
fprintf(fid, 'airfoil_coordinates.txt \n');
% fprintf(fid,'ppar \n');
fprintf(fid,'OPER \n');
fprintf(fid,'OPER \n');
fprintf(fid,'iter \n');
fprintf(fid,'200 \n');
fprintf(fid,'visc \n');
   fprintf(fid,'3000000 \n');
      fprintf(fid,'pacc \n');
      fprintf(fid,[filename '\n']);
      fprintf(fid,'  \n');
  fprintf(fid,'Cl \n');
  fprintf(fid,'0.5\n');


% Close file
fclose(fid);

% Run XFoil using input file
cmd = 'xfoil.exe < xfoil_parsec_input.txt';
[status,result] = system(cmd);

outputfile = 'Save_Airfoil.txt';                                              
fidCP = fopen(outputfile);                                                 
dataBuffer = textscan(fidCP,'%f %f %f','HeaderLines',12,...                 
                            'CollectOutput',1,...
                            'Delimiter','');
fclose(fidCP); 

%% Separate and display data

AoA = dataBuffer{1,1}(:,1);
CL  = dataBuffer{1,1}(:,2);
CD  = dataBuffer{1,1}(:,3);
AoA = AoA(1,1);
CL = CL(1,1);
CD = CD(1,1);
res = [CL,CD];
%% delete text files used during script (COMMENT OUT THESE LINES IF YOU NEED TO TROUBLESHOOT THE TEXT FILES)

delete('airfoil_coordinates.txt')
delete('Save_Airfoil.txt')
delete('xfoil_parsec_input.txt')
end 

