% Script to generate uniform mesh on unit circle with hole 

% efault syntax

%function [p,t]  =distmesh2d(fd,fh,h0,bbox,pfix,varargin)

%      P:         Node positions (Nx2)
%      T:         Triangle indices (NTx3)
%      FD:        Distance function d(x,y)
%      FH:        Scaled edge length function h(x,y)
%      H0:        Initial edge length
%      BBOX:      Bounding box [xmin,ymin; xmax,ymax]
%      PFIX:      Fixed node positions (NFIXx2)
%      FPARAMS:   Additional parameters passed to FD and FH


%%   Example: (Uniform Mesh on Unit Circle)

clear all
close all
clc

disp('Mesh generation initialized')
 
%fd = @(p) (-0.3 + abs(0.4-sqrt(sum(p.^2,2))));

% f :: Defines the OML geometry

fd = @(p) ddiff(dcircle(p,0,0,1),dcircle(p,0,0,0.9));

[p,t] = distmesh2d(fd,@huniform,0.02,[-1,-1;1,1],[]);

      
disp('Mesh generation complete!')

