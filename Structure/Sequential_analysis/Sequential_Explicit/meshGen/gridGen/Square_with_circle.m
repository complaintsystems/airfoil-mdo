% Script to generate uniform mesh on a square with hole

% efault syntax

%function [p,t]  =distmesh2d(fd,fh,h0,bbox,pfix,varargin)

%      P:         Node positions (Nx2)
%      T:         Triangle indices (NTx3)
%      FD:        Distance function d(x,y)
%      FH:        Scaled edge length function h(x,y)
%      H0:        Initial edge length
%      BBOX:      Bounding box [xmin,ymin; xmax,ymax]
%      PFIX:      Fixed node positions (NFIXx2)
%      FPARAMS:   Additional parameters passed to FD and FH


%%   Example: (Uniform Mesh on Unit Circle)

clear all
close all
clc

disp('Mesh generation initialized')
 
fd = @(p) ddiff(drectangle(p,-1,1,-1,1),dcircle(p,0,0,0.3));

fh = @(p) 0.05 + 0.3*dcircle(p,0,0,0.3);

pfix = [-1,-1;-1,1;1,-1;1,1];

[p,t]=distmesh2d(fd,fh,0.03,[-1,-1;1,1],pfix);

 
      
disp('Mesh generation complete!')

