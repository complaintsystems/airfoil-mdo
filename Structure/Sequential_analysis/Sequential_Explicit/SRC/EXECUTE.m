%function main_2

clear all
clc
close all

addpath('meshGen/gridGen')
addpath('meshGen/')
addpath('functions/')


%% Airfoil mesh generation

global  XYZ LE  outeriter h d  BC


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------- MESH PARAMETERS -----------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

     hlead=0.005; htrail=0.01; hmax=2; circx=2; circr=4;
     
     
    % a=.12/.2*[0.2969,-0.1260,-0.3516,0.2843,-0.1036];
    
     a=.12/.2*[0.2969,-0.1260,-0.3516,0.2843,-0.1036];

 % Airfoil with single circular spar  
 
    fd  = @(p) ddiff((abs(p(:,2))-polyval([a(5:-1:2),0],p(:,1))).^2-a(1)^2*p(:,1),dcircle(p,0.46,0,0.04));
     
 % Airfoil with rectangular spar
 
      %fd  = @(p) ddiff((abs(p(:,2))-polyval([a(5:-1:2),0],p(:,1))).^2-a(1)^2*p(:,1),drectangle(p,0.25,0.35,-0.04,0.04));
   
% Airfoil with double circular spar  

     %fd = @(p) ddiff((abs(p(:,2))-polyval([a(5:-1:2),0],p(:,1))).^2-a(1)^2*p(:,1),dcircle(p,0.25,0,0.04));
     %d2 = @(p) dcircle(p,0.75,0,0.01);
     %fd = @(p) ddiff(d1,d2);
     
     %  fh=@(p) min(min(hlead+0.3*dcircle(p,0,0,0),htrail+0.3*dcircle(p,1,0,0)),hmax);
     
     fixx=1-htrail*cumsum(1.3.^(0:4)');
     fixy=a(1)*sqrt(fixx)+polyval([a(5:-1:2),0],fixx);
     fix=[[circx+[-1,1,0,0]*circr; 0,0,circr*[-1,1]]'; 0,0; 1,0; fixx,fixy; fixx,-fixy];
     box=[circx-circr,-circr; circx+circr,circr];
     h0=min([hlead,htrail,hmax]);
     
[p,t]=distmesh2d(fd,@huniform,0.01,box,fix);



% FIXED NODES DEFINITION :: MANUAL

% [FixedNodes] = nodeImport();

figure(1)
trimesh (t,p(:,1),p(:,2));
hold on
% sz = 20;
% scatter(FixedNodes.x, FixedNodes.y,sz,'MarkerEdgeColor',[0 0.5 0.5],...
%     'MarkerFaceColor',[0 .7 .7],...
%               'LineWidth',1.5)
hold off
grid minor
axis equal
xlabel 'X/C'
ylabel 'Y/C'
title 'Baseline profile'

%% Sort for boundary conditions 
%  FixedNodes = table2array(FixedNodes);

 BC = [514,520,526,532,537,542,546,549,548,544,539,533,527,521,515,509 ...
     ,509,502,496,490,489,493,497,503,508];


 
    
 BC = BC';
 
LE = zeros(size(t));
XYZ = zeros(size(p));


LE = t;
XYZ = p;
 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------------------------- MESHING END---------------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Discretization parameters

nnodes=size(XYZ,1);
nel = size(LE,1);
nel=size(LE,1);
penal=3;
ndof = 2;
neq=nnodes*2;

%% Volume fraction

volFrac = 0.5;
x(1:nel,1) = volFrac;

%% Element aspect ratio 
for e=1:nel
elXY=XYZ(LE(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate
a=sqrt((Y(1)-Y(2))^2+(X(1)-X(2))^2);
b=sqrt((Y(2)-Y(3))^2+(X(2)-X(3))^2);
c=sqrt((Y(1)-Y(3))^2+(X(1)-X(3))^2);
s=(a+b+c)/2;
AR(e,1)=a*b*c/(8*(s-a)*(s-b)*(s-c));
Area(e,1)=X(1)*Y(2)+X(2)*Y(3)+X(3)*Y(1)-X(1)*Y(3)-X(2)*Y(1)-X(3)*Y(2);
end


%% Density filter calculation
rmin = s;
W_filt = density_filter(XYZ,LE,rmin,nel);
 
%% Optimization paramters


xval=x;
m = 1;  % # of constraints
n=nel;
epsimin = 1e-7;
xold1   = xval;
xold2   = xval;
xmin    = 0.01*ones(n, 1);
xmax    = 1*ones(n, 1);
low     = xmin;
upp     = xmax;
c_mma   = 1000*ones(m, 1);
d_mma   = 1*ones(m, 1);
a0_mma  = 1;
a_mma   = 0*ones(m, 1);
outeriter=0;
kkttol  = 1e-5;
kktnorm = kkttol + 1;
kktcond=kktnorm;
 
while kktnorm > kkttol
    outeriter = outeriter + 1;
    x_phy = W_filt * x;
   
  %% FINITE ELEMENT ANALYSIS
    
 [comp, dc] = FEA_sens(x_phy);
 if outeriter ==1
     c0 = comp; % Vary value for feasible design 
 end
 df0dx = W_filt * dc;
 f0val= comp;
 
 
%% Constraints
%------------------------------MASS CONSTRAINT---------------------------%
M_bound=0.5; 
M=sum(diag(Area)*(W_filt*x));
M_total=sum(Area);
vol=M/M_total;
%------------------------------VOLUME CONSTRAINT--------------------------%
fval(1)= M/M_total - M_bound;
dfdx(:,1)=( 1/M_total)*Area;

%------------------------------COMPLAINCE CONSTRAINT----------------------%

% fval(2)= c0-comp;
% dfdx(:,2)= -df0dx;

%% MMA OPTIMIZATION SUB-ROUTINE
    
    [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,low,upp] = mmasub(m,n,outeriter,x,xmin,xmax,xold1,xold2,f0val,df0dx,fval',dfdx',low,upp,a0_mma,a_mma,c_mma,d_mma);
    
    % Updated vectors
    
    xold2 = xold1; 
    xold1 = x;
    x = xmma;
    
    % Perform kkt condition check
    
   [residu,kktnorm,residumax] = kktcheck(m,n,xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,xmin,xmax,df0dx,fval',dfdx',a0_mma,a_mma,c_mma,d_mma);
   save('rhoblock.mat','x_phy');
   disp([' ************************************* ']);
   disp([' outiter: ' sprintf('%4i', outeriter) ' kktnorm: ' sprintf('%6.4e', kktnorm) ...
        ' f_snap: ' sprintf('%10.7e', f0val) ' VolFrac: ' sprintf('%5.3f', M/M_total) ]);
   disp([' ************************************* ']);
domain(XYZ,LE,x_phy,zeros(neq,1),0,1,[],[],[],[],penal);
end 

x_phy
% domain(XYZ,LE,x_phy,zeros(neq,1),0,1,[],[],[],[],penal);


