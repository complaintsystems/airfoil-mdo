function [comp,dc] = FEA_sens(x)

global   XYZ LE  outeriter h d  BC


% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
volfrac=0.4;
penal=3;

neq=nnodes*2;
% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
ndof=2;
neq=nnodes*2;


% Specify density distribution
%x=ones(nel,1);
penal=3;

% specify Nodal parameters
% load_node=145*2-1; % 143 

end_node = 895*2;

 pt1 = 81*2;
 pt2 = 211*2;
pt3 = 346*2;
pt4 = 474*2;
 pt5 = 553*2;
pt6 = 654*2;
pt7 = 745*2;
 pt8 = 822*2;
 pt9 = 873*2;
 pt10 = 895*2-1;
 pt11 = 872*2;
 pt12 = 823*2;
pt13 = 753*2;
pt14 = 664*2;
 pt15= 559*2;
pt16 = 475*2;
pt17 = 339*2;
 pt18 =  213*2;
pt19 = 82*2;
pt20 = 24*2;
pt21 = 14*2;
% 
 pt22 = 1*2-1;
pt23 = 3*2-1;
pt24 = 2*2-1;
pt25 = 8*2;
pt26 = 9*2;
pt27 = 30*2;
pt28 = 32*2;




 
   


% boundary conditions

alldofs=1:neq;
fixed_nodes=BC';


fixeddofs=[fixed_nodes.*2-1 fixed_nodes.*2];
fixeddofs=sort(fixeddofs);
freedofs=setdiff(alldofs,fixeddofs);
nbc=size(fixeddofs,2);

for i=1:size(freedofs,2)
    if freedofs(:,i)==end_node
        count=i;
    end
   
end

% Newton-Raphson Convergence Criterion
epsilon=1e-9;
force_ctr=0;
const=5*100;
%for step=1:10
    %step;
    force_ctr=force_ctr+1;
% Initialize force distribution
Fext=zeros(neq,1);

 Fext(pt1,1)=-0.4107;
 Fext(pt2,1)=-0.3909;
Fext(pt3,1)=-0.3321;
Fext(pt4,1)=-0.2747;
 Fext(pt5,1)=-0.2172;
Fext(pt6,1)=-0.1702;
Fext(pt7,1)=-0.1137;
 Fext(pt8,1)=-0.0408;
 Fext(pt9,1)=-0.0465;
Fext(pt10,1)=-0.4148;
 Fext(pt11,1)=0.0465;
 Fext(pt12,1)=0.0405;
Fext(pt13,1)=0.1039;
Fext(pt14,1)=0.1608;
 Fext(pt15,1)=0.2172;
Fext(pt16,1)=0.2746;
Fext(pt17,1)=0.3321;
 Fext(pt18,1)=0.3904;
Fext(pt19,1)=0.4130;
Fext(pt20,1)=0.3268;
Fext(pt21,1)=-0.2641;

Fext(pt22,1)=0.9944;
Fext(pt23,1)=0.8599;
Fext(pt24,1)=0.8598;
Fext(pt25,1)=-0.0549;
Fext(pt26,1)=0.1201;
Fext(pt27,1)=-0.3511;
Fext(pt28,1)=0.3511;
% Specify Starting Displacememnt
U = zeros(neq,1);
%norm_res=1;
[dFint_dX,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal);
 Fext(end_node,1); 
 residual=Fext-Fint;
 norm_res=norm(residual(freedofs,1));
while norm_res>epsilon

delta_u=KT(freedofs,freedofs)\residual(freedofs);
U(freedofs)=U(freedofs)+delta_u;
[dFint_dX,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal);
Fext(end_node,1);
residual=Fext-Fint;
norm_res=norm(residual(freedofs,1));
end
U(end_node,1);
%end
%**************************************************************************
mes=1;
den=0;
% domain(XYZ,LE,x,U,0,1,[],[],[],[],penal);

%% Compliance
comp = U' * KT * U ;

%% sensitivity evaluation

kff=KT(freedofs,freedofs);

lambda = zeros(neq,1);
lambdaf=-Fext(freedofs,1)'/kff;
lambda(freedofs,1)=lambdaf';

for IE=1:nel
    
    IDOF=zeros(1,6);
    for I=1:3
        II=(I-1)*ndof+1;
        IDOF(II:II+1)=(LE(IE,I)-1)*ndof+1:(LE(IE,I)-1)*ndof+2;
    end

    
    dc(IE,:)=lambda(IDOF,1)'*dFint_dX(IDOF,IE);

    
end
end

