function [OML_plot] = airfoil_plot(airfoil_p)


global x0;
global y0;
global xf;
global yf;

p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];
airfoil_coord_calc(p);

outputfile = 'airfoil_coordinates.txt';                                              
file = fopen(outputfile);                                                 
dataBuffer = textscan(file,'%f %f','HeaderLines',0,...                 
                            'CollectOutput',1,...
                            'Delimiter','');
fclose(file); 

x0 = dataBuffer{1,1}(:,1);
y0 = dataBuffer{1,1}(:,2);



 airfoil_p = [airfoil_p,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];
airfoil_coord_calc(airfoil_p);

outputfile = 'airfoil_coordinates.txt';                                              
file = fopen(outputfile);                                                 
dataBuffer = textscan(file,'%f %f','HeaderLines',0,...                 
                           'CollectOutput',1,...
                           'Delimiter','');
fclose(file); 

xf = dataBuffer{1,1}(:,1);
yf = dataBuffer{1,1}(:,2);
hold off
figure(2)
hold on
plot(x0,y0,'LineWidth', 2);
plot(xf,yf,'LineWidth', 2);
% axis 'equal'
grid minor
xlabel( 'X/C','FontWeight','bold','FontSize',20);
ylabel ('Y/C','FontWeight','bold','FontSize',20);
title ('Airfoil Geometries','FontWeight', 'bold','FontSize',20)
hold off
% legend('First config', 'Second config')
%delete('airfoil_coordinates.txt')

end

