function [comp,dc] = FEA_sens(x)

global   XYZ LE  outeriter h d  BC OML


% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
volfrac=0.4;
penal=3;

neq=nnodes*2;
% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
ndof=2;
neq=nnodes*2;


% Specify density distribution
%x=ones(nel,1);
penal=3;

%% SPECIFY NODAL PARAMETERS

for k = 1:length(OML)
    pt(k,1) = OML(k,1)*2;
end

pt1 = 1*2-1;
pt2 = 2*2-1;
pt3 = 4*2-1;
pt4 = 8*2;
pt5 =13*2;
pt6 = 23*2;
pt7 = 30*2;
pt8 =38*2;
pt9 = 48*2;
pt10 = 59*2;

pt11 = 70*2;
pt12 = 81*2;
pt13 = 104*2;
pt14 = 116*2;
pt15 =134*2;
pt16 = 159*2;
pt17 = 185*2;
pt18 =198*2;
pt19 = 224*2;
pt20 = 237*2;


pt21 = 265*2;
pt22 = 292*2;
pt23 = 305*2;
pt24 = 332*2;
pt25 = 346*2;
pt26 = 367*2;
pt27 = 394*2;
pt28 = 408*2;
pt29 = 435*2;
pt30 = 461*2;

pt31 = 474*2;
pt32 = 498*2;
pt33 = 512*2;
pt34 = 518*2;
pt35 = 528*2;
pt36 = 541*2;
pt37 = 553*2;
pt38 = 576*2;
pt39 = 588*2;
pt40 = 610*2;

pt41 = 632*2;
pt42 = 643*2;
pt43 = 654*2;
pt44 = 665*2;
pt45 = 687*2;
pt46 = 698*2;
pt47 = 708*2;
pt48 = 727*2;
pt49 = 745*2;
pt50 = 753*2;

pt51 = 771*2;
pt52 = 779*2;
pt53 = 787*2;
pt54 = 794*2;
pt55 = 808*2;
pt56 = 815*2;
pt57 = 822*2;
pt58 = 834*2;
pt59 = 844*2;
pt60 = 849*2;

pt61 = 863*2;
pt62 = 866*2;
pt63 = 876*2;
pt64 = 879*2;
pt65 = 882*2;
pt66 = 885*2;
pt67 = 889*2;
pt68 = 892*2;
pt69 = 894*2;
pt70 = 895*2-1;

pt71 = 893*2;
pt72 = 891*2;
pt73 = 888*2;
pt74 = 886*2;
pt75 = 878*2;
pt76 = 872*2;
pt77 = 869*2;
pt78 = 861*2;
pt79 = 851*2;
pt80 = 846*2;

pt81 = 835*2;
pt82 = 823*2;
pt83 = 816*2;
pt84 = 802*2;
pt85 = 795*2;
pt86 = 788*2;
pt87 = 772*2;
pt88 = 754*2;
pt89 = 744*2;
pt90 = 726*2;

pt91 = 707*2;
pt92 = 686*2;
pt93 = 664*2;
pt94 = 630*2;
pt95 = 607*2;
pt96 = 583*2;
pt97 = 559*2;
pt98 = 545*2;
pt99 = 529*2;
pt100 = 513*2;

pt101 = 499*2;
pt102 = 475*2;
pt103 = 433*2;
pt104 = 407*2;
pt105 = 396*2;
pt106 = 368*2;
pt107 = 339*2;
pt108 = 325*2;
pt109 = 297*2;
pt110 = 283*2;


pt111 = 255*2;
pt112 = 227*2;
pt113 = 213*2;
pt114 = 200*2;
pt115 = 187*2;
pt116 = 160*2;
pt117 = 147*2;
pt118 = 121*2;
pt119 = 108*2;
pt120 = 94*2;

pt121 = 82*2;
pt122 = 71*2;
pt123 = 60*2;
pt124 = 50*2;
pt125 = 41*2;
pt126 = 32*2;
pt127 = 24*2;
pt128 = 17*2;
pt129 = 9*2;
pt130 = 5*2;
pt131 = 3*2-1;






 
   


% boundary conditions

alldofs=1:neq;
fixed_nodes=BC';


fixeddofs=[fixed_nodes.*2-1 fixed_nodes.*2];
fixeddofs=sort(fixeddofs);
freedofs=setdiff(alldofs,fixeddofs);
nbc=size(fixeddofs,2);

% for i=1:size(freedofs,2)
%     if freedofs(:,i)==end_node
%         count=i;
%     end
%    
% end

% Newton-Raphson Convergence Criterion
epsilon=1e-9;
force_ctr=0;
const=5*100;
%for step=1:10
    %step;
    force_ctr=force_ctr+1;
% Initialize force distribution
Fext=zeros(neq,1);

%% Define external pressure forces at nodes

% for k = 1:length(pt)
%     Fext(pt(k,1),1) = 0.9999;
%     end
    
Fext(pt1,1) = 0.99435;
Fext(pt2,1) = 0.60265;
Fext(pt3,1) = 0.11212;
Fext(pt4,1) = -0.12075;
Fext(pt5,1) = -0.26389;
Fext(pt6,1) = -0.32661;
Fext(pt7,1) = -0.3511;
Fext(pt8,1) = -0.37042;
Fext(pt9,1) = -0.38615;
Fext(pt10,1) = -0.39772;
  

Fext(pt11,1) = -0.40581;
Fext(pt12,1) = -0.41069;
Fext(pt13,1) = -0.41303;
Fext(pt14,1) = -0.41104;
Fext(pt15,1) = -0.40752;
Fext(pt16,1) = -0.40298;
Fext(pt17,1) = -0.3972;
Fext(pt18,1) = -0.39092;
Fext(pt19,1) = -0.38336;
Fext(pt20,1) = -0.37591;

Fext(pt21,1) = -0.36776;
Fext(pt22,1) = -0.35923;
Fext(pt23,1) = -0.35036;
Fext(pt24,1) = -0.34144;
Fext(pt25,1) = -0.33212;
Fext(pt26,1) = -0.32274;
Fext(pt27,1) = -0.31332;
Fext(pt28,1) = -0.30359;
Fext(pt29,1) = -0.29408;
Fext(pt30,1) = -0.28451;

Fext(pt31,1) = -0.27468;
Fext(pt32,1) = -0.26521;
Fext(pt33,1) = -0.25539;
Fext(pt34,1) = -0.24599;
Fext(pt35,1) = -0.23609;
Fext(pt36,1) = -0.22685;
Fext(pt37,1) = -0.21718;
Fext(pt38,1) = -0.20775;
Fext(pt39,1) = -0.1983;
Fext(pt40,1) = -0.18882;

Fext(pt41,1) = -0.17959;
Fext(pt42,1) = -0.17019;
Fext(pt43,1) = -0.17019;
Fext(pt44,1) = -0.16084;
Fext(pt45,1) = -0.15163;
Fext(pt46,1) = -0.14218;
Fext(pt47,1) = -0.13265;
Fext(pt48,1) = -0.12334;
Fext(pt49,1) = -0.1137;
Fext(pt50,1) = -0.10394;

Fext(pt51,1) = -0.09433;
Fext(pt52,1) = -0.09433;
Fext(pt53,1) = -0.08411;
Fext(pt54,1) = -0.07384;
Fext(pt55,1) = -0.06341;
Fext(pt56,1) = -0.05214;
Fext(pt57,1) = -0.04083;
Fext(pt58,1) = -0.02879;
Fext(pt59,1) = -0.01596;
Fext(pt60,1) = -0.00253;

Fext(pt61,1) = 0.01252;
Fext(pt62,1) = 0.02853;
Fext(pt63,1) = 0.04649;
Fext(pt64,1) = 0.0668;
Fext(pt65,1) = 0.09039;
Fext(pt66,1) = 0.11795;
Fext(pt67,1) = 0.1515;
Fext(pt68,1) = 0.19552;
Fext(pt69,1) = 0.25527;
Fext(pt70,1) = 0.41477;

Fext(pt71,1) = -0.25526;
Fext(pt72,1) = -0.19576;
Fext(pt73,1) = -0.15145;
Fext(pt74,1) = -0.06679;
Fext(pt75,1) =-0.06679;
Fext(pt76,1) = -0.06679;
Fext(pt77,1) = -0.02868;
Fext(pt78,1) =-0.01228;
Fext(pt79,1) = 0.00231;
Fext(pt80,1) = 0.01572;

Fext(pt81,1) = 0.02922;
Fext(pt82,1) = 0.04046;
Fext(pt83,1) = 0.05243;
Fext(pt84,1) = 0.06327;
Fext(pt85,1) =0.07387;
Fext(pt86,1) = 0.08408;
Fext(pt87,1) = 0.09429;
Fext(pt88,1) =0.1039;
Fext(pt89,1) = 0.11384;
Fext(pt90,1) = 0.12327;

Fext(pt91,1) = 0.13275;
Fext(pt92,1) = 0.15148;
Fext(pt93,1) = 0.1608;
Fext(pt94,1) =0.17963;
Fext(pt95,1) =0.18885;
Fext(pt96,1) = 0.19827;
Fext(pt97,1) = 0.2172;
Fext(pt98,1) =0.22667;
Fext(pt99,1) = 0.23636;
Fext(pt100,1) = 0.25549;

Fext(pt101,1) = 0.27463;
Fext(pt102,1) = 0.27463;
Fext(pt103,1) = 0.29413;
Fext(pt104,1) =0.3037;
Fext(pt105,1) =0.31315;
Fext(pt106,1) = 0.32284;
Fext(pt107,1) =0.33211;
Fext(pt108,1) =0.35038;
Fext(pt109,1) = 0.35038;
Fext(pt110,1) =0.35927;

Fext(pt111,1) = 0.36769;
Fext(pt112,1) = 0.37583;
Fext(pt113,1) = 0.38355;
Fext(pt114,1) =0.39067;
Fext(pt115,1) =0.39733;
Fext(pt116,1) = 0.40283;
Fext(pt117,1) =0.40762;
Fext(pt118,1) =0.41109;
Fext(pt119,1) = 0.4128;
Fext(pt120,1) =0.41303;

Fext(pt121,1) = 0.41076;
Fext(pt122,1) =0.40568; 
Fext(pt123,1) = 0.39793;
Fext(pt124,1) =0.38582;
Fext(pt125,1) =0.37053;
Fext(pt126,1) = 0.35105;
Fext(pt127,1) =0.32679;
Fext(pt128,1) =0.26406;
Fext(pt129,1) = 0.1201;
Fext(pt130,1) =-0.11157;
Fext(pt131,1) =-0.99438;


  
  
% Specify Starting Displacememnt
U = zeros(neq,1);
%norm_res=1;
[dFint_dX,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal); 
 residual=Fext-Fint;
 norm_res=norm(residual(freedofs,1));
while norm_res>epsilon

delta_u=KT(freedofs,freedofs)\residual(freedofs);
U(freedofs)=U(freedofs)+delta_u;
[dFint_dX,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal);
residual=Fext-Fint;
norm_res=norm(residual(freedofs,1));
end

%end
%**************************************************************************
mes=1;
den=0;
% domain(XYZ,LE,x,U,0,1,[],[],[],[],penal);

%% Compliance
comp = U' * KT * U ;

%% sensitivity evaluation

kff=KT(freedofs,freedofs);

lambda = zeros(neq,1);
lambdaf=-Fext(freedofs,1)'/kff;
lambda(freedofs,1)=lambdaf';

for IE=1:nel
    
    IDOF=zeros(1,6);
    for I=1:3
        II=(I-1)*ndof+1;
        IDOF(II:II+1)=(LE(IE,I)-1)*ndof+1:(LE(IE,I)-1)*ndof+2;
    end

    
    dc(IE,:)=lambda(IDOF,1)'*dFint_dX(IDOF,IE);

    
end
end

