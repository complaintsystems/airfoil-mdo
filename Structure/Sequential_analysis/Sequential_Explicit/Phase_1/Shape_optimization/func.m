function [f,g] = func(parameters)
% PLEASE SELECT AN OBJECTIVE FUNCTION
        %1:MINIMIZE CD 
        %2:MINIMIZE wl*(1-CC(1)/cl_star)^2+wd*(1-CC(2)/cd_star)^2
        % IF 2 IS CHOSEN, PLEASE SPECIFY TARGET CL AND CD IN FUNC.M
        objfun = 2;
        
wl = 0.005;
wd = 0.005;
cl_star = 0.5;
cd_star = 0.005;
 parameters = [parameters,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];
CC = interface(parameters);      
%  CC = [CL,CD]
G = zeros(1,2);
g = zeros(1,4);
                                                       % minimise f at fixed CD
% f= wl*(1-CC(1)/cl_star)^2+wd*(1-CC(2)/cd_star)^2
if objfun == 1
 f = CC(2);
else if objfun == 2
 f= wl*(1-CC(1)/cl_star)^2+wd*(1-CC(2)/cd_star)^2;
    end
end
 for i = 1:4
    I = [parameters,i];
G = grad(I);
if objfun == 1
if i == 1
    g(1,i) = (G(2)-CC(2))/abs(0.0001*parameters(i));
else if i<8 && i~=1
g(1,i) = (G(2)-CC(2))/abs(0.003*parameters(i));

else 
      g(1,i) = (G(2)-CC(2))/abs(1e-6*parameters(i));
    end
end
else if objfun == 2
        if i == 1
          G = (G-CC)/abs(0.0001*parameters(i));
          g(1,i) = -wl*2*(1-CC(1)/cl_star)/cl_star*G(1)-2*wd*(1-CC(2)/cd_star)/cd_star*G(2);
        else if i<8 && i~=1
        G = (G-CC)/abs(0.003*parameters(i));
        g(1,i) = -wl*2*(1-CC(1)/cl_star)/cl_star*G(1)-2*wd*(1-CC(2)/cd_star)/cd_star*G(2);

        else 
         G = (G-CC)/abs(1e-6*parameters(i));   
        g(1,i) = -wl*2*(1-CC(1)/cl_star)/cl_star*G(1)-2*wd*(1-CC(2)/cd_star)/cd_star*g(2);
            end
        end
    end
end
end