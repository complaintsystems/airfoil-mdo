function [c,ceq] = nlconstraint(CL)
global CL0
CL0 = 0.4428
ceq = [];
eps = 1e-4
c = (CL - CL0)-eps 
end