function stop = outfun(x,optimValues,state)
history.x = [];
history.fval = [];
searchdir = [];
     stop = false;
     switch state
         case 'init'
         case 'iter'
         % Concatenate current point and objective function
           history.fval = [history.fval; optimValues.fval];
           history.x = [history.x; x];
           searchdir = [searchdir;optimValues.searchdirection'];
%            plot(x(1),'o');
         % Label points with iteration number and add title.
         case 'done'
         otherwise
     end

end