
% Script to compute solution of linear system using Gauss Seidel 

function [x, iter] = Gauss_Seidel(A, b, x0, nmax, tol, omega)

%% NOMENCLATURE

%{

A :: Square single line/block matrix
b :: Vector of RHS
x0 :: Initial Guess
nmax :: Maximum nunber of iterations
tol :: Solution tolerance
omega :: Relaxation factor

%}

% Solve for linear system A * x = b

[n,m] = size(A);

if n~=m
    error(' Only Square Systems! ')
end

iter = 0;
r = b-A*x0;
r0 = norm(r);
err = norm(r);
xold = x0;


while err > tol & iter < nmax
    iter = iter + 1
    
    for i = 1:n
        s = 0;
        for j = 1:i-1
            s = s + A(i,j)*x(j);
        end
         
        for j = i+1:n
            s = s+ A(i,j) *xold(j);
        end
         
        x(i,1) = omega * (b(i)-s)/A(i,i) + (1-omega)*xold(i)
    end
    
    xold = x;
    r = b-A*x;
    err = norm(r)/r0;
end
return
