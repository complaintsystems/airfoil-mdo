clc
clear all
close all

A = [4 1 -1;
    2 7 1;
    1 -3 12];

    
b = [3;19;31];


x0 = [0;0;0];
 tol = 1e-04;
 omega = 0.5;

 nmax = 10;

[x, iter] = Gauss_Seidel(A, b, x0, nmax, tol, omega)