clc,clear
N = 18;
fid = fopen('NACA2412.txt','r');  % read coordinates
    xu = zeros(N,1);
    yu = zeros(N,1);
    xl = zeros(N,1);
    yl = zeros(N,1);
for i = 1:N
  coord = fscanf(fid,'%f %f',2);    %divide them into upper\lower surface
  xu(i) = coord(1);
  yu(i) = coord(2);
end
for i = 1:N
  coord = fscanf(fid,'%f %f',2);
  xl(i) = coord(1);
  yl(i) = coord(2);
end
for i = 1:N-1
panelUx(i) = (xu(i+1)+xu(i))/2;
panelUy(i) = (yu(i+1)+yu(i))/2;
panelLx(i) = (xl(i+1)+xl(i))/2;
panelLy(i) = (yl(i+1)+yl(i))/2;
end
chord = (yu+yl)/2;
n = 10;                               %number of vortex 
% al = linspace(-20,20,41);           %angle of attack
 al = 10.0254;
for z = 1:length(al)
a = deg2rad(al(z)); %angle of attack

ctrl = zeros(n,2);                  %define vortex and control point position
vortex = zeros(n,2);

A = zeros(n,n);                     %define phi matrix (A)
B = zeros(n,1);
panel = zeros(n+1,2);               %define panels along the chord
temp = round(linspace(2,length(chord),n+1),0);  %find coordinate of each panel
for i = 1:length(temp)
panel(i,:) = [xu(temp(i)),chord(temp(i))];      
end
for i = 2:length(temp)              %vortices are located at 1/4 of each panel and control point at 3/4
    vortex(i-1,:) = (panel(i,:)-panel(i-1,:))*0.25+panel(i-1,:);
    ctrl(i-1,:) = (panel(i,:)-panel(i-1,:))*0.75+panel(i-1,:);
end
for i = 2:length(temp)              %find the local angle of attack caused by camber along the chord
    deriv(i-1) = atan((panel(i,2)-panel(i-1,2))...
                /(panel(i,1)-panel(i-1,1)));
end
for i = 1:n
    for j = 1:n                     %construct linear system to solve for strength of vortex
      A(i,j) = ((ctrl(i,1)-vortex(j,1))/...
               (((ctrl(i,1)-vortex(j,1))^2+(ctrl(i,2)-vortex(j,2))^2)*2*pi))...
                                                        *cos(deriv(i))+...
               ((ctrl(i,2)-vortex(j,2))/...
               (((ctrl(i,1)-vortex(j,1))^2+(ctrl(i,2)-vortex(j,2))^2)*2*pi))...
                                                        *sin(deriv(i));
    end
    B(i) = sin(a-deriv(i));         %such that essential BC is that normal velocity across control point is 0
end
AIC = -2*inv(A);                    %Compute AIC

Gamma = linsolve(A,B);              %solve linear system for vortex strength
cl(z) = 2*sum(Gamma);               %calculate cl
cm(z) = sum(Gamma)/2;
for i = 1:length(panelUx)-1                %find cp distribution by calculating flow velocity at airfoil OML
    tanu(i) = atan((panelUy(i+1)-panelUy(i))/(panelUx(i+1)-panelUx(i)));
    for j = 1:length(vortex)
        Qvn(i,j) = -((panelUx(i)-vortex(j,1))/...
               (((panelUx(i)-vortex(j,1))^2+(panelUy(i)-vortex(j,2))^2)*2*pi));
                                                        
       Qvt(i,j) =((panelUy(i)-vortex(j,2))/...
               (((panelUx(i)-vortex(j,1))^2+(panelUy(i)-vortex(j,2))^2)*2*pi));
    end
    Qit(i) = cos(a);
end
Q =((Qvt*Gamma+Qit').^2+(Qvn*Gamma).^2); %
cpu = 1-(Q);                    %cp  =1-(v/v_inf)^2; similiarly for lower surface
cpux = cpu'.*cos(tanu-a);       %find vertical and horizontal component of cp
cpuy = cpu'.*sin(tanu-a);
for i = 1:length(panelLx)-1
    tanl(i) = atan((panelLy(i+1)-panelLy(i))/(panelLx(i+1)-panelLx(i)));
    for j = 1:length(vortex)

        Qv(i,j) = ((panelLx(i)-vortex(j,1))/...
               (((panelLx(i)-vortex(j,1))^2+(panelLy(i)-vortex(j,2))^2)*2*pi));
               
        Qh(i,j) = ((panelLy(i)-vortex(j,2))/...
               (((panelLx(i)-vortex(j,1))^2+(panelLy(i)-vortex(j,2))^2)*2*pi));
    end
        Qi(i) = cos(a);
end
Q =(Qh*Gamma+Qi').^2+(Qv*Gamma).^2;
cpl = 1-(Q);
cplx = cpl'.*cos(tanl-a);
cply = cpl'.*sin(tanl-a);
cd(z) = sum((-cpu+cpl)*1/19)*sin(a);
end



