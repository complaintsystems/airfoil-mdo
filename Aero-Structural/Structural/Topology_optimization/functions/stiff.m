function [ Fint_d1,Ktan,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal)
global Strain Sigma
neq=nnodes*NDOF;
% Kgeo   = speye(neq,neq);
Kmat   = speye(neq,neq);
J      = sparse(neq);
Fint   = zeros(neq,1);
Fint_d1=zeros(neq,nel);
S = zeros(3,nel);


for e=1:nel
    [fint_d1,fe, kgeo, kmat,strain,sigma] = hyper2d(e,u,XYZ,LE,nel,x,penal);
    
   
    
% output=struct('array1',fint_d1,'array2',fe,'array3',kgeo,'array4',kmat);

% strain(:,e) = strain;
    
    for I=1:3
        II=(I-1)*NDOF+1;
        IDOF(II:II+1)=(LE(e,I)-1)*NDOF+1:(LE(e,I)-1)*NDOF+2;
    end   

    
    Fint(IDOF)      = Fint(IDOF)+fe;
    Fint_d1(IDOF,e)   = Fint_d1(IDOF,e)+ fint_d1;
%     
%      Kgeo(IDOF,IDOF) = Kgeo(IDOF,IDOF) + kgeo;
    Kmat(IDOF,IDOF) = Kmat(IDOF,IDOF) + kmat;
    %      J(IDOF,IDOF)    = J(IDOF,IDOF) + je;
    
    Strain(:,e) = strain;
    
    Sigma(:,e) = sigma;
end

%  Kgeo = Kgeo- speye(neq);
Kmat = Kmat- speye(neq);
% J    =  J    - speye(neq);

% R =  Fext - Fint;

 Ktan = Kmat;
end

