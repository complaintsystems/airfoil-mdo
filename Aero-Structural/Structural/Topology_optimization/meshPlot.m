clc
clear all
close all

addpath('meshGen')


%% IMPORT GEOMETRY

fileID = fopen('FULL_AIRFOIL_XYZ.TXT');
formatSpec=('%*s%*d%f%f%*d%*d%*d%*d');
XYZ=textscan(fileID,formatSpec);
[XYZ_1 XYZ_2]=XYZ{:};
XYZ=[XYZ_1 XYZ_2];


%Element conenctivity

fileID = fopen('FULL_AIRFOIL_LE.TXT');
formatSpec=('%*s%*d%*s%d%d%d');
LE=textscan(fileID,formatSpec);
[LE_1 LE_2 LE_3]=LE{:};
LE=[LE_1 LE_2 LE_3];

% [x,y] = meshgrid(1:15,1:15);
% tri = delaunay(x,y);
% z = peaks(15);
% trimesh(tri,x,y)
figure()
trimesh(LE,XYZ_1,XYZ_2)
axis equal
xlabel 'X'
ylabel 'Y'

