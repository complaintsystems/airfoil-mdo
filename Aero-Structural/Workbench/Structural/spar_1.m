function [Cav_x, Cav_y, Cav_z] = spar_1()

x = 0.25;                  % Spar location in x
y = 0.0;                   % Spar location in y
r = 0.03;                  % Spar radius

th = 0:pi/50:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;

xunit = xunit';
yunit = yunit';

Coord = [xunit yunit];

%% Restructure the perimeter coordinates based on PSLG convention

Coord_low = Coord((length(Coord)-1)/2 +1:end,:);
Coord_up = Coord(1:(length(Coord)-1)/2,:);


Coord = vertcat(Coord_low,Coord_up);
Coord((length(Coord)-1)/2 + 1,:) =[];
Coord_z = zeros(length(Coord),1);
Coord = horzcat(Coord,Coord_z);

Cav_x = Coord(:,1);
Cav_y = Coord(:,2);
Cav_z = Coord(:,3);



end

