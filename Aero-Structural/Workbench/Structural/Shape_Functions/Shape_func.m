

% Script to compute aerodynamic panel displacement 

%% Iniitalize arbitrary values for nodal displacment 

function [N] = Shape_func(r, s)


% Define shape functions 

N1 = 1-r-s;
N2 = r;


% Construct shape function matrix [N]

N = [N1 N2];

end




