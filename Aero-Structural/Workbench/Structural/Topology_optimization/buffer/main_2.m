%function main_2

clc
clear all
close all

global   XYZ LE  outeriter h d  BC

addpath('meshGen')

addpath('functions')

%% IMPORT GEOMETRY

fileID = fopen('FULL_AIRFOIL_XYZ.TXT');
formatSpec=('%*s%*d%f%f%*d%*d%*d%*d');
XYZ=textscan(fileID,formatSpec);
[XYZ_1 XYZ_2]=XYZ{:};
XYZ=[XYZ_1 XYZ_2];



% element conenctivity

fileID = fopen('FULL_AIRFOIL_LE.TXT');
formatSpec=('%*s%*d%*s%d%d%d');
LE=textscan(fileID,formatSpec);
[LE_1 LE_2 LE_3]=LE{:};
LE=[LE_1 LE_2 LE_3];

% Boundary Conditions

fileID=fopen('FULL_AIRFOIL_BC.TXT');
formatSpec=('%*s%d%*f%*f%*f%*f%*f%*f');
BC=textscan(fileID,formatSpec);
BC=BC{:};
BC([2 4 1 7 10 8],:)=[];

%BC1=[41:43 45:46 284:290]; % if you want break in the boundary
BC1=[34:43 45:53 277:298];  % if you want full boundary
BC=[BC1';BC];% FOR JAN_20

% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
volfrac=0.4;
penal=3;

neq=nnodes*2;

% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
% volfrac=1;
% x(1:nel,1)=volfrac;
% penal=3;

ndof=2;
neq=nnodes*2;


% Specify density distribution
x=ones(nel,1);
penal=3;

% specify Nodal parameters
 load_node=145*2-1; % 143 

 end_node=44*2;

%% BOUNDARY CONDITIONS

alldofs=1:neq;
fixed_nodes=BC';


fixeddofs=[fixed_nodes.*2-1 fixed_nodes.*2];
fixeddofs=sort(fixeddofs);
freedofs=setdiff(alldofs,fixeddofs);
nbc=size(fixeddofs,2);

for i=1:size(freedofs,2)
    if freedofs(:,i)==load_node
        count=i;
    end
   
end

% Newton-Raphson Convergence Criterion
epsilon=1e-9;
force_ctr=0;
const=5*100;
for step=1:10
    step;
    force_ctr=force_ctr+1;
% Initialize force distribution
Fext=zeros(neq,1);
Fext(end_node,1)=const*force_ctr;
% Specify Starting Displacememnt
U = zeros(neq,1);
%norm_res=1;
[Fint_d1,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal);
 Fext(end_node,1);
 residual=Fext-Fint;
 norm_res=norm(residual(freedofs,1));
while norm_res>epsilon
% [Fint_d1,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal);
% Fext(end_node,1)
% residual=Fext-Fint;
% Fint(load_node)
delta_u=KT(freedofs,freedofs)\residual(freedofs);
U(freedofs)=U(freedofs)+delta_u;
[Fint_d1,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal);
Fext(end_node,1);
residual=Fext-Fint;
norm_res=norm(residual(freedofs,1));
end
U(end_node,1);
end
%**************************************************************************
mes=1;
den=0;
domain(XYZ,LE,[],U,mes,den,[],[],[],[],penal);


% h3=figure(3);
% %plot((uy),(theta_hist),'linewidth',2);
% xlabel('Displacement') % x-axis label
% ylabel('Load \theta') % y-axis label
% set(gcf,'color','w');
% grid on
% saveas(h3,sprintf('FIG%d.png',outeriter))
% %  PLOT(XYZ,u,nelx,nely)
% %fext=theta;


%% SENSITIVITY CALCULATION

% fixeddof=[load_node fixeddofs];
%         fixeddof=sort(fixeddof);
%         alldof=1:neq;
%         freedof=setdiff(alldof,fixeddof);
%         kff=KT(freedof,freedof);
%         kpf=KT(fixeddof,freedof);
%         kfp=KT(freedof,fixeddof);
%         kpp=KT(fixeddof,fixeddof);
% lambda=zeros(neq,1);
% lambdap_T=force(fixeddof,1)';
% lambdap2_T=zeros(size(fixeddof,2),1)';
% 
% u_new=zeros(neq,1);
% u_new(end_node,1)=1;
% u_new=u_new(freedof,1);
% 
% 
% lambdaf2_T=-u_new'/kff;
% lambdaf_T=-lambdap_T*kpf/(kff);
% 
% %
% lambda(freedof,1)=lambdaf_T';
% lambda(fixeddof,1)=lambdap_T';
% lambda2(freedof,1)=lambdaf2_T';
% lambda2(fixeddof,1)=lambdap2_T';



% for IE=1:nel
%     
%     IDOF=zeros(1,6);
%     for I=1:3
%         II=(I-1)*NDOF+1;
%         IDOF(II:II+1)=(LE(IE,I)-1)*NDOF+1:(LE(IE,I)-1)*NDOF+2;
%     end
%     lambda2_e=lambda2(IDOF);
%     lambda_e=lambda(IDOF);
%     
%     dc(IE,:)=lambda_e'*Fint_d1(IDOF,IE);
%     dc2(IE,:)=lambda2_e'*Fint_d1(IDOF,IE);
%     
% end
%end




%%%%%%%%%%%%%%%%%%%%%%%%%%% APPENDIX%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
