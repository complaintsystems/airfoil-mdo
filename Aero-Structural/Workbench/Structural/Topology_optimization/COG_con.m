function [COG_cons] = COG_con(x_phy)
global Centx nel Area Mass


for e = 1:nel
    Mass(e,1) = x_phy(e,1).*Area(e,1);
end

Weight = Mass;


cgx = zeros(nel,1);
%cgy = zeros(nel,1);

for e = 1:nel
    cgx(e,1) = Weight(e,1).*Centx(e,1);
    %cgy(e,1) = Weight(e,1).*Centy(e,1);
end

COGx = sum(cgx)/sum(Weight);
eps = 1e-03;
COG_cons = (COGx-0.47).^2 - eps;


%CGY = sum(cgy)/sum(Weight);

end

