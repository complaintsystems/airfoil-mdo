% Two-dimensional potential flow solver



function [cl,Gamma,cmac,xp,yp,cp,cpx,cpy,XB,YB] = Potential_Solver(p,alpha)
% 
% p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101, -2.7791,9.2496];
% alpha = 1
% Obtain values from PARSEC

[XB, YB] = Airfoil_Paramterization(p);
xu = flip([XB(1:(length(XB)-1)/2);0]);
yu = flip([YB(1:(length(XB)-1)/2);0]);
xl = ([0;XB((length(XB)+3)/2:length(XB))]);
yl = ([0;YB((length(XB)+3)/2:length(XB))]);
chord = (yu+yl)/2;
for i = 1:length(xu)-1
panelUx(i) = (xu(i+1)+xu(i))/2;
panelUy(i) = (yu(i+1)+yu(i))/2;
panelLx(i) = (xl(i+1)+xl(i))/2;
panelLy(i) = (yl(i+1)+yl(i))/2;
end
XB = flip(XB);
YB = flip(YB);


n = 100;                            %number of vortex along chord
a = deg2rad(alpha);
ctrl = zeros(n,2);                  %define vortex and control point position
vortex = zeros(n,2);
A = zeros(n,n);                     %define phi matrix (A)
B = zeros(n,1);
panel = zeros(n+1,2);               %define panels along the chord
temp = round(linspace(2,length(chord),n+1),0);  %find coordinate of each right sided panel node
for i = 1:length(temp)
panel(i,:) = [xu(temp(i)),chord(temp(i))];      
end
for i = 2:length(temp)              %vortices are located at 1/4 of each panel and control point at 3/4
    vortex(i-1,:) = (panel(i,:)-panel(i-1,:))*0.25+panel(i-1,:);
    ctrl(i-1,:) = (panel(i,:)-panel(i-1,:))*0.75+panel(i-1,:);
end
for i = 2:length(temp)              %find the local angle of attack caused by camber along the chord
    deriv(i-1) = atan((panel(i,2)-panel(i-1,2))...
                /(panel(i,1)-panel(i-1,1)));
end
for i = 1:n
    for j = 1:n                     %construct linear system to solve for strength of vortex
      A(i,j) = ((ctrl(i,1)-vortex(j,1))/...
               (((ctrl(i,1)-vortex(j,1))^2+(ctrl(i,2)-vortex(j,2))^2)*2*pi))...
                                                        *cos(deriv(i))+...
               ((ctrl(i,2)-vortex(j,2))/...
               (((ctrl(i,1)-vortex(j,1))^2+(ctrl(i,2)-vortex(j,2))^2)*2*pi))...
                                                        *sin(deriv(i));
    end
    B(i) = sin(a-deriv(i));         %such that essential BC is that normal velocity across control point is 0
end
AIC = -2*inv(A);                    %Compute AIC

Gamma = linsolve(A,B);              %solve linear system for vortex strength

for i = 1:length(panelUx)-1                %find cp distribution by calculating flow velocity at airfoil OML
    tanu(i) = atan((panelUy(i+1)-panelUy(i))/(panelUx(i+1)-panelUx(i)));
end
tanu = [tanu,0];
for i = 1:length(panelUx)
    for j = 1:length(vortex)
        Qvn(i,j) = -((panelUx(i)-vortex(j,1))/...
               (((panelUx(i)-vortex(j,1))^2+(panelUy(i)-vortex(j,2))^2)*2*pi));
                                                        
       Qvt(i,j) =((panelUy(i)-vortex(j,2))/...
               (((panelUx(i)-vortex(j,1))^2+(panelUy(i)-vortex(j,2))^2)*2*pi));
    end
    Qit(i) = cos(a);
end
Q =((Qvt*Gamma+Qit').^2+(Qvn*Gamma).^2); %
cpu = 1-(Q);                    %cp  =1-(v/v_inf)^2; similiarly for lower surface
cpux = cpu'.*cos(tanu-a);       %find vertical and horizontal component of cp
cpuy = cpu'.*sin(tanu-a);
for i = 1:length(panelLx)-1
    tanl(i) = atan((panelLy(i+1)-panelLy(i))/(panelLx(i+1)-panelLx(i)));
end
    tanl = [tanl,0];
for i = 1:length(panelLx)
    for j = 1:length(vortex)

        Qv(i,j) = ((panelLx(i)-vortex(j,1))/...
               (((panelLx(i)-vortex(j,1))^2+(panelLy(i)-vortex(j,2))^2)*2*pi));
               
        Qh(i,j) = ((panelLy(i)-vortex(j,2))/...
               (((panelLx(i)-vortex(j,1))^2+(panelLy(i)-vortex(j,2))^2)*2*pi));
    end
        Qi(i) = cos(a);
end
Q =(Qh*Gamma+Qi').^2+(Qv*Gamma).^2;
cpl = 1-(Q);
cplx = cpl'.*cos(tanl-a);
cply = cpl'.*sin(tanl-a);
cl = 2*sum(Gamma);              %calculate cl
cmac = sum(Gamma)/2;
cd = sum((-cpu+cpl)*1/length(xu))*sin(a);
xp = [flip(panelLx),panelUx];
yp = [flip(panelLy),panelUy];
cp = [flip(cpl'),cpu'];
cpx = [flip(cplx),cpux];
cpy = [flip(cply),cpuy];

%end






  

  
 

 


