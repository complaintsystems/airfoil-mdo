% Two-dimensional potential flow solver
%for naca0012, try this input
%[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496],1

function [cl,xp,yp,dL,XB,YB,pan_span] = Potential_Solver_CDM(alpha,d_beta)

al = deg2rad(alpha);
p = [0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];%initialize using parsec
% Obtain values from PARSEC
[XB, YB] = Airfoil_Paramterization(p);
d_beta = [d_beta(172:end),d_beta(1:171)];
XB = flip(XB);
YB = flip(YB);
%create a mapping from local angle of attack to OML coordinates
YB_new = zeros(length(XB),1);   %mapped OML y coordinates
pMid = zeros(length(XB)-1,2);   %panel control point coordinates
PMidy = zeros(1,length(XB)-1);  %panel control point y coordinates
S_proj = zeros(1,length(XB)-1); %Projected length of each panel
phi = zeros(1,length(XB)-1);    %local angle of attack
S = zeros(1,length(XB)-1);      %actual length of each panel
rho_air = 1.225;                %density of air
    for i = 1:length(XB)-1      %from Parsec, initialize OML (NACA0012)
        S_proj(i) = (XB(i+1)-XB(i));    %calculate projected lenth
        phi(i) = atan2((YB(i+1)-YB(i)),(XB(i+1)-XB(i)));    %initialize local AOA
    end
phi = phi+d_beta;               %superpose initial AOA with AOA changes from structre code
    for i = 1:length(XB)-1
        YB_new(i+1) = S_proj(i)*tan(phi(i))+YB_new(i);  %use local AOA calculate 
        PMidy(i) = S_proj(i)*0.5*tan(phi(i))+YB_new(i); %coordinates of OML and 
        pMid(i,:) = [(XB(i+1)+XB(i))/2,PMidy(i)];       %panel control point
        S(i) = S_proj(i)/cos(phi(i));       %panel length
    end
nhat = [-sin(phi);cos(phi)];    %rotation matrix from global to local coordinates
Ni = length(pMid);              %initialize size of linear system
Nj = length(XB) - 1;
c = zeros(Ni+1,Nj+1);
a = zeros(Ni,Nj);       %aic
    for i = 1:Ni
         %find colocation point in linear system
        Co_x = cos(phi).*(pMid(i,1) - XB(1:end-1))' +sin(phi).*(pMid(i,2) - YB_new(1:end-1))';
        Co_y = -sin(phi).*(pMid(i,1) - XB(1:end-1))' +cos(phi).*(pMid(i,2) - YB_new(1:end-1))';
        eta_1 = atan2(Co_y,Co_x);
        eta_2 = atan2(Co_y,Co_x - S);
        c(i,1:end-1) = -1/(2*pi)*(eta_2 - eta_1);
        c(i,i) = 0.5;
        theta_1 = atan2((pMid(i,2)-YB_new(end)),(pMid(i,1)-XB(end)));
        theta_2 = atan2(pMid(i,2)-YB_new(end),pMid(i,1)-10000000*XB(end));
        c(i,Nj+1) = 1/(2*pi)*(theta_1 - theta_2);
    end
a(:,1) = c(1:end-1,1) - c(1:end-1,Nj+1);
a(:,end) = c(1:end-1,Nj) + c(1:end-1,Nj+1);
a(:,2:end-1) = c(1:end-1,2:end-2);
a = [a(end/2+1:end,:);a(1:end/2,:)];        %construct LHS of the lienar system
    for i = 1:Nj                            %denotes AIC of the solver. Is 
        a(i,:) =flip(a(i,:));               %solely based on local AOA
                                            %The matrix is manipulated to
                                            %meet requirment of structre
                                            %code
    end
%Source  (RHS) Using sources to complete the boundary condition. b is based
%solely on local AOA while sigma, the strength of sources are based on
%alpha
b = zeros(Ni,Nj);
    for i = 1:Ni
        Co_x = cos(phi).*(pMid(i,1) - XB(1:end-1))' +sin(phi).*(pMid(i,2) - YB_new(1:end-1))';
        Co_y = -sin(phi).*(pMid(i,1) - XB(1:end-1))' +cos(phi).*(pMid(i,2) - YB_new(1:end-1))';
        eta_1 = atan2(Co_y,Co_x);
        eta_2 = atan2(Co_y,Co_x - S);
        R12 = Co_x.^2 + Co_y.^2; 
        R22 = (Co_x - S).^2 + Co_y.^2;
        f = (Co_x.*log(R12) - (Co_x - S).*log(R22) + 2*Co_y.*...
                (eta_2 - eta_1));
        b(i,:) = 1/(4*pi)*f; 
    end
b(i,i) = 1/(2*pi)*Co_x(i)*log(R12(i));
b = [b(end/2+1:end,:);b(1:end/2,:)];
    for i = 1:Nj
        b(i,:) =flip(b(i,:));
    end
sigma = zeros(Ni,1);
    for i = 1:Ni
    	sigma(i) = [cos(al),sin(al)]*nhat(:,i);
    end
sigma = flip(sigma);
RHS = -b*sigma; 
mu = linsolve(a,RHS);
% Utilities, the requested matrix of output is given at the end
Q_t = zeros(length(mu)-1,1);
zeta = zeros(length(mu)-1,1);
Cp = zeros(length(mu)-1,1);
    for i = 1:length(mu)
        zeta(i) = (pMid(i,1)*cos(al) + pMid(i,2)*sin(al)) + mu(i);    
    end
S = [S,S(end)];
zeta = [zeta;2*zeta(end)-zeta(end-1)];
dL = zeros(1,length(mu));
    for i = 1:length(mu)      
        Q_t(i) = 2*(zeta(i) - zeta(i+1))/(S(i)+S(i+1));
        Cp(i) = 1 - Q_t(i)^2;
    end
cpy = Cp.*cos(phi(1:end)-al)';
cpx = Cp.*sin(phi(1:end)-al)';
cl = sum(cpy.*S(1:end-1)');
cd = -sum(cpx.*S(1:end-1)');
%local lift
dL = cpy*0.5*rho_air.*S(1:end-1)';
xp = pMid(:,1);
yp = pMid(:,2);
pan_span = S(1:end-1)';
ANS = [xp,yp,dL,pan_span];
end


