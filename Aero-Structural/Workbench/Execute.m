clear all
close all
clc

%global Coord_FEA

%addpath('C:/Users/Prateek/Documents/Repo/Integrated_Airfoil_Design/Aero-Structural/Aerodynamics') 
%addpath('C:/Users/Prateek/Documents/Repo/Integrated_Airfoil_Design/Aero-Structural/Structural/poly-data')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Workbench/Aerodynamics')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Workbench/Structural/poly-data')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Workbench/Structural/')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Workbench/Structural/Shape_Functions')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Workbench/Structural/Topology_optimization')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Workbench/Structural/Topology_optimization/functions')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Plot')


global Strain Sigma Centx Centy
alpha = 1;
d_beta = zeros(1,340);

[cl,xp,yp,dL,XB,YB,pan_span] = Potential_Solver_CDM(alpha,d_beta);



Coord = [XB YB];

pan_span_low = pan_span(1:(length(xp)/2+1),1);
pan_span_up = pan_span((length(xp)/2)+1:end,1);

dL_low = dL(1:(length(xp)/2+1),1);
dL_up = dL((length(xp)/2)+1:end,1);

X_low = Coord(1:(length(xp)/2+1),1);
Y_low = Coord(1:(length(xp)/2+1),2);


X_up = Coord((length(xp)/2)+1:end,1);
Y_up = Coord((length(xp)/2)+1:end,2);


Coord_FEA = [vertcat(X_up,X_low) vertcat(Y_up,Y_low)];

dL_FEA = [vertcat(dL_up,dL_low)];

pan_span_FEA = [vertcat(pan_span_up,pan_span_low)];

pan_span_FEA((length(xp)/2)+1,:) = [];

dL_FEA((length(xp)/2)+1,:) = [];

Coord_FEA((length(xp)/2)+1,:) = [];



% Generate FEA Mesh file

%% COORDINATE FILE DESCRIPTION

%{

Coord_FEA goes clockwise : Starting from LE towards TE and then downward
                           towards LE

Coord_panel_ctr_FEA_comp goes clockwise : Starting from LE towards TE and then downward
                              towards LE, contains x & y coordinates with
                              cpx and cpy

%}

mesh_gen(Coord_FEA(:,1),Coord_FEA(:,2));

%movefile ('airfoil.txt', 'C:\Users\Prateek\Documents\Repo\Integrated_Airfoil_Design\Aero-Structural\Structural\MESH')
movefile ('airfoil.txt', '/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Workbench/Structural/MESH')
[tri, vert] = FEA_MESH;


%% Aerodynamic and structural node alignment test

if Coord_FEA(1:end-1,1)-vert(1:length(Coord_FEA)-1,1) == 0
    msg = "Aerodynamic nodes and structural nodes are aligned"
end


%% Define fixed nodes

%-------------------------------SPAR NODES--------------------------------%
%%

[cav_x_1, cav_y_1, cav_z_1] = spar_1();
[cav_x_2, cav_y_2, cav_z_2] = spar_2();


fixed_nodes =(length(Coord_FEA):length(Coord_FEA)+length(cav_x_1)+length(cav_x_2)-1)';

   
%%



fixed_vert = zeros(length(fixed_nodes),2);

fixed_vert(:,1) = vert(fixed_nodes,1);
fixed_vert(:,2) = vert(fixed_nodes,2);

load_nodes = [1:size(Coord_FEA,1)];




%% Define load vertices



 load_nodes = 1:length(Coord_FEA)-1;
%load_nodes = 1:length(Coord_FEA)-1;   % "-1" because of LE node (0,0) removal


load_nodes = load_nodes';



%% Assigning loads to load vertices


load_vert_x = vert((load_nodes),1);
load_vert_y = vert((load_nodes),2);



%% Compute local reference coordinates at load_point



% Re-structure xp yp

xp_low = xp(1:(length(xp)/2+1),1);
yp_low = yp(1:(length(yp)/2+1),1);

xp_up = xp((length(xp)/2)+1:end,1);
yp_up = yp((length(xp)/2)+1:end,1);

xp_rst = vertcat(xp_up, xp_low);
yp_rst = vertcat(yp_up, yp_low);

xp_rst((length(xp)/2)+1,:) = [];
yp_rst((length(xp)/2)+1,:) = [];
 


load_vert = [load_vert_x load_vert_y  load_nodes(:,1)];




% make BC compatible for top opt sub-routine : 

BC = fixed_nodes';


%% Truncate load nodes
% dummy = load_nodes(end,1);
% load_nodes = vertcat(load_nodes,dummy);
% load_nodes(inter_x) = [];

%% Check fixed and load nodes

% if length(OML_load_vert) ~= length(load_nodes)
%     msg = 'Error! Truncation mismatch';
%     error(msg)
% else
%     msg = 'Plotting load nodes and fixed nodes'
% end


figure
trimesh(tri,vert(:,1),vert(:,2))
axis equal
xlabel 'X/C'
ylabel 'Y/C'
hold on
sz = 40;
scatter(fixed_vert(:,1), fixed_vert(:,2),'MarkerEdgeColor',[1 0 0],...
              'MarkerFaceColor',[1 0 0],...
              'LineWidth',1.5)
          axis equal 
hold on

scatter(load_vert(:,1),load_vert(:,2),'MarkerEdgeColor',[0 .5 .5],...
              'MarkerFaceColor',[0 1 0],...
              'LineWidth',1.5)
hold off



%%%%%%%%%%%%%%%%%%%%%%% CLEAN UP REDUNDANT VARIABLES%%%%%%%%%%%%%%%%%%%%%%%

clear Coord_Panel_ctr_up
clear Coord_ctr_ctr_low
clear fixed_nodes_1
clear fixed_nodes_2
clear fixed_nodes_3
clear fixed_nodes_4
clear fixed_nodes_12
clear fixed_nodes_34
clear C1
clear C2
clear C3
clear C11
clear C22
clear C33
clear CC1
clear CC2
clear CC3
clear load_elements
clear load_elements_tri
clear load_elements_tri_final
clear load_elements_tri_sorted
clear load_elements_tri_sorted_final
clear iax
clear iay
clear ibx
clear iby
clear idy
clear inter_x
clear inter_y
clear intersect_nodes
clear j
clear load_vert_x
clear load_vert_y
clear Lx
clear Ly
clear OML_node_End
clear OML_node_Start
clear p
clear S
clear sz
clear X_low
clear X_up
clear Y_up
clear Y_low
clear xp
clear xp_low
clear i
clear idx
clear yp
clear yp_low



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%OMLload_vert = [LOAD_VERT_X LOAD_VERT_Y XP YP dLX_FEA dLY_FEA LOAD NODES];



%% Apply weight to each cpx


% T2 :: Transformation matrix that transfers the load from aerodynamic mesh
% to structural nodes ---> Fx_node,F_y_node = N.* Cp1x,Cp1y

% Create matrix T2 


T2  =zeros(2*length(load_vert),2*length(load_vert));

% Populate diagonal elements

for i = 1:length(T2)
        T2(i,i) = 1;
end
 

V2 = diag(ones(1,length(T2)-2),-2);


T2 = T2 + V2;

T2(1,2*length(load_vert)-1) = 1; % Accounts for double x forces on LE Node by last panel  
T2(2,2*length(load_vert)) = 1;   % Accounts for double y forces on LE Node by last panel  



Load = zeros(2*length(dL_FEA),1);

Load(1:2:end) = 0;
Load(2:2:end) = dL_FEA;


F_ext= T2* Load;

Fx = F_ext(1:2:end);
Fy = F_ext(2:2:end);



load_vert(:,4) = Fx;
load_vert(:,5) = -1.*Fy;



  






%% Finite Element Analysis and Topology Optimization



[U,x_phy] = topology_optimization(tri,vert,BC,load_vert);


%% Perform Finite Element Analysis

%msg = 'Performing Finite Element Analysis'

% [U] = FEA_tutorial(tri,vert,BC,load_vert);
% 
% UX = U(171*2-1,1);
% UY = U(171*2,1);


% %% Specify optimization paramters
% 
% xval=x;
% m = 1;                                       % total number of constraints
% n=nel;
% epsimin = 1e-7;
% xold1   = xval;
% xold2   = xval;
% xmin    = 0.01*ones(n, 1);
% xmax    = 1*ones(n, 1);
% low     = xmin;
% upp     = xmax;
% c_mma   = 1000*ones(m, 1);
% d_mma   = 1*ones(m, 1);
% a0_mma  = 1;
% a_mma   = 0*ones(m, 1);
% outeriter=0;
% kkttol  = 1e-3;                               % Convergence tolerance
% kktnorm = kkttol + 1;
% kktcond=kktnorm;
% 
% %% Begin topology optimization
% 
% % while kktnorm > kkttol
% %     outeriter = outeriter + 1;
% %     x_phy = W_filt * x;   
% %     [comp, dc,U] = FEA(vert,tri,BC,load_nodes,cpx,cpy,x_phy);
% %     UY = U(171*2,1);
% %     
%     
% % %  if outeriter ==1
% % %      c0 = comp; % Vary value for feasible design 
% % %  end
% %  df0dx = W_filt * dc;
% %  f0val= -UY;
%  
%  %% Constraints
% %------------------------------MASS CONSTRAINT---------------------------%
% M_bound=0.5; 
% M=sum(diag(Area)*(W_filt*x));
% M_total=sum(Area);
% vol=M/M_total;
% %------------------------------VOLUME CONSTRAINT--------------------------%
% fval(1)= M/M_total - M_bound;
% dfdx(:,1)=( 1/M_total)*Area;
% 
% %------------------------------COMPLAINCE CONSTRAINT----------------------%
% 
% %  fval(2)= c0-comp;
% %  dfdx(:,2)= -df0dx;
% 
% %-------------------------------CG CONSTRAINT-----------------------------%
% %COG = COG_con(x_phy);
% 
% %% MMA OPTIMIZATION SUB-ROUTINE
%     
%     [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,low,upp] = mmasub(m,n,outeriter,x,xmin,xmax,xold1,xold2,f0val,df0dx,fval',dfdx',low,upp,a0_mma,a_mma,c_mma,d_mma);
%     
%     % Updated vectors
%     
%     xold2 = xold1; 
%     xold1 = x;
%     x = xmma;
%     
%     % Perform kkt condition check
%     
%  [residu,kktnorm,residumax] = kktcheck(m,n,xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,xmin,xmax,df0dx,fval',dfdx',a0_mma,a_mma,c_mma,d_mma);
%    save('rhoblock.mat','x_phy');
%    disp([' ************************************* ']);
%    disp([' outiter: ' sprintf('%4i', outeriter) ' kktnorm: ' sprintf('%6.4e', kktnorm) ...
%         ' f_snap: ' sprintf('%10.7e', f0val) ' VolFrac: ' sprintf('%5.3f', M/M_total) ]);
%    disp([' ************************************* ']);
% domain(vert,tri,x_phy,zeros(neq,1),0,1,[],[],[],[],penal);
% end 
% x_phy;



for e=1:nel
elXY=vert(tri(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate
Centx(e,1) = (X(1) + X(2) + X(3))/3;
Centy(e,1) = (Y(1) + Y(2) + Y(3))/3;
end























%% Post process stress field

Stress = Sigma';


%#########################  VON-MISSES-STRESS #############################


% Sigma_VM = sqrt(Stress(:,1).^2 + Stress(:,2).^2 -Stress(:,1).*Stress(:,2) ...
%     + 3.* Stress(:,3).^2);
% 
% figure
% xcent = linspace(min(Centx),max(Centx));
% ycent = linspace(min(Centy),max(Centy));
%  [XI YI] = meshgrid(xcent,ycent);
%  ZI = griddata(Centx,Centy,Sigma_VM(:,1),XI,YI);
%  contourf(XI,YI,ZI,500,'LineStyle', 'none')
%  axis 'equal'
%  colormap(jet)
%  colorbar


%U = U';
%% Transformating matrix

T1 = zeros(length(dL),2*length(dL));
%T1 = zeros(4,8);

for i = 1:size(T1,1)
    for j =2*i
        T1(i,j) = 0.5;
    end
end
 
for i = 1:size(T1,1)
    idx(i,1) = find(T1(i,:)==0.5);
end
  
idx(1) = [];


for i = 1:size(T1,1)-1
    T1(i,idx(i)) = 0.5;
end


T1(size(T1,1),2) = 0.5;




U_OML = U(1:2*length(dL_FEA),1);

B = T1*U_OML;

%d_beta = B./pan_span_FEA;

d_beta = B;

d_beta = d_beta';







% %% Perform post-processing 
% 
% % Extract Ux and Uy components
% 
% U = U';
% 
% Ux = U(1:2:end);          
% Uy = U(2:2:end); 
% 
% Ux = Ux';
% Uy = Uy';
% 
% 
% deform_x = Ux(:,1) + vert(:,1);
% deform_y = Uy(:,1) + vert(:,2);
% 
% 
% D = [deform_x deform_y];
% 
% % figure
% % trimesh(tri,D(:,1),D(:,2))
% % axis equal
% % 
% % hold on
% % scatter(vert(:,1),vert(:,2))
% % axis equal
% 
% 
% %% Post Processing :: Displacement
% 
% figure
% subplot(2,1,1)
% plot(D(1:length(Coord_FEA)-1,1),D(1:length(Coord_FEA)-1,2), 'lineWidth',2)
% axis equal 
% hold on
% plot(vert(1:length(Coord_FEA)-1,1),vert(1:length(Coord_FEA)-1,2), 'lineWidth',2)
% grid minor
% hold off
% 
% subplot(2,1,2)
% plot(D(1:length(Coord_FEA)-1,1),D(1:length(Coord_FEA)-1,2), 'lineWidth',2)
% axis equal 
% hold on
% plot(vert(1:length(Coord_FEA)-1,1),vert(1:length(Coord_FEA)-1,2), 'lineWidth',2)
% grid minor
% hold off
% axis([0.35 1 -0.1 0.1]);
% legend('Deformed', 'Original')
% 
% 
% 
%  figure
% 
% [CS,h]=tricontf(D(:,1),D(:,2),tri,Ux);
% axis equal
% 
% axis([0 1 -0.5 0.5]);
% colormap(jet)
% c = colorbar;
% c.Label.String = '(m)';
% title ('Horizontal Displacement contours ')
% 
% figure
% 
% [CS,h]=tricontf(D(:,1),D(:,2),tri,Uy);
% axis equal
% 
% axis([0 1 -0.5 0.5]);
% colormap(jet)
% c = colorbar;
% c.Label.String = '(m)';
% title ('Vertical Displacement contours ')




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    





