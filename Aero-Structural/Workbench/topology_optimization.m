function [U,x_phy] = topology_optimization(tri,vert,BC,load_vert)
global volFrac Centx Centy sigma

addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Structural/Topology_optimization/functions')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Structural/Topology_optimization')
%% Specify nodes at which pressure act

load_nodes = load_vert(:,3);

%% Specify forces


cpx = load_vert(:,4);
cpy = load_vert(:,5);


%% Discretization parameters

nnodes=size(vert,1);                        % total number of nodes
nel = size(tri,1);                          % total number of elements
ndof = 2;                                   % degrees of freedom per node
neq=nnodes*2;                               % total number of degrees of freedom

%% Volume fraction and penalization

volFrac = 0.50;
x(1:nel,1) = volFrac;
penal=3;

%% Compute element aspect ratio 
for e=1:nel
elXY=vert(tri(e,:),:);                      % elemental nodal coordinates
X=elXY(:,1);                                % elemnt X coordinate
Y=elXY(:,2);                                % element Y coordinate
a=sqrt((Y(1)-Y(2))^2+(X(1)-X(2))^2);
b=sqrt((Y(2)-Y(3))^2+(X(2)-X(3))^2);
c=sqrt((Y(1)-Y(3))^2+(X(1)-X(3))^2);
s=(a+b+c)/2;
AR(e,1)=a*b*c/(8*(s-a)*(s-b)*(s-c));
Area(e,1)=X(1)*Y(2)+X(2)*Y(3)+X(3)*Y(1)-X(1)*Y(3)-X(2)*Y(1)-X(3)*Y(2);
end

%%  Compute density filter parameters
rmin = s;                                    % min radius equals the element perimeter
W_filt = density_filter(vert,tri,rmin,nel);    

%% Specify optimization paramters

xval=x;
m = 1;                                       % total number of constraints
n=nel;
epsimin = 1e-7;
xold1   = xval;
xold2   = xval;
xmin    = 0.01*ones(n, 1);
xmax    = 1*ones(n, 1);
low     = xmin;
upp     = xmax;
c_mma   = 1000*ones(m, 1);
d_mma   = 1*ones(m, 1);
a0_mma  = 1;
a_mma   = 0*ones(m, 1);
outeriter=0;
kkttol  = 1e-3;                               % Convergence tolerance
kktnorm = kkttol + 1;
kktcond=kktnorm;

%% Begin topology optimization

while kktnorm > kkttol
    outeriter = outeriter + 1;
    x_phy = W_filt * x;   
    [comp, dU,U] = FEA(vert,tri,BC,load_nodes,cpx,cpy,x_phy);
 if outeriter ==1
     c0 = comp; % Vary value for feasible design 
 end
 df0dx = -1*W_filt * dU;
 f0val= -1*U(171*2,1);
 
 %% Constraints
%------------------------------MASS CONSTRAINT---------------------------%
M_bound=0.5; 
M=sum(diag(Area)*(x));
M_total=sum(Area);
vol=M/M_total;
%------------------------------VOLUME CONSTRAINT--------------------------%
fval(1)= M/M_total - M_bound;
dfdx(:,1)=( 1/M_total)*Area;

%------------------------------COMPLAINCE CONSTRAINT----------------------%

%  fval(2)= c0-comp;
%  dfdx(:,2)= -df0dx;

%-------------------------------CG CONSTRAINT-----------------------------%
%COG = COG_con(x_phy);

%% MMA OPTIMIZATION SUB-ROUTINE
    
    [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,low,upp] = mmasub(m,n,outeriter,x,xmin,xmax,xold1,xold2,f0val,df0dx,fval',dfdx',low,upp,a0_mma,a_mma,c_mma,d_mma);
    
    % Updated vectors
    
    xold2 = xold1; 
    xold1 = x;
    x = xmma;
    
    % Perform kkt condition check
    
 [residu,kktnorm,residumax] = kktcheck(m,n,xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,xmin,xmax,df0dx,fval',dfdx',a0_mma,a_mma,c_mma,d_mma);
   save('rhoblock.mat','x_phy');
   disp([' ************************************* ']);
   disp([' outiter: ' sprintf('%4i', outeriter) ' kktnorm: ' sprintf('%6.4e', kktnorm) ...
        ' f_snap: ' sprintf('%10.7e', f0val) ' VolFrac: ' sprintf('%5.3f', M/M_total) ]);
   disp([' ************************************* ']);
domain(vert,tri,x_phy,zeros(neq,1),0,1,[],[],[],[],penal);
end 
x_phy;



for e=1:nel
elXY=vert(tri(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate
Centx(e,1) = (X(1) + X(2) + X(3))/3;
Centy(e,1) = (Y(1) + Y(2) + Y(3))/3;
end

end

