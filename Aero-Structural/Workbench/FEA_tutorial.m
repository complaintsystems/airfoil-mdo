function [U,strain,sigma] = FEA_tutorial(tri,vert,BC,load_vert);

LE = tri;
XYZ = vert;

global Strain Sigma Centx Centy

load_nodes = load_vert(:,3);



cpx = load_vert(:,4);
cpy = load_vert(:,5);

%% Compute element centeroid ( Gauss ) point

for e=1:length(LE)
elXY=XYZ(LE(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate
Centx(e,1) = (X(1) + X(2) + X(3))/3;
Centy(e,1) = (Y(1) + Y(2) + Y(3))/3;
end


% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
volfrac=0.4;
penal=3;

neq=nnodes*2;
% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
ndof=2;
neq=nnodes*2;

volFrac=0.3;
x(1:nel,1) = volFrac;

% Specify density distribution
%x=ones(nel,1);
penal=3;

%% SPECIFY NODAL PARAMETERS

ptx = load_nodes*2-1;
pty = load_nodes*2;


% boundary conditions

alldofs=1:neq;
fixed_nodes=BC';


fixeddofs=[fixed_nodes.*2-1 fixed_nodes.*2];
fixeddofs=sort(fixeddofs);
freedofs=setdiff(alldofs,fixeddofs);
nbc=size(fixeddofs,2);


% Newton-Raphson Convergence Criterion
epsilon=1e-9;
force_ctr=0;
const=5*100;
%for step=1:10
    %step;
    force_ctr=force_ctr+1;
% Initialize force distribution
Fext=zeros(neq,1);


%% Define external pressure forces at nodes


Fext(ptx,1) =  cpx;
Fext(pty,1) =  cpy;
  
  




% Specify Starting Displacememnt
U = zeros(neq,1);
%norm_res=1;
sigma = [];
strain = ones(3,1);
[dFint_dX,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal); 
 residual=Fext-Fint;
 norm_res=norm(residual(freedofs,1));
while norm_res>epsilon

delta_u=KT(freedofs,freedofs)\residual(freedofs);
U(freedofs)=U(freedofs)+delta_u;
[dFint_dX,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal);
residual=Fext-Fint;
norm_res=norm(residual(freedofs,1));
end

%end
%**************************************************************************
mes=1;
den=0;
% domain(XYZ,LE,x,U,0,1,[],[],[],[],penal);

%% Compliance
comp = U' * KT * U ;

%% sensitivity evaluation

kff=KT(freedofs,freedofs);

lambda = zeros(neq,1);
lambdaf=-Fext(freedofs,1)'/kff;
lambda(freedofs,1)=lambdaf';

for IE=1:nel
    
    IDOF=zeros(1,6);
    for I=1:3
        II=(I-1)*ndof+1;
        IDOF(II:II+1)=(LE(IE,I)-1)*ndof+1:(LE(IE,I)-1)*ndof+2;
    end

    
    dc(IE,:)=lambda(IDOF,1)'*dFint_dX(IDOF,IE);

    
end
end

