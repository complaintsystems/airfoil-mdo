%%%%%%%%%%%%%%%%%%%%%%%% ELEMENTAL STIFFNESS MATRIX%%%%%%%%%%

function [dfint_dX,fint,kgeo,kmat,sigma]=hyper2d(e,u,XYZ,LE,nel,x,penal)

%global sigma

ngp=1; % no. of gauss points in each direction
nen=3;
NDOF=2;
IE=e;

ndof=2;
ind2voit=[1; 4; 3];

elXY=XYZ(LE(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate

IDOF=zeros(1,6);
for I=1:3
II=(I-1)*NDOF+1;
IDOF(II:II+1)=(LE(IE,I)-1)*NDOF+1:(LE(IE,I)-1)*NDOF+2;
end
ue=u(IDOF); % elemnt nodal displacements
ux=[ue(1) ue(3) ue(5)]; % X displacements
uy=[ue(2) ue(4) ue(6)]; % Y displacements

% Ccur=[X+ux' Y+uy']; % current coordinates
Ccur=[X Y]; % current coordinates
[w,gp] = lgwt(ngp); % weights and integration points

 kgeo=zeros(nen*ndof,nen*ndof); % stiffness matrix
 kmat=zeros(nen*ndof,nen*ndof);
 fint=zeros(nen*ndof,1);        % internal force
 dfint_dX=zeros(nen*ndof,1);
 
 
 for i=1:ngp
   for j=1:ngp
       eta = gp(i);            
       psi = gp(j);
 
       [dNdx,Bx, detJx]     = SHAPEL2D(Ccur);     % derivative of the shape functions                   
         strain=Bx*ue ;  
        
  %E_0 = 1;    
  E_0 = 50 * 10^1;
  E=x(e,1)^penal*E_0;
  

  
  nu = 0.33;
      D    = E/(1-nu^2).* [1        nu 0 ;
                 nu 1 0;
                   0 0 (1-nu)/2];
               
               
  E=x(e,1)^penal*E_0;     
  
  dE_dX = penal * x(e,1)^(penal-1)* E_0;
  
  dD_dX    = dE_dX/(1-nu^2).* [1        nu 0 ;
                 nu 1 0;
                   0 0 (1-nu)/2];
  dSigma_dX = dD_dX * strain;
  
  dfint_dX = dfint_dX + Bx'*dSigma_dX*detJx*w(i)*w(j);

sigma=D*strain;

fint = fint + Bx'*sigma*detJx*w(i)*w(j);
      kmat = kmat + w(i)*w(j)*Bx'*D*Bx*detJx;   % material element matrix        

      
      
   end
end

end