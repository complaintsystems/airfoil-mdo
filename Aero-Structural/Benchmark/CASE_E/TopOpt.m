%function main_2

function [U,Centx, Centy] = TopOpt(tria,vert,BC,load_suc,load_pr,load_node_suc,load_node_pr);

% clear all
% clc
% close all
% 
% 
% addpath('meshGen_Explicit/')
% addpath('functions/')

%% Airfoil mesh generation

global  XYZ LE  outeriter h d  BC Centx nel Area Mass U load_node_pr load_node_suc

global Sigma load_suc load_pr


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------- MESH PARAMETERS -----------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hfun = +0.3;   % Edge length
% [t,p] = meshGen(hfun);
% 
% figure(1)
% trimesh (t,p(:,1),p(:,2));
% grid minor
% axis equal
% xlabel 'X/C'
% ylabel 'Y/C'
% title 'Baseline profile'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------- BOUNDARY CONDITIONS -------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Left_edge_nodes = find(p(:,1)==0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%--------------------------- LOADING CONDITIONS -------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for i = 1:length(p)
%     if p(i,1) == max(p(:,1)) && p(i,2) == 0
%         load_point_node = i;
%     end
% end
% 
% loadp = -1;


% BC = Left_edge_nodes;
%  
 


LE = tria;
XYZ = vert;
 


%% Discretization parameters

nnodes=size(XYZ,1);
nel = size(LE,1);
nel=size(LE,1);
penal=3;
ndof = 2;
neq=nnodes*2;

%% Volume fraction

volFrac = 0.3;
x(1:nel,1) = volFrac;

%% Element aspect ratio 
for e=1:nel
elXY=XYZ(LE(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate
a=sqrt((Y(1)-Y(2))^2+(X(1)-X(2))^2);
b=sqrt((Y(2)-Y(3))^2+(X(2)-X(3))^2);
c=sqrt((Y(1)-Y(3))^2+(X(1)-X(3))^2);
s=(a+b+c)/2;
AR(e,1)=a*b*c/(8*(s-a)*(s-b)*(s-c));
Area(e,1)=X(1)*Y(2)+X(2)*Y(3)+X(3)*Y(1)-X(1)*Y(3)-X(2)*Y(1)-X(3)*Y(2);
end

for e=1:nel
elXY=XYZ(LE(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate
Centx(e,1) = (X(1) + X(2) + X(3))/3;
Centy(e,1) = (Y(1) + Y(2) + Y(3))/3;
end




%% Density filter calculation
rmin = s;
W_filt = density_filter(XYZ,LE,rmin,nel);
 
%% Optimization paramters


xval=x;
m = 1;  % # of constraints
n=nel;
epsimin = 1e-7;
xold1   = xval;
xold2   = xval;
xmin    = 0.01*ones(n, 1);
xmax    = 1*ones(n, 1);
low     = xmin;
upp     = xmax;
c_mma   = 1000*ones(m, 1);
d_mma   = 1*ones(m, 1);
a0_mma  = 1;
a_mma   = 0*ones(m, 1);
outeriter=0;
kkttol  = 1e-4;
kktnorm = kkttol + 1;
kktcond=kktnorm;
 
while kktnorm > kkttol
    outeriter = outeriter + 1;
    x_phy = W_filt * x;
   
  %% FINITE ELEMENT ANALYSIS
    
 [comp, dc,U] = FEA_sens(x_phy);
 if outeriter ==1
     c0 = comp; % Vary value for feasible design 
 end
 df0dx = W_filt * dc;
 f0val= comp;
 
 
%% Constraints
%------------------------------MASS CONSTRAINT---------------------------%
M_bound=0.5; 
M=sum(diag(Area)*(W_filt*x));
M_total=sum(Area);
vol=M/M_total;
%------------------------------VOLUME CONSTRAINT--------------------------%
fval(1)= M/M_total - M_bound;
dfdx(:,1)=( 1/M_total)*Area;

%------------------------------COMPLAINCE CONSTRAINT----------------------%

% fval(2)= c0-comp;
% dfdx(:,2)= -df0dx;

%-------------------------------CG CONSTRAINT-----------------------------%
%COG = COG_con(x_phy);


% % 
%  fcn = @COG_con;
% 
% 
% 
% fval(2) = COG;
% dfdx(:,2) = fwd(fcn,x_phy,1e-03);

%% MMA OPTIMIZATION SUB-ROUTINE
    
    [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,low,upp] = mmasub(m,n,outeriter,x,xmin,xmax,xold1,xold2,f0val,df0dx,fval',dfdx',low,upp,a0_mma,a_mma,c_mma,d_mma);
    
    % Updated vectors
    
    xold2 = xold1; 
    xold1 = x;
    x = xmma;
    
    % Perform kkt condition check
    
   [residu,kktnorm,residumax] = kktcheck(m,n,xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,xmin,xmax,df0dx,fval',dfdx',a0_mma,a_mma,c_mma,d_mma);
   save('rhoblock.mat','x_phy');
   disp([' ************************************* ']);
   disp([' outiter: ' sprintf('%4i', outeriter) ' kktnorm: ' sprintf('%6.4e', kktnorm) ...
        ' f_snap: ' sprintf('%10.7e', f0val) ' VolFrac: ' sprintf('%5.3f', M/M_total) ]);
   disp([' ************************************* ']);
domain(XYZ,LE,x_phy,zeros(neq,1),0,1,[],[],[],[],penal);
end 

x_phy;
% domain(XYZ,LE,x_phy,zeros(neq,1),0,1,[],[],[],[],penal);


%% Computing element mass

% Mass = zeros(nel,1);
% 
% for e = 1:nel
%     Mass(e,1) = x_phy(e,1).*Area(e,1);
% end
% 
% 
% Weight = Mass.* 9.81;p;

%% Compute total CG





% cgx = zeros(nel,1);
% cgy = zeros(nel,1);
% 
% for e = 1:nel
%     cgx(e,1) = Weight(e,1).*Centx(e,1);
%     cgy(e,1) = Weight(e,1).*Centy(e,1);
% end
% 
% CGX = sum(cgx)/sum(Weight);
% CGY = sum(cgy)/sum(Weight);

% % Plot element centeroid
% trimesh (t,p(:,1),p(:,2));
% hold on
% scatter(Centx,Centy)
% hold on
% scatter(CGX,CGY,'filled')
% hold off
% 
% COG = [CGX CGY];







