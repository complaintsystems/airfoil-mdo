
%% FORWARD DIFFERENCE APPROXIMATOR

% Inputs : r - Perturbation
%         fcn - Perturbation function
%          x  - Design variable


%This function returns the gradient vector of the function fcn
% using a forward difference approximation. Inputs are the function @fcn,
%x=[x1,..xn] and the purturbation size r.

%}
function [gradf]=fwd(fcn,x_phy,r)

pf=fcn(x_phy);

for i=1:length(x_phy)

    dx=abs(r*x_phy(i));
    
    xtemp1=x_phy;
    xtemp1(i)=xtemp1(i)+dx;

    pfpx(i)=(fcn(xtemp1)-pf)/(dx);
    
end

gradf=pfpx;

%end


