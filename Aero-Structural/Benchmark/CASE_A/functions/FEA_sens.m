function [comp,dc,U] = FEA_sens(x)

global   XYZ LE  outeriter h d  BC load_point_node loadp

global Sigma


% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
volfrac=0.3;
penal=3;

neq=nnodes*2;
% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
ndof=2;
neq=nnodes*2;


% Specify density distribution
%x=ones(nel,1);
penal=3;

%% SPECIFY NODAL PARAMETERS

pt = load_point_node*2;


% boundary conditions

alldofs=1:neq;
fixed_nodes=BC';


fixeddofs=[fixed_nodes.*2-1 fixed_nodes.*2];
fixeddofs=sort(fixeddofs);
freedofs=setdiff(alldofs,fixeddofs);
nbc=size(fixeddofs,2);

% for i=1:size(freedofs,2)
%     if freedofs(:,i)==end_node
%         count=i;
%     end
%    
% end

% Newton-Raphson Convergence Criterion
epsilon=1e-9;
force_ctr=0;
const=5*100;
%for step=1:10
    %step;
    force_ctr=force_ctr+1;
% Initialize force distribution
Fext=zeros(neq,1);

%% Define external pressure forces at nodes


Fext(pt,1) = loadp;
  
  
% Specify Starting Displacememnt
U = zeros(neq,1);
%norm_res=1;
[dFint_dX,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal); 
 residual=Fext-Fint;
 norm_res=norm(residual(freedofs,1));
while norm_res>epsilon

delta_u=KT(freedofs,freedofs)\residual(freedofs);
U(freedofs)=U(freedofs)+delta_u;
[dFint_dX,KT,Fint]= stiff(nel,nnodes,ndof,LE,XYZ,U,x,penal);
residual=Fext-Fint;
norm_res=norm(residual(freedofs,1));
end

%end
%**************************************************************************
mes=1;
den=0;
% domain(XYZ,LE,x,U,0,1,[],[],[],[],penal);

%% Compliance
comp = U' * KT * U ;

%% sensitivity evaluation

kff=KT(freedofs,freedofs);

lambda = zeros(neq,1);
lambdaf=-Fext(freedofs,1)'/kff;
lambda(freedofs,1)=lambdaf';

for IE=1:nel
    
    IDOF=zeros(1,6);
    for I=1:3
        II=(I-1)*ndof+1;
        IDOF(II:II+1)=(LE(IE,I)-1)*ndof+1:(LE(IE,I)-1)*ndof+2;
    end

    
    dc(IE,:)=lambda(IDOF,1)'*dFint_dX(IDOF,IE);

    
end
end

