% MAIN script for topology optimization
clear all
clc 
close all

global Sigma 

%% MESH Generation 

addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_A/MeshGen_Explicit/poly-data/') 
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_A/MeshGen_Explicit/')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_A/functions/')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_A/')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Plot/')


l = 12;
h = 2;

hfun = +0.15;   % Edge length


% Generate .msh file 
node_Gen(l,h)

movefile ('mesh.txt','/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_A/MeshGen_Explicit/poly-data/')

% Generate the mesh 
[tria, vert] = meshGen(hfun);

%% Define boundary conditons

A =  find(vert(:,2)==0);
%B = find(vert(:,1) == l & vert(:,2) == 0);

BC = A;
%BC = horzcat(A,B);

fixed_node = vert(A,:);
%fixed_node2 = vert(B,:);

%fixed_node = vertcat(fixed_node1, fixed_node2);


%% Define load point and node 

% load_node1 = find(vert(:,1) == 0 & vert(:,2) == 2);
% load_node2 = find(vert(:,1) == l & vert(:,2) == 2);
% load_node3 = find(vert(:,1) == 0.25*l & vert(:,2) == 2);
% load_node4 = find(vert(:,1) == 0.25*l & vert(:,2) == 2);

load_node = find(vert(:,2) == 2);

load_vert_x = vert(load_node,1);
load_vert_y = vert(load_node,2);

load_vert = [load_vert_x, load_vert_y];

%% Plot mesh with BC and load point

trimesh(tria,vert(:,1), vert(:,2))
hold on
scatter(load_vert(:,1), load_vert(:,2))
hold on 
scatter(fixed_node(:,1), fixed_node(:,2))
hold off
axis equal 


%% Prepare inputs for topology optimization 

load = 1;





%% Topology Optimization

[U,Centx, Centy] = TopOpt(tria,vert,BC,load,load_node);
%% Post Process displacement

Uy = U(2:2:end);
Ux = U(1:2:end);

deform_x = Ux(:,1) + vert(:,1);
deform_y = Uy(:,1) + vert(:,2);

D = [deform_x, deform_y];

figure

trimesh(tria,D(:,1), D(:,2))
axis equal 

[CS,h]=tricontf(D(:,1),D(:,2),tria,Ux);
axis equal

%axis([0 1 -0.5 0.5]);
colormap(jet)
c = colorbar;
c.Label.String = '(m)';
title ('Horizontal Displacement contours ')

figure

[CS,h]=tricontf(D(:,1),D(:,2),tria,Uy);
axis equal

%axis([0 1 -0.5 0.5]);
colormap(jet)
c = colorbar;
c.Label.String = '(m)';
title ('Vertical Displacement contours ')


%% Post Processing :: Stress

Stress = Sigma';


% Compute von-Misses Stress

Sigma_VM = sqrt(Stress(:,1).^2 + Stress(:,2).^2 -Stress(:,1).*Stress(:,2) ...
    + 3.* Stress(:,3).^2);



figure
xcent = linspace(min(Centx),max(Centx));
ycent = linspace(min(Centy),max(Centy));
 [XI YI] = meshgrid(xcent,ycent);
 ZI = griddata(Centx,Centy,Sigma_VM(:,1),XI,YI);
 contourf(XI,YI,ZI,500,'LineStyle', 'none')
 axis 'equal'
 colormap(jet)
 colorbar













