% MAIN script for topology optimization
clear all
clc 
close all

%% MESH Generation 

addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_C//MeshGen_Explicit/poly-data/') 
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_C/MeshGen_Explicit/')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_C/functions/')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_C/')

l = 4;
h = 1;

hfun = +0.03;   % Edge length


% Generate .msh file 
node_Gen(l,h)

movefile ('mesh.txt','/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Benchmark/CASE_B/MeshGen_Explicit/poly-data/')

% Generate the mesh 
[tria, vert] = meshGen(hfun);



%% Define boundary conditons

A =  find(vert(:,1)==0);
%B = find(vert(:,1) == 0 & vert(:,2) == h);

BC = A;
%BC = horzcat(A,B);

fixed_node = vert(A,:);
%fixed_node2 = vert(B,:);

%fixed_node = vertcat(fixed_node1, fixed_node2);


%% Define load point and node 

% load_node1 = find(vert(:,1) == 0 & vert(:,2) == 2);
% load_node2 = find(vert(:,1) == l & vert(:,2) == 2);
% load_node3 = find(vert(:,1) == 0.25*l & vert(:,2) == 2);
% load_node4 = find(vert(:,1) == 0.25*l & vert(:,2) == 2);

load_node = 2;

load_vert_x = vert(load_node,1);
load_vert_y = vert(load_node,2);

load_vert = [load_vert_x, load_vert_y];

%% Plot mesh with BC and load point

trimesh(tria,vert(:,1), vert(:,2))
hold on
scatter(load_vert(:,1), load_vert(:,2))
hold on 
scatter(fixed_node(:,1), fixed_node(:,2))
hold off
axis equal 


%% Prepare inputs for topology optimization 

load = -1;


%% Topology Optimization

U = TopOpt(tria,vert,BC,load,load_node);
%% Post Process displacement

Uy = U(2:2:end);
Ux = U(1:2:end);

deform_x = Ux(:,1) + vert(:,1);
deform_y = Uy(:,1) + vert(:,2);

deform = [deform_x, deform_y];

figure

trimesh(tria,deform(:,1), deform(:,2))
axis equal 





















