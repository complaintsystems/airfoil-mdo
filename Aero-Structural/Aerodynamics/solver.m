function [cl,cd ,cmac,xp,yp,cp,cpx,cpy,COSINE]=solver(XB,YB,ALPHA)


%%----------------------------------------------------------------------%%
%%----------------------------------------------------------------------%%

if size(XB,1)~=1 || size(YB,1) ~=1
error ('Transpose coordinate arrays')
end

% Get L.E node value and index location
[XMIN,ILE]=min(XB);

% Get total number of grid nodes

MP1=size(XB,2);

% Total number of panels

M=MP1-1;

% Convert AOA from degrees to radians
ALPHA1=ALPHA*pi/180;

if (YB(1)~=0||YB(MP1)~=0)
error ('Coordinate input format error, Y(0) and Y(end) must be zero')
end

if (XB(1)~=XB(MP1)||YB(ILE)~=0)
error ('airfoil specification error')
end

% Re-transpose coordinates for aerodynamic solver
XB=XB';
YB=YB';

% Compute chord length
 c=XB(1)-XMIN;
 
 % Compute aerodynamic solver location
 XAC=XMIN+c/4;
 
 % Iniitalize RHS :: Unknown
 RHS=zeros(MP1,1);
 
 gamma=zeros(MP1,1);
 
  AN=zeros(MP1,MP1);
  AT=zeros(M,MP1);
  
  % Initialize " Panel-based" matrices
  
  THETA=zeros(M,1);
  X=zeros(M,1);
  Y=zeros(M,1);
  S=zeros(M,1);
SX=zeros(M,1);
SY=zeros(M,1);
SINE=zeros(M,1);
COSINE=zeros(M,1);

X=(XB([1:M])+XB([2:MP1]))/2;      % Mid span location in x

Y=(YB([1:M])+YB([2:MP1]))/2;       % Mid span location in y

SX=XB([2:MP1])-XB([1:M]); 
SY=YB([2:MP1])-YB([1:M]);
S=sqrt(SX.^2+SY.^2);



% Local AOA of each panel

THETA=atan2(SY,SX);

SINE=sin(THETA);

COSINE=cos(THETA);

for I=1:M
for J=1:M
if I==J
CN1(I,J)=-1.0;
CN2(I,J)=1.0;
CT1(I,J)=pi/2;
CT2(I,J)=pi/2;
else
XTEMP=X(I)-XB(J);
YTEMP=Y(I)-YB(J);
A=-XTEMP*COSINE(J)-YTEMP*SINE(J);
B=XTEMP^2+YTEMP^2;
TTEMP=THETA(I)-THETA(J);
C=sin(TTEMP);
D=cos(TTEMP);
E=XTEMP*SINE(J)-YTEMP*COSINE(J);
F=log(1+S(J)*(S(J)+2*A)/B);
G=atan2(E*S(J),B+A*S(J));
TTEMP=TTEMP-THETA(J);
P=XTEMP*sin(TTEMP)+YTEMP*cos(TTEMP);
Q=XTEMP*cos(TTEMP)-YTEMP*sin(TTEMP);
CN2(I,J)=D+Q*F/(2*S(J))-(A*C+D*E)*G/S(J);
CN1(I,J)=D*F/2+C*G-CN2(I,J);
CT2(I,J)=C+P*F/(2*S(J))+(A*D-C*E)*G/S(J);
CT1(I,J)=C*F/2-D*G-CT2(I,J);
end
end
end

% Compute INFLUENCE COEFFICIENTS
AN=[CN1(:,1),CN1(:,[2:M])+CN2(:,[1:M-1]),CN2(:,M);...
1,zeros(1,M-1),1];


AT=[CT1(:,1),CT1(:,[2:M])+CT2(:,[1:M-1]),CT2(:,M)];
RHS=[sin(THETA-ALPHA1);0];

% Compute circulation 
gamma=AN\RHS;

% Compute velocity
V=cos(THETA-ALPHA1)+AT*gamma;

% Compute pressure coefficient
cp=1-V.^2;

% Compute total circulation
G=S'*(gamma([1:M])+gamma([2:MP1]))/2;

% Compute lift coefficient
cl=4*pi*G/c;

%Compute pitching moment about aerodynamic center
%ISurf=sign([1:M]-ILE);ISurf(ILE)=1;
cmac=S'*(cp.*((X-XAC).*COSINE+Y.*SINE)/c^2);
xp=X;
yp=Y;

 dx=XB(2:MP1)-XB(1:MP1-1);
 dy=YB(2:MP1)-YB(1:MP1-1);
 cfx=cp'*dy;
 cfy=-cp'*dx;
 
 cd=cfx*cos(ALPHA1)+cfy*sin(ALPHA1);
 

 cpy = cp./COSINE;
 cpx = -cpy.*SINE;
 
 

 
 
 
 