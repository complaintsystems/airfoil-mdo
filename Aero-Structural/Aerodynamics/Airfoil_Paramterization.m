
%% PARSEC Airfoil paramterization

% PARSEC nomenclature

%{
p1  :: leading edge radius
p2  :: upper crest position in horizontal coordinates
p3  :: upper crest position in vertical coordinates
p4  :: upper crest curvature
p5  :: lower crest position in horizontal coordinates
p6  :: lower crest position in vertical coordinates
p7  :: lower crest curvature
p8  :: trailing edge offset in vertical offset
p9  :: trailing edge thickness
p10 :: trailing edge direction
p11 :: trailing edge wedge angle
%}

%% Candidate points

%NACA 0012 :: Default values
%p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496];

% Airfoil Candidate values

%p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101, -2.7791,9.2496];

%% Compute shape coefficients


function [X,Y] = Airfoil_Paramterization(p)

% clear all
% clc
% close all

%p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101,-2.7791,9.2496]


% Compute initial coefficients
a = parsec(p);
point=20;
n=(1/point);

% Enable higher resolution at LE
nn=n/8; 
nn1=n/10;

% suction surface coordinates
x1=1:-nn:0.35;
x2=0.35:-n:0.25;
x22 = 0.25:-nn1:0;
% x1=1:-nn:0.1;
% x2=0.1:-nn:0;

% pressure surface coordinates

x3=0:nn1:0.25;
x33 = 0.25:n:0.35;
x4=0.35:nn:1;
% x3=0:nn:0.1;
% x4=0.1:n:1;

xu = horzcat(x1,x2,x22);
xl = horzcat(x3,x33,x4);
% xu(1:length(x1))=x1;
% xu(length(x1)+1:length(x1)+length(x2))=x2;
% xu(length(x1)+1:length(x1)+length(x2)+length(x22))=x3;
% 
% xl(1:length(x3))=x3;
% xl(length(x3)+1:length(x3)+length(x3))=x33;
% xl(length(x3)+1:length(x3)+length(x33)+length(x4))=x4;
% % xl(length(x3)+1:length(x3)+length(x4))=x4;

X(1:length(xu))=xu;
X(length(xu)+1:length(xu)+length(xl))=xl;

X=X';

yu=a(1)*xu.^.5+a(2)*xu.^(1.5)+a(3)*xu.^(2.5)+a(4)*xu.^(3.5)+a(5)*xu.^(4.5)+a(6)*xu.^(5.5);
yl=-(a(7)*xl.^.5+a(8)*xl.^(1.5)+a(9)*xl.^(2.5)+a(10)*xl.^(3.5)+a(11)*xl.^(4.5)+a(12)*xl.^(5.5));
Y(1:length(yu),1)=yu;
Y(length(yu)+1:length(yu)+length(yl),1)=yl;


X=round(X,7);
Y= round(Y,7);

%% Rearrange coordinates in a " solver-compatible " format

% Convert from blunt to sharp T.E

Y(1)   = 0;
Y(end) = 0;

% Remove double LE point to define one shared node

 X(length(X)/2) = [];
 Y(length(Y)/2) = [];
 
Coord = [X Y];

% Remove repeated coordinate values 

for i = 1:(size(Coord,1))-5
    if Coord(i+1,1) == Coord(i,1)
        Coord(i+1,:) = [];
    end
end

X = Coord(:,1);
Y = Coord(:,2);

% scatter(X,Y)
% axis equal 






%% Plot results
% figure(1)
% plot(X,Y, 'lineWidth', 1, 'color','red')
% axis equal
% grid minor
%end





%% Airfoil coordinates write output control

% file = fopen('airfoil_coordinates.txt','w');
% fprintf(file,'%d %d\n',Coord');
% fclose(file);


