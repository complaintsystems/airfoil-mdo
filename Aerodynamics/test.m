clear all
close all
clc


p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101, -2.7791,9.2496];


[Coord] = Airfoil_Paramterization(p)


for i = 1:length(Coord)-3
    if Coord(i+1,1) == Coord(i,1)
        Coord(i+1,:) = [];
    end
end

Coord