% Two-dimensional potential flow solver


clear all
clc
close all

% NACA0012 Airfoil Coeffecients

p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101, -2.7791,9.2496];

% Obtain values from PARSEC

[XB, YB] = Airfoil_Paramterization(p);
XB = flip(XB);
YB = flip(YB);
% XB = XB';
% YB = YB';
     
ALPHA=4;
     
% Execute flow solver

[cl,cd,cmac,xp,yp,cp,THETA]=solver(XB,YB,ALPHA);

plot(xp,cp,'lineWidth',2)
xlabel 'X/C'
ylabel 'Cp'
grid minor


  

  
 

 


