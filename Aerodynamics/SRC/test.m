clear all
close all
clc


XB=[1 0.933 0.75 0.5 0.25 0.067 0 0.067 0.25 0.5 0.75 0.933 1];
YB=[0 -0.005 -0.017 -0.033 -0.042 -0.033 0 0.045 0.076 0.072 0.044 0.013 0];

ALPHA=8;

[cl,cmac,xp,yp,cp,V,gamma,AN]=airfoil(XB,YB,ALPHA);