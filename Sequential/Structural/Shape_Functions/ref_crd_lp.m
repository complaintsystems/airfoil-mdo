function[r,s] =  ref_crd_lp(OML_load_vert)

x1 = OML_load_vert(1,1);
x2 = OML_load_vert(1+1,1);
y1 = OML_load_vert(1,2);
y2 = OML_load_vert(1+1,2);
xp = OML_load_vert(1,3);
yp = OML_load_vert(1,4);

  B = [(xp-x1); (yp-y1)];
  
  A = [(x2-x1) (-x1); (y2-y1) (-y1)];
  
  Coeff = linsolve(A,B);
  
  
 r = Coeff(1);
 s = Coeff(2);
  
  
  
  
  





% %$ Construct the linear system 
% 
% % Construct B matrix
% 
% 
% 
% 
% 
% A = [-X(1,1)+X(2)  -X(1,1)+X(3); -Y(1,1)+Y(2,1)  Y(3)-Y(1)];
% B = [aero_node(1,1)-X(1,1); aero_node(2,1)-Y(1,1)];
% 
% % SOlve AX = B
% 
% 
% 
% Coeff = linsolve(A,B);
% 
% r = Coeff(1)
% s = Coeff(2)
end
