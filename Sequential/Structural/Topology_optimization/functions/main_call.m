function main_call
clear all
close all
clc
global XYZ LE rhoBlock1  outeriter  vol num BC

%% IMPORT GEOMETRY

% Nodal Coordinates

fileID = fopen('FULL_AIRFOIL_XYZ.TXT');
formatSpec=('%*s%*d%f%f%*d%*d%*d%*d');
XYZ=textscan(fileID,formatSpec);
[XYZ_1 XYZ_2]=XYZ{:};
XYZ=[XYZ_1 XYZ_2];

% element conenctivity

fileID = fopen('FULL_AIRFOIL_LE.TXT');
formatSpec=('%*s%*d%*s%d%d%d');
LE=textscan(fileID,formatSpec);
[LE_1 LE_2 LE_3]=LE{:};
LE=[LE_1 LE_2 LE_3];

%% Boundary Conditions

fileID=fopen('FULL_AIRFOIL_BC.TXT');
  formatSpec=('%*s%d%*f%*f%*f%*f%*f%*f');
  BC=textscan(fileID,formatSpec);
   BC=BC{:};
BC([2 4 1 7 10 8],:)=[];
%     BC1=[41:43 45:46 284:290]; % if you want break in the boundary
  BC1=[34:43 45:53 277:298];  % if you want full boundary
   BC=[BC1';BC];% FOR JAN_20

% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
volfrac=0.4;
penal=3;
NDOF=2;
neq=nnodes*2;
% Emin=0.001;

% Plotting the airfoil design domain
mes=1;den=0;U=zeros(neq,1);
domain(XYZ,LE,[],U,mes,den,[],[],[],[],penal)

% Specify the deflection of end_node required
U_DEF=19; 


% Calculate Area of each element

for e=1:nel
elXY=XYZ(LE(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate
a=sqrt((Y(1)-Y(2))^2+(X(1)-X(2))^2);
b=sqrt((Y(2)-Y(3))^2+(X(2)-X(3))^2);
c=sqrt((Y(1)-Y(3))^2+(X(1)-X(3))^2);
s=(a+b+c)/2;
AR(e,1)=a*b*c/(8*(s-a)*(s-b)*(s-c));
Area(e,1)=X(1)*Y(2)+X(2)*Y(3)+X(3)*Y(1)-X(1)*Y(3)-X(2)*Y(1)-X(3)*Y(2);
end




end_node=44*2; % for airfoil_final

%stored values
%  load('rhoblock.mat')
%  rhoBlock1=reshape(rhoBlock1,nel,1);
%   x(1:nel,1)=rhoVec;


 


% density distribution
   x(1:nel,1)=volfrac;




% rmin calculation
rmin=4; % 




% Filter calculation
 W_filt = density_filter(XYZ,LE,rmin,nel);
 
% Filter Density Values
x_tilde=W_filt*x;


% specify range of displcemnets
u_marker=[5 15];
n_marker=2;

% cOMPLAINCE CONSTRAINT CALCULATION
% u_comp=10;
 
% [comp,dc_comp]= NR_COMP(x_tilde,penal,neq,u_comp);

% C0=comp;




% FORCE & DISPLACEMENT CONSTRAINT CALCULATION
%for i=1:1
    
    %if i==1
        
        
        [u_prev,theta_prev,u_input,theta_input,dc2(:,i),U,f(:,i),df(:,i)]= newton_raphson(i,x_tilde,penal,u_marker(i),[],[],[],[]);
   % end
    %if i==2
        
        %[~,~,~,~,dc2(:,i),U,f(:,i),df(:,i)]= newton_raphson(i,x_tilde,penal,u_marker(i),u_input,theta_input,u_prev,theta_prev);
        
   % end
    
%end


% calculate constraint function
outeriter=0;
kkttol  = 1e-3;
kktnorm = kkttol + 1;
kktcond=kktnorm;


% MASS CONSTRAINT
M_bound=volfrac; 
M=sum(diag(Area)*(W_filt*x));
M_total=sum(Area);
vol=M/M_total;
% vol=vol/M_bound;

f0=f(1);
f_bound=0.1*f(1);

% OBJECTIVE FUNCTION
f0val= f(n_marker)/f0 ;
df0dx= df(:,n_marker)/f0;

% FORCE CONSTRAINT
fval(1)= f_bound/f0-f(1)/f0;
dfdx(:,1)= -df(:,1)/f0;

% VOLUME CONSTRAINT
fval(2)= M/M_total - M_bound;
dfdx(:,2)=( 1/M_total)*Area;

% DISPLACEMENT CONSTRAINT
fval(3)=1+(U(end_node))/U_DEF;
dfdx(:,3)=dc2(:,2)/U_DEF;

%COMPLIANCE CONSTRAINT
% fval(4)=-1-(U(end_node))/25; % get rid of this
% dfdx(:,4)=-dc2(:,2)/25;
fval(4)=1-comp/C0;
dfdx(:,4)=-dc_comp/C0;



% % DISPLAY STRUCTURE
% disp([' ************************************* ']);
% disp([' outiter: ' sprintf('%4i', outeriter) ' kktnorm: ' sprintf('%6.4e', kktnorm)  ...
%     ' f_snap: ' sprintf('%10.7e', f0val) ' VolFrac: ' sprintf('%5.3f', M/M_total)]);
% disp([' ************************************* ']);
% 
% % PLOT AIRFOIL
% rhoVec = W_filt*x;
% U_end=U(end_node);
% mes=0;den=1;U=zeros(neq,1);
% domain(XYZ,LE,rhoVec,U,mes,den,outeriter,kktnorm,vol,U_end,penal)


% optimization PARAMETERS
xval=x;
m = 4;
n=nel;
epsimin = 1e-7;
xold1   = xval;
xold2   = xval;
xmin    = 0.1*ones(n, 1);
xmax    = ones(n, 1);
low     = xmin;
upp     = xmax;
c_mma   = 1000*ones(m, 1);
d_mma   = 1*ones(m, 1);
a0_mma  = 1;
a_mma   = 0*ones(m, 1);

maxoutit  = 100;
while  kktnorm > kkttol %outeriter < maxoutit
    
   
    
     % Plot material distribution
     disp([' ************************************* ']);
    disp([' outiter: ' sprintf('%4i', outeriter) ' kktnorm: ' sprintf('%6.4e', kktnorm) ...
        ' f_snap: ' sprintf('%10.7e', f0val) ' VolFrac: ' sprintf('%5.3f', M/M_total) ' ' num2str(U(end_node))]);
    disp([' ************************************* ']);
    rhoVec =W_filt*x;
    U_end=U(end_node);
    mes=0;den=1;U=zeros(neq,1);
    domain(XYZ,LE,rhoVec,U,mes,den,outeriter,kktnorm,vol,U_end,penal)
    
     outeriter = outeriter+1;
%     penal_old=penal; % OLD PENALIZATION
%     penal=max(1.0,min(ceil(outeriter/5),3.0)); % INCREASE PENALIZATION EVERY 5 ITERATIONS
%     penal_new=penal; % NEW PENALIZATION
%     
%     constant=nthroot(Emin,penal); %P_MIN 
%     xmin    = constant*ones(n, 1);
%     
%     A=(1-nthroot(Emin,penal+1))/(1-nthroot(Emin,penal));
%     B=1-A;
%     num=constant;
%     
%     if (penal_new-penal_old)>0  % IF THERE IS CHANGE IN PENALIZATION
%         parfor el=1:nel
%             x(el)=A*x(el)+B;     % UPDATE DENSITIES
%         end
%     end
    
%     M_bound=volfrac+(1-volfrac)*constant;  % UPDATE VOLUME FRACTION
    
    
    
   
    
  
    
    
     x_tilde=W_filt*x;
    
    
   
 
 % COMPLAINCE CONSTRAINT
    [comp,dc_comp]=NR_COMP(x_tilde,penal,neq,u_comp);
    
    
  % STRUCTURAL CONSTRAINT
  for i=1:n_marker
      
      if i==1
          
          
         [u_prev,theta_prev,u_input,theta_input,dc2(:,i),U,f(:,i),df(:,i)]= newton_raphson(i,x_tilde,penal,u_marker(i),[],[],[],[]);
      end
      if i==2
          
          [~,~,~,~,dc2(:,i),U,f(:,i),df(:,i)]= newton_raphson(i,x_tilde,penal,u_marker(i),u_input,theta_input,u_prev,theta_prev);
          
      end
    
  end
    


  M=sum(diag(Area)*(W_filt*x));
  M_total=sum(Area);
  vol=M/M_total;
%   vol=vol/M_bound
    
    % OBJECTIVE FUNCTION
    f0val= f(n_marker)/f0
    df0dx= df(:,n_marker)/f0;
    
    % FORCE CONSTRAINT
    fval(1)= f_bound/f0-f(1)/f0;
    dfdx(:,1)= -df(:,1)/f0;
    
    % VOLUME CONSTRAINT
    fval(2)= M/M_total - M_bound;
    dfdx(:,2)= (1/M_total)*Area;
    
    %DISPLACEMENT CONSTRAINTS
    fval(3)= 1+(U(end_node))/U_DEF;
    dfdx(:,3)= dc2(:,2)/U_DEF;
    
    %COMPLAICE CONSTRAINT
    % fval(4)=-1-(U(end_node)/25);
    % dfdx(:,4)=-dc2(:,2)/25;
    
    fval(4)= 1-comp/C0;
    dfdx(:,4)=  -dc_comp/C0;
    
   
    % MMA FUNCTION
    
    [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,low,upp] = mmasub(m,n,outeriter,x,xmin,xmax,xold1,xold2,f0val,df0dx,fval',dfdx',low,upp,a0_mma,a_mma,c_mma,d_mma);
     %%%% Some vectors are updated:
    xold2 = xold1; 
    xold1 = x;
    x = xmma;
    
    
    %%%% The residual vector of the KKT conditions is calculated:
    
    [residu,kktnorm,residumax] = kktcheck(m,n,xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,xmin,xmax,df0dx,fval',dfdx',a0_mma,a_mma,c_mma,d_mma);
   
% %     x_tilde=W_filt*x;
    

     
    density_values(outeriter,:)=rhoVec;
    save('rhoblock.mat','rhoVec');
    
    save('density_values.mat','density_values');
end
 
end

%%%%%%%%%%%%% SENSITIVITY VERIFICATION%%%%%%%%%%%%%%%%%%%%%%%%
% function main_call
% 
% clc
% clear all
% 
% format long
% global XYZ LE d h
% 
% 
% % Nodal Coordinates
% fileID = fopen('COORD.DAT');
% formatSpec=('%*s%*d%f%f%*d%*d%*d%*d');
% XYZ=textscan(fileID,formatSpec);
% [XYZ_1 XYZ_2]=XYZ{:};
% XYZ=[XYZ_1 XYZ_2];
% 
% 
% 
% % element conenctivity
% fileID = fopen('ELEM_CON.DAT');
% formatSpec=('%*s%*d%*s%d%d%d');
% LE=textscan(fileID,formatSpec);
% [LE_1 LE_2 LE_3]=LE{:};
% LE=[LE_1 LE_2 LE_3];
% 
% 
% 
% % structural parameters
% volfrac=1;
% penal=3;
% u_comp=0.5;
% nel=size(LE,1);
% nnodes=size(XYZ,1);
% neq=nnodes*2;
% d=rand([nel 1]);
% h=1e-6;
% % u=zeros(neq,1);
% %  PLOT(XYZ,u,LE)
% % density distribution
%   x_tilde(1:nel,1)=volfrac;
%    u_marker=0.5;
%    n_marker=1;
% % Adjoint Sensitivity
% %       [dc1,theta1]=NR_COMP(x_tilde,penal,u_comp,[],0);
%   [dc1,theta1]=newton_raphson(x_tilde,penal,u_marker,n_marker,[],0); 
% % % for the main loop 
% %   n_marker=1;
% 
% %   [~,~,~,~, ~,~,theta1,dc1]= newton_raphson(n_marker,x_tilde,penal,u_marker,[],[],[],[],[],[]);
% 
% %  % Finite Difference Sensitivity
%  
%  
%  
% %   for el=1:nel
%   
% %   [~,theta2]=NR_COMP(x_tilde,penal,u_comp,[],1);
%  [~,theta2]=newton_raphson(x_tilde,penal,u_marker,n_marker,[],1);
% %    [~,~,~,~, ~,~,theta2,~]= newton_raphson(n_marker,x_tilde,penal,u_marker,[],[],[],[], [],1);
% dc2=(theta2-theta1)/1e-6;
% 
% %   end
% 
%   dc1=dc1'*d;
% 
% dc1
% dc2
% 
% figure(3)
% 
% %      plot(dc1,'k');
%     loglog((dc1),'k');
% hold on 
% %  figure(4)
% %    plot(dc2,'r');
%      loglog((dc2),'ro');
% 
% 
% 
% end
% % %^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
% Newton-Raphson for complaince constraint
% 
%  function [comp,dc]= NR_COMP(x_tilde,penal,neq,u_comp)
% % %         function[dc,comp]=NR_COMP(x_tilde,penal,u_comp,el,direct)
% % % clc
% % % clear all
%  global  XYZ LE outeriter  d h BC
% 
% % Specify Number of steps
% n_inc=3;
% 
% % structural parameters
% nnodes=size(XYZ,1);
% nel=size(LE,1);
% % volfrac=1;
% % x(1:nel,1)=volfrac;
% % penal=3;
% NDOF=2;
% neq=nnodes*2;
% 
% 
% % Specify density distribution
% x=x_tilde;
% 
% 
% % specify Nodal parameters
%  load_node=44*2;
% 
%  fixed_node=145;
% 
%  
%    
% 
% 
% % boundary conditions
% 
% alldofs=1:neq;
% fixed_nodes=[BC' fixed_node];
% 
% 
% fixeddofs=[fixed_nodes.*2-1 fixed_nodes.*2];
% fixeddofs=sort(fixeddofs);
% freedofs=setdiff(alldofs,fixeddofs);
% nbc=size(fixeddofs,2);
% 
% for i=1:size(freedofs,2)
%     if freedofs(:,i)==load_node
%         count=i;
%     end
%    
% end
% 
% % Newton-Raphson Convergence Criterion
% epsilon=1e-9;
% 
% 
% % Initialize force distribution
% 
% u=zeros(neq,1);
% 
% theta0=0;
% 
% force=zeros(neq,1);
% force(load_node,1)=1;
% Fint_d1=zeros(neq,nel);
% 
% 
% %specify lambda
% lambda=0;
% 
% du = zeros(neq,1);
% step_dir=1;
% % specify displacement parameter
% 
% u(load_node,1)=0;
% 
% 
% % Specify Starting Displacememnt
% u_start=u(load_node,1);
% 
% u_final=u_comp;
% %Specify End displacemnet
% u_end=u_comp;
% u_end=u_final;
% %Specify Step Size
% delta_u=(u_end-u_start)/n_inc;
% % delta_l=delta_u;
% u_1=u_start + delta_u;
% % Specify Iteration Counters
% iter_outer_loop=0;
% iter_inner_loop=0;
% % u_conv(:,1)=0;
% % lambda_conv(1,:)=0;
% % delta_u=0;
% n=1;
% increment=1;
% [Fint_d1,KT,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal);
% du(freedofs) = KT(freedofs, freedofs)\force(freedofs);
% theta = delta_u/du(load_node);
% du = theta*du;
% dl = sqrt(du'*du);
% u_hist(:, n) = u;
% theta_hist = theta0;
% t_vec = u(freedofs)/norm(u(freedofs)); % normalized tangential predictor
% t_bar = t_vec; % store old tangential predictor
% u(load_node)
% u_end
% %          delta_l=0.1;
% %          lambda=0;
% %     delta_lambda=0;
% %     delubar=0;
% tol_u=1e-06;
% while abs(u(load_node))<abs(u_end)
%     n=n+1;
%     du_bar=zeros(neq,1);
%  [Fint_d1,KT,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal);   
%   du_bar(freedofs) = KT(freedofs, freedofs)\force(freedofs);
%     d_theta = dl/sqrt(du_bar'*du_bar);
%      t_vec = du_bar(freedofs)/norm(du_bar(freedofs));
%     gsp1 = t_vec'*t_bar; % general stiffness parameter
%     t_bar = t_vec; 
%      if gsp1 < 0
%             % switch force direction if limit point has been passed
%             step_dir = -step_dir;
%      end
%         
%      du_bar = step_dir*d_theta*du_bar;    
%      u = u_hist(:, n-1) + du_bar;
%      theta = theta_hist(n-1) + d_theta;
% 
%  [Fint_d1,KT,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal);
%     R = theta*force - Fint;
%     normR = norm(R(freedofs));
%     normddu = 1;
% while  (normR>epsilon) && (normddu>tol_u)
%      del_u = KT(freedofs, freedofs)\[R(freedofs), force(freedofs)];
%         
%         du_int_cor = del_u(:, 1);
%         du_ext_cor = del_u(:, 2);
%         
%         % evalute coefficients in quadratic equation
%         u_hat = du_bar(freedofs) + du_int_cor;
%         C = u_hat'*u_hat - dl^2;
%         B = 2*u_hat'*du_ext_cor;
%         A = du_ext_cor'*du_ext_cor;
%         
%         % evaluate both roots of the quadratic equation
%         sol1 = (-B + sqrt(B^2-4*A*C))/(2*A);
%         sol2 = (-B - sqrt(B^2-4*A*C))/(2*A);
%         
%         % update du using both values
%         du1 = du_bar(freedofs) + du_int_cor + sol1*du_ext_cor;
%         du2 = du_bar(freedofs) + du_int_cor + sol2*du_ext_cor;
%         
%         % evaluate Crisfields criterion
%         cf1 = du_bar(freedofs)'*du1;
%         cf2 = du_bar(freedofs)'*du2;
%         
%         if cf1 > cf2
%             du_bar(freedofs) = du1;
%             d_theta = d_theta + sol1;
%             ddu = [du_int_cor + sol1*du_ext_cor; sol1];
%         else
%             du_bar(freedofs) = du2;
%             d_theta = d_theta + sol2;
%             ddu = [du_int_cor + sol2*du_ext_cor; sol2];
%         end
%         
%          u = u_hist(:, n-1) + du_bar;
%          theta = theta_hist(n-1) + d_theta;
%         
% %         [Ktan, Fint] = NLResidual_arcTop(u,ELEMENTS,MESH,BC,rhoVec);
% [Fint_d1,KT,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal);
%         R = theta*force - Fint;
%         normR = norm(R(freedofs));
%         normddu = norm(ddu);
% end
% u(load_node)
%  uy(n)    = u(load_node);
%     u_hist(:, n) = u;
%     theta_hist(n) = theta;
%     
%     increment = increment + 1;
% 
% end
% fprintf('Switching to displacement control \n');
% normR=1;
% del_u_disp = zeros(neq, 2);
% 
% du_bar = zeros(neq,1); 
% iter=1;
% [Fint_d1,KT,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal);
% du_bar(freedofs) = KT(freedofs, freedofs)\force(freedofs);
% d_theta = (u_final-u_hist(load_node, n-1))/du_bar(load_node);
% 
% % Apply predictor step
% du_bar = d_theta*du_bar;    
% u = u_hist(:, n-1) + du_bar;
% theta = theta_hist(n-1) + d_theta;
% 
% u_hist(:, n) = u;
% theta_hist(n) = theta;
% [Fint_d1,KT,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal);
% R = theta*force - Fint;
% normR = norm(R(freedofs));
% 
% normddu = 1; % initialize norm of corrector step
% while (normR>epsilon) && (normddu>tol_u)
%     % Perform corrector steps
% 
% %     MESH.iter = iter;
% 
%     del_u_disp(freedofs, :) = KT(freedofs, freedofs)\[R(freedofs), force(freedofs)];
% 
%     du_int_cor = del_u_disp(:, 1);
%     du_ext_cor = del_u_disp(:, 2);
% 
%     del_theta = -du_int_cor(load_node)/du_ext_cor(load_node);
%     ddu = del_theta*du_ext_cor + du_int_cor;
%     %du_bar = du_bar + ddu;
%     %d_theta = d_theta + del_theta;
% 
%     %u(:) = u_hist(:, n-1) + du_bar;
%     u(:) = u_hist(:, n) + ddu;
%     %theta = theta_hist(n-1) + d_theta;
%     theta = theta_hist(n) + del_theta;
%     
%     u_hist(:, n) = u;
%     theta_hist(n) = theta;
% 
% %     [Ktan, Fint] = NLResidual_arcTop(u,ELEMENTS,MESH,BC,rhoVec);
% [Fint_d1,KT,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal);
%     R = theta*force - Fint;
%     normR = norm(R(freedofs));
%     normddu = norm(ddu);
% 
% %     fprintf(1,'Load Increment:%d   iter: %d   norm(R)=%2.8f  norm(dq)=%2.8f  u: %2.4e\n',n-1, iter,normR,normddu,u(app_dof));
%     iter = iter + 1;
% %    break
% end
% uy(n)    = u(load_node);
% u(load_node)
% u_hist(:, n) = u;
% theta_hist(n) = theta;
% 
% increment = increment + 1;
% 
% %**************************************************************************
% 
% % Force(n) = 10000*theta;
% %fprintf(1,'|F|: %f    gsp1: %d  \n',Force(n), gsp1);
% uy(n)    = u(load_node);
% u_hist(:, n) = u;
% theta_hist(n) = theta;
% % if n_marker==1
% %     u_prev=uy;
% %     theta_prev=theta_hist;
% % end
% % if n_marker==2
% %     uy(1)=u_prev(size(u_prev,2));
% % uy=[u_prev uy];
% % theta_hist=[theta_prev  theta_hist];
% % end
% % 
% % u_input=u;
% % theta_input=theta;
% % 
% % 
% % h3=figure(3);
% % plot((uy),(theta_hist),'linewidth',2);
% % xlabel('Displacement') % x-axis label
% % ylabel('Load \theta') % y-axis label
% % set(gcf,'color','w');
% % grid on
% % saveas(h3,sprintf('FIG%d.png',outeriter))
% %  PLOT(XYZ,u,nelx,nely)
% comp=theta*u(load_node);
% % sensitivity evaluation
% 
% fixeddof=[load_node fixeddofs];
%         fixeddof=sort(fixeddof);
%         alldof=1:neq;
%         freedof=setdiff(alldof,fixeddof);
%         kff=KT(freedof,freedof);
%         kpf=KT(fixeddof,freedof);
%         kfp=KT(freedof,fixeddof);
%         kpp=KT(fixeddof,fixeddof);
% 
% u_p=zeros(neq,1);
% u_p(load_node)=u_comp;
% lambda=zeros(neq,1);
% lambdap_T=u_p(fixeddof,1)';
% lambdaf_T=-lambdap_T*kpf/(kff);
% 
% %
% lambda(freedof,1)=lambdaf_T';
% lambda(fixeddof,1)=lambdap_T';
% 
% for IE=1:nel
%     
%     IDOF=zeros(1,6);
%     for I=1:3
%         II=(I-1)*NDOF+1;
%         IDOF(II:II+1)=(LE(IE,I)-1)*NDOF+1:(LE(IE,I)-1)*NDOF+2;
%     end
%     
%     lambda_e=lambda(IDOF);
%     dc(IE,:)=lambda_e'*Fint_d1(IDOF,IE);
%     
% end
% end
 
%function [ u_prev,theta_prev,u_input,theta_input,dc2,u,fext,dc]= newton_raphson(n_marker,x_tilde,penal,u_marker,u_input,theta_input,u_prev,theta_prev)
%        function[dc2,fext]=newton_raphson(x_tilde,penal,u_marker,n_marker,el,direct)
function[U, dfdx]=FEA(x_tilde, penal)
% clc
% clear all

global   XYZ LE  outeriter h d  BC
% BC= SDISPT_new;
% XYZ=XY_new;
% LE=LE_new;
% Specify Number of steps
n_inc=25;
if n_marker==1
    n_inc=5;
end
% structural parameters
nnodes=size(XYZ,1);
nel=size(LE,1);
% volfrac=1;
% x(1:nel,1)=volfrac;
% penal=3;
NDOF=2;
neq=nnodes*2;


% Specify density distribution
x=x_tilde;


% specify Nodal parameters
 load_node=145*2-1; % 143 

 end_node=44*2;

 
   


% boundary conditions

alldofs=1:neq;
fixed_nodes=BC';


fixeddofs=[fixed_nodes.*2-1 fixed_nodes.*2];
fixeddofs=sort(fixeddofs);
freedofs=setdiff(alldofs,fixeddofs);
nbc=size(fixeddofs,2);

for i=1:size(freedofs,2)
    if freedofs(:,i)==load_node
        count=i;
    end
   
end

% Newton-Raphson Convergence Criterion
epsilon=1e-9;


% Initialize force distribution
Fext=zeros(neq,1);
Fext(load_node,1)=0.05;
% Specify Starting Displacememnt
U = zeros(neq,1);

[Fint_d1,KT,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,U,x,penal);
residual=Fint-Fext;
%**************************************************************************



h3=figure(3);
plot((uy),(theta_hist),'linewidth',2);
xlabel('Displacement') % x-axis label
ylabel('Load \theta') % y-axis label
set(gcf,'color','w');
grid on
saveas(h3,sprintf('FIG%d.png',outeriter))
%  PLOT(XYZ,u,nelx,nely)
fext=theta;
% sensitivity evaluation

fixeddof=[load_node fixeddofs];
        fixeddof=sort(fixeddof);
        alldof=1:neq;
        freedof=setdiff(alldof,fixeddof);
        kff=KT(freedof,freedof);
        kpf=KT(fixeddof,freedof);
        kfp=KT(freedof,fixeddof);
        kpp=KT(fixeddof,fixeddof);
lambda=zeros(neq,1);
lambdap_T=force(fixeddof,1)';
lambdap2_T=zeros(size(fixeddof,2),1)';

u_new=zeros(neq,1);
u_new(end_node,1)=1;
u_new=u_new(freedof,1);


lambdaf2_T=-u_new'/kff;
lambdaf_T=-lambdap_T*kpf/(kff);

%
lambda(freedof,1)=lambdaf_T';
lambda(fixeddof,1)=lambdap_T';
lambda2(freedof,1)=lambdaf2_T';
lambda2(fixeddof,1)=lambdap2_T';



for IE=1:nel
    
    IDOF=zeros(1,6);
    for I=1:3
        II=(I-1)*NDOF+1;
        IDOF(II:II+1)=(LE(IE,I)-1)*NDOF+1:(LE(IE,I)-1)*NDOF+2;
    end
    lambda2_e=lambda2(IDOF);
    lambda_e=lambda(IDOF);
    
    dc(IE,:)=lambda_e'*Fint_d1(IDOF,IE);
    dc2(IE,:)=lambda2_e'*Fint_d1(IDOF,IE);
    
end
end

   %%%%%%%%%%%%%%%   TOTAL STIFFNESS MATRIX      %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ Fint_d1,Ktan,Fint]= stiff(nel,nnodes,NDOF,LE,XYZ,u,x,penal)
neq=nnodes*NDOF;
% Kgeo   = speye(neq,neq);
Kmat   = speye(neq,neq);
J      = sparse(neq);
Fint   = zeros(neq,1);
Fint_d1=zeros(neq,nel);

for e=1:nel
    
    [fint_d1,fe, kgeo, kmat] = hyper2d(e,u,XYZ,LE,nel,x,penal);
% output=struct('array1',fint_d1,'array2',fe,'array3',kgeo,'array4',kmat);
    

    for I=1:3
        II=(I-1)*NDOF+1;
        IDOF(II:II+1)=(LE(e,I)-1)*NDOF+1:(LE(e,I)-1)*NDOF+2;
    end   

    
    Fint(IDOF)      = Fint(IDOF)+fe;
    Fint_d1(IDOF,e)   = Fint_d1(IDOF,e)+ fint_d1;
%     
%      Kgeo(IDOF,IDOF) = Kgeo(IDOF,IDOF) + kgeo;
    Kmat(IDOF,IDOF) = Kmat(IDOF,IDOF) + kmat;
    %      J(IDOF,IDOF)    = J(IDOF,IDOF) + je;
end

%  Kgeo = Kgeo- speye(neq);
Kmat = Kmat- speye(neq);
% J    =  J    - speye(neq);

% R =  Fext - Fint;

 Ktan = Kmat;
end



%%%%%%%%%%%%%%%%%%%%%%%% ELEMENTAL STIFFNESS MATRIX%%%%%%%%%%

function [fint_d1,fint,kgeo,kmat]=hyper2d(e,u,XYZ,LE,nel,x,penal)

% nu=0.3;
ngp=1; % no. of gauss points in each direction
nen=3;
NDOF=2;
IE=e;


% Needed for optimization only 
% E=15;
% ndof=NDOF;
% mu=E/(2*(1+nu)); 
% lam=nu*E/((1+nu)*(1-2*nu));
% mu_e=x(e,1)^penal*mu;
% lam_e=x(e,1)^penal*lam;
% mu_d1=penal*x(e,1)^(penal-1)*mu;
% lam_d1=penal*x(e,1)^(penal-1)*lam;
ind2voit=[1; 4; 3];

elXY=XYZ(LE(e,:),:); % elemental nodal coordinates
X=elXY(:,1); % elemnt X coordinate
Y=elXY(:,2); % element Y coordinate

IDOF=zeros(1,6);
for I=1:3
II=(I-1)*NDOF+1;
IDOF(II:II+1)=(LE(IE,I)-1)*NDOF+1:(LE(IE,I)-1)*NDOF+2;
end
ue=u(IDOF); % elemnt nodal displacements
ux=[ue(1) ue(3) ue(5)]; % X displacements
uy=[ue(2) ue(4) ue(6)]; % Y displacements

% Ccur=[X+ux' Y+uy']; % current coordinates
Ccur=[X Y]; % current coordinates
[w,gp] = lgwt(ngp); % weights and integration points

 kgeo=zeros(nen*ndof,nen*ndof); % stiffness matrix
 kmat=zeros(nen*ndof,nen*ndof);
 fint=zeros(nen*ndof,1);        % internal force
 fint_d1=zeros(nen*ndof,1);
 
 
 for i=1:ngp
   for j=1:ngp
       eta = gp(i);            
       psi = gp(j);
 
       [dNdx,Bx, detJx]     = SHAPEL2D(Ccur);     % derivative of the shape functions                   
            
%        dNdx  = [Bx(1,1) Bx(1,3) Bx(1,5) Bx(1,7);
%                 Bx(2,2) Bx(2,4) Bx(2,6) Bx(2,8)];
       
%         Finv = [X  Y]'*dNdx';
%         F = inv(Finv);
%                          
%        J     = det(F);
%        BB    = F*F';                      
%        cauchy = mu_e/J*(BB - eye(2)) + lam_e/J*log(J)*eye(2) ;  % compressible neo-hook
%       cauchy_d1=mu_d1/J*(BB - eye(2)) + lam_d1/J*log(J)*eye(2) ;
%        fint = fint + Bx'*cauchy(ind2voit)*detJx*w(i)*w(j);
%        fint_d1=fint_d1+Bx'*cauchy_d1(ind2voit)*detJx*w(i)*w(j);
%       
%        Gx = dNdx'*cauchy*dNdx;
%         I = eye(2);      
% %       % generte a block geometric contribution
%        BLK = [Gx(1,1)*I    Gx(1,2)*I   Gx(1,3)*I     
%               Gx(2,1)*I    Gx(2,2)*I   Gx(2,3)*I     
%               Gx(3,1)*I    Gx(3,2)*I   Gx(3,3)*I  ];
% %          
%         lam1 = lam_e/J;
%         mu1  = (mu_e - lam_e*log(J))/J;
        
        
  E = 69 * 10^9;
  nu = 0.33;
      D    = E/(1-nu^2).* [1        nu 0 ;
                 nu 1 0;
                   0 0 (1-nu)/2];
%        D=x(e,1)^penal*D;

%       kgeo = kgeo + w(i)*w(j)*BLK*detJx;   % geometric element matrix        
      kmat = kmat + w(i)*w(j)*Bx'*D*Bx*detJx;   % material element matrix        

      
      
   end
end

end





%%%%%%%%%%%%%%%%%%%%%%% SHAPE FUNCTIONS%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [dNdx,B, det] = SHAPEL2D(C)

% C is the current coordinates [x1 y1;x2 y2;x3 y3];

x2=C(2,1);
x1=C(1,1);
x3=C(3,1);
y1=C(1,2);
y2=C(2,2);
y3=C(3,2);

det=(x2-x1)*(y3-y1)-(x3-x1)*(y2-y1);
B=(1/det)*[y2-y3  0    y3-y1   0    y1-y2     0;
             0   x3-x2   0   x1-x3    0    x2-x1;
           x3-x2 y2-y3 x1-x3 y3-y1  x2-x1   y1-y2];

       
 j_inv=(1/det)*[  y3-y1    -(y2-y1);
                -(x3-x1)      x2-x1];
            
   dNdx=j_inv*[-1 1 0;-1 0 1];     

end
































%%%%%%%%%%%%%%%%%%%%%%%%%%% APPENDIX%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [w,x]=lgwt(N);
a=-1;
b=1;
% lgwt.m
%
% This script is for computing definite integrals using Legendre-Gauss 
% Quadrature. Computes the Legendre-Gauss nodes and weights  on an interval
% [a,b] with truncation order N
%
% Suppose you have a continuous function f(x) which is defined on [a,b]
% which you can evaluate at any x in [a,b]. Simply evaluate it at all of
% the values contained in the x vector to obtain a vector f. Then compute
% the definite integral using sum(f.*w);
%
% Written by Greg von Winckel - 02/25/2004
N=N-1;
N1=N+1; N2=N+2;

xu=linspace(-1,1,N1)';

% Initial guess
y=cos((2*(0:N)'+1)*pi/(2*N+2))+(0.27/N1)*sin(pi*xu*N/N2);

% Legendre-Gauss Vandermonde Matrix
L=zeros(N1,N2);

% Derivative of LGVM
Lp=zeros(N1,N2);

% Compute the zeros of the N+1 Legendre Polynomial
% using the recursion relation and the Newton-Raphson method

y0=2;

% Iterate until new points are uniformly within epsilon of old points
while max(abs(y-y0))>eps
    
    
    L(:,1)=1;
    Lp(:,1)=0;
    
    L(:,2)=y;
    Lp(:,2)=1;
    
    for k=2:N1
        L(:,k+1)=( (2*k-1)*y.*L(:,k)-(k-1)*L(:,k-1) )/k;
    end
 
    Lp=(N2)*( L(:,N1)-y.*L(:,N2) )./(1-y.^2);   
    
    y0=y;
    y=y0-L(:,N2)./Lp;
    
end

% Linear map from[-1,1] to [a,b]
x=(a*(1-y)+b*(1+y))/2;      

% Compute the weights
w=(b-a)./((1-y.^2).*Lp.^2)*(N2/N1)^2;
end








% function [W]=density_filter(NE,R,XYZ,LE) 
% %
% 
% if R == 0
%     W = eye(NE);
%     return
% end
% % %
% W = zeros(NE);
% w = zeros(NE);
% %
% for eli = 1:NE
%     ci = [(XYZ(LE(eli,1),1)+XYZ(LE(eli,3),1))/2, (XYZ(LE(eli,1),2)+XYZ(LE(eli,3),2))/2];
%     for elj = 1:NE
%         cj = [(XYZ(LE(elj,1),1)+XYZ(LE(elj,3),1))/2, (XYZ(LE(elj,1),2)+XYZ(LE(elj,3),2))/2];
%         
%         r = sqrt((ci(1)-cj(1))^2 + (ci(2)-cj(2))^2);
%         
%         w(eli,elj) = max(0,R-r);
%     end
%     w_norm = 1/sum(w(eli,:));
%     W(eli,:) = w_norm*w(eli,:);
%     a= sum(W(eli,:));
% end
% end

function [ W] = density_filter( XYZ,LE,rmin,nel)


% nel=nelx*nely;
% nnodes=(nelx+1)*(nely+1);
% neq=nnodes*2;
% n_nodes_x=nelx+1;
% n_nodes_y=nely+1;


% coordinates of each element
%  N=0;
% for j=1:n_nodes_x
%     for i=1:n_nodes_y
%         N=N+1;
%         XYZ(N,:)=[(j-1)*a (nely-(i-1))*b];
%
%     end
% end


% centroid of each element
% for elx=1:nelx
%     for ely=1:nely
%
%
%  n1 = (nely+1)*(elx-1)+ely;
%  n2 = (nely+1)* elx +ely;
%  n3=(nely+1)*(elx-1)+ely+1;
%  n4=(nely+1)* elx +ely+1;
%       x1=XYZ(n1,:);
%       x2=XYZ(n2,:);
%       x3=XYZ(n3,:);
%       x4=XYZ(n4,:);
%       x_c(ely,elx)=((x2(1)-x1(1))/2)+(elx-1)*a;
%       y_c(ely,elx)=((x2(2)-x4(2))/2)+(ely-1)*b;
%
%
%
%     end
% end
for e=1:nel
    elXY=XYZ(LE(e,:),:); % elemental nodal coordinates
    X=elXY(:,1); % elemnt X coordinate
    Y=elXY(:,2); % element Y coordinate
    x_c(e)=mean(X);
    y_c(e)=mean(Y);
%     rmin(e)=sqrt((X(1)-X(2))^2+((Y(1)-Y(2))^2));
end

element_1=0;

for el_1=1:nel
    
    element_1=element_1+1;
    
    x_c1=x_c(el_1);
    y_c1=y_c(el_1);
    element_2=0;
    for el_2=1:nel
        
        element_2=element_2+1;
        x_c2=x_c(el_2);
        y_c2=y_c(el_2);
        R=sqrt((y_c2-y_c1)^2+(x_c2-x_c1)^2);
        w(element_1,element_2)=max(0,rmin-R);
        
    end
    
    wnorm=sum(w(element_1,:));
    W(element_1,:) = w(element_1,:)/wnorm;
    
    
end




end

%---------------------------------------------------------------------
%  This is the file kktcheck.m
%  Version Dec 2006.
%  Krister Svanberg <krille@math.kth.se>
%
function[residu,residunorm,residumax] = ...
kktcheck(m,n,x,y,z,lam,xsi,eta,mu,zet,s, ...
         xmin,xmax,df0dx,fval,dfdx,a0,a,c,d);
%
%  The left hand sides of the KKT conditions for the following
%  nonlinear programming problem are calculated.
%         
%      Minimize  f_0(x) + a_0*z + sum( c_i*y_i + 0.5*d_i*(y_i)^2 )
%    subject to  f_i(x) - a_i*z - y_i <= 0,  i = 1,...,m
%                xmax_j <= x_j <= xmin_j,    j = 1,...,n
%                z >= 0,   y_i >= 0,         i = 1,...,m
%*** INPUT:
%
%   m    = The number of general constraints.
%   n    = The number of variables x_j.
%   x    = Current values of the n variables x_j.
%   y    = Current values of the m variables y_i.
%   z    = Current value of the single variable z.
%  lam   = Lagrange multipliers for the m general constraints.
%  xsi   = Lagrange multipliers for the n constraints xmin_j - x_j <= 0.
%  eta   = Lagrange multipliers for the n constraints x_j - xmax_j <= 0.
%   mu   = Lagrange multipliers for the m constraints -y_i <= 0.
%  zet   = Lagrange multiplier for the single constraint -z <= 0.
%   s    = Slack variables for the m general constraints.
%  xmin  = Lower bounds for the variables x_j.
%  xmax  = Upper bounds for the variables x_j.
%  df0dx = Vector with the derivatives of the objective function f_0
%          with respect to the variables x_j, calculated at x.
%  fval  = Vector with the values of the constraint functions f_i,
%          calculated at x.
%  dfdx  = (m x n)-matrix with the derivatives of the constraint functions
%          f_i with respect to the variables x_j, calculated at x.
%          dfdx(i,j) = the derivative of f_i with respect to x_j.
%   a0   = The constants a_0 in the term a_0*z.
%   a    = Vector with the constants a_i in the terms a_i*z.
%   c    = Vector with the constants c_i in the terms c_i*y_i.
%   d    = Vector with the constants d_i in the terms 0.5*d_i*(y_i)^2.
%     
%*** OUTPUT:
%
% residu     = the residual vector for the KKT conditions.
% residunorm = sqrt(residu'*residu).
% residumax  = max(abs(residu)).
%
rex   = df0dx + dfdx'*lam - xsi + eta;
rey   = c + d.*y - mu - lam;
rez   = a0 - zet - a'*lam;
relam = fval - a*z - y + s;
rexsi = xsi.*(x-xmin);
reeta = eta.*(xmax-x);
remu  = mu.*y;
rezet = zet*z;
res   = lam.*s;
%
residu1 = [rex' rey' rez]';
residu2 = [relam' rexsi' reeta' remu' rezet res']';
residu = [residu1' residu2']';
residunorm = sqrt(residu'*residu);
residumax = max(abs(residu));
%---------------------------------------------------------------------
end

%-------------------------------------------------------
%    This is the file mmasub.m
%
function [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,low,upp] = ...
mmasub(m,n,iter,xval,xmin,xmax,xold1,xold2, ...
f0val,df0dx,fval,dfdx,low,upp,a0,a,c,d);
%
%    Version September 2007 (and a small change August 2008)
%
%    Krister Svanberg <krille@math.kth.se>
%    Department of Mathematics, SE-10044 Stockholm, Sweden.
%
%    This function mmasub performs one MMA-iteration, aimed at
%    solving the nonlinear programming problem:
%         
%      Minimize  f_0(x) + a_0*z + sum( c_i*y_i + 0.5*d_i*(y_i)^2 )
%    subject to  f_i(x) - a_i*z - y_i <= 0,  i = 1,...,m
%                xmin_j <= x_j <= xmax_j,    j = 1,...,n
%                z >= 0,   y_i >= 0,         i = 1,...,m
%*** INPUT:
%
%   m    = The number of general constraints.
%   n    = The number of variables x_j.
%  iter  = Current iteration number ( =1 the first time mmasub is called).
%  xval  = Column vector with the current values of the variables x_j.
%  xmin  = Column vector with the lower bounds for the variables x_j.
%  xmax  = Column vector with the upper bounds for the variables x_j.
%  xold1 = xval, one iteration ago (provided that iter>1).
%  xold2 = xval, two iterations ago (provided that iter>2).
%  f0val = The value of the objective function f_0 at xval.
%  df0dx = Column vector with the derivatives of the objective function
%          f_0 with respect to the variables x_j, calculated at xval.
%  fval  = Column vector with the values of the constraint functions f_i,
%          calculated at xval.
%  dfdx  = (m x n)-matrix with the derivatives of the constraint functions
%          f_i with respect to the variables x_j, calculated at xval.
%          dfdx(i,j) = the derivative of f_i with respect to x_j.
%  low   = Column vector with the lower asymptotes from the previous
%          iteration (provided that iter>1).
%  upp   = Column vector with the upper asymptotes from the previous
%          iteration (provided that iter>1).
%  a0    = The constants a_0 in the term a_0*z.
%  a     = Column vector with the constants a_i in the terms a_i*z.
%  c     = Column vector with the constants c_i in the terms c_i*y_i.
%  d     = Column vector with the constants d_i in the terms 0.5*d_i*(y_i)^2.
%     
%*** OUTPUT:
%
%  xmma  = Column vector with the optimal values of the variables x_j
%          in the current MMA subproblem.
%  ymma  = Column vector with the optimal values of the variables y_i
%          in the current MMA subproblem.
%  zmma  = Scalar with the optimal value of the variable z
%          in the current MMA subproblem.
%  lam   = Lagrange multipliers for the m general MMA constraints.
%  xsi   = Lagrange multipliers for the n constraints alfa_j - x_j <= 0.
%  eta   = Lagrange multipliers for the n constraints x_j - beta_j <= 0.
%   mu   = Lagrange multipliers for the m constraints -y_i <= 0.
%  zet   = Lagrange multiplier for the single constraint -z <= 0.
%   s    = Slack variables for the m general MMA constraints.
%  low   = Column vector with the lower asymptotes, calculated and used
%          in the current MMA subproblem.
%  upp   = Column vector with the upper asymptotes, calculated and used
%          in the current MMA subproblem.
%
%epsimin = sqrt(m+n)*10^(-9);
epsimin = 10^(-7);
raa0 = 0.00001;
albefa = 0.1;
asyinit = 0.5;
asyincr = 1.2;
asydecr = 0.7;
eeen = ones(n,1);
eeem = ones(m,1);
zeron = zeros(n,1);

% Calculation of the asymptotes low and upp :
if iter < 2.5
  low = xval - asyinit*(xmax-xmin);
  upp = xval + asyinit*(xmax-xmin);
else
  zzz = (xval-xold1).*(xold1-xold2);
  factor = eeen;
  factor(find(zzz > 0)) = asyincr;
  factor(find(zzz < 0)) = asydecr;
  low = xval - factor.*(xold1 - low);
  upp = xval + factor.*(upp - xold1);
  lowmin = xval - 10*(xmax-xmin);
  lowmax = xval - 0.01*(xmax-xmin);
  uppmin = xval + 0.01*(xmax-xmin);
  uppmax = xval + 10*(xmax-xmin);
  low = max(low,lowmin);
  low = min(low,lowmax);
  upp = min(upp,uppmax);
  upp = max(upp,uppmin);
end

% Calculation of the bounds alfa and beta :

zzz = low + albefa*(xval-low);
alfa = max(zzz,xmin);
zzz = upp - albefa*(upp-xval);
beta = min(zzz,xmax);

% Calculations of p0, q0, P, Q and b.

xmami = xmax-xmin;
xmamieps = 0.00001*eeen;
xmami = max(xmami,xmamieps);
xmamiinv = eeen./xmami;
ux1 = upp-xval;
ux2 = ux1.*ux1;
xl1 = xval-low;
xl2 = xl1.*xl1;
uxinv = eeen./ux1;
xlinv = eeen./xl1;
%
p0 = zeron;
q0 = zeron;
p0 = max(df0dx,0);
q0 = max(-df0dx,0);
%p0(find(df0dx > 0)) = df0dx(find(df0dx > 0));
%q0(find(df0dx < 0)) = -df0dx(find(df0dx < 0));
pq0 = 0.001*(p0 + q0) + raa0*xmamiinv;
p0 = p0 + pq0;
q0 = q0 + pq0;
p0 = p0.*ux2;
q0 = q0.*xl2;
%
P = sparse(m,n);
Q = sparse(m,n);
P = max(dfdx,0);
Q = max(-dfdx,0);
%P(find(dfdx > 0)) = dfdx(find(dfdx > 0));
%Q(find(dfdx < 0)) = -dfdx(find(dfdx < 0));
PQ = 0.001*(P + Q) + raa0*eeem*xmamiinv';
P = P + PQ;
Q = Q + PQ;
P = P * spdiags(ux2,0,n,n);
Q = Q * spdiags(xl2,0,n,n);
b = P*uxinv + Q*xlinv - fval ;
%
%%% Solving the subproblem by a primal-dual Newton method
[xmma,ymma,zmma,lam,xsi,eta,mu,zet,s] = ...
subsolv(m,n,epsimin,low,upp,alfa,beta,p0,q0,P,Q,a0,a,b,c,d);




end

%-------------------------------------------------------------
%    This is the file subsolv.m
%
%    Version Dec 2006.
%    Krister Svanberg <krille@math.kth.se>
%    Department of Mathematics, KTH,
%    SE-10044 Stockholm, Sweden.
%
function [xmma,ymma,zmma,lamma,xsimma,etamma,mumma,zetmma,smma] = ...
subsolv(m,n,epsimin,low,upp,alfa,beta,p0,q0,P,Q,a0,a,b,c,d);
%
% This function subsolv solves the MMA subproblem:
%         
% minimize   SUM[ p0j/(uppj-xj) + q0j/(xj-lowj) ] + a0*z +
%          + SUM[ ci*yi + 0.5*di*(yi)^2 ],
%
% subject to SUM[ pij/(uppj-xj) + qij/(xj-lowj) ] - ai*z - yi <= bi,
%            alfaj <=  xj <=  betaj,  yi >= 0,  z >= 0.
%        
% Input:  m, n, low, upp, alfa, beta, p0, q0, P, Q, a0, a, b, c, d.
% Output: xmma,ymma,zmma, slack variables and Lagrange multiplers.
%
een = ones(n,1);
eem = ones(m,1);
epsi = 1;
epsvecn = epsi*een;
epsvecm = epsi*eem;
x = 0.5*(alfa+beta);
y = eem;
z = 1;
lam = eem;
xsi = een./(x-alfa);
xsi = max(xsi,een);
eta = een./(beta-x);
eta = max(eta,een);
mu  = max(eem,0.5*c);
zet = 1;
s = eem;
itera = 0;
while epsi > epsimin
  epsvecn = epsi*een;
  epsvecm = epsi*eem;
  ux1 = upp-x;
  xl1 = x-low;
  ux2 = ux1.*ux1;
  xl2 = xl1.*xl1;
  uxinv1 = een./ux1;
  xlinv1 = een./xl1;
  plam = p0 + P'*lam ;
  qlam = q0 + Q'*lam ;
  gvec = P*uxinv1 + Q*xlinv1;
  dpsidx = plam./ux2 - qlam./xl2 ;
  rex = dpsidx - xsi + eta;
  rey = c + d.*y - mu - lam;
  rez = a0 - zet - a'*lam;
  relam = gvec - a*z - y + s - b;
  rexsi = xsi.*(x-alfa) - epsvecn;
  reeta = eta.*(beta-x) - epsvecn;
  remu = mu.*y - epsvecm;
  rezet = zet*z - epsi;
  res = lam.*s - epsvecm;
  residu1 = [rex' rey' rez]';
  residu2 = [relam' rexsi' reeta' remu' rezet res']';
  residu = [residu1' residu2']';
  residunorm = sqrt(residu'*residu);
  residumax = max(abs(residu));
  ittt = 0;
  while residumax > 0.9*epsi & ittt < 200
    ittt=ittt + 1;
    itera=itera + 1;
    ux1 = upp-x;
    xl1 = x-low;
    ux2 = ux1.*ux1;
    xl2 = xl1.*xl1;
    ux3 = ux1.*ux2;
    xl3 = xl1.*xl2;
    uxinv1 = een./ux1;
    xlinv1 = een./xl1;
    uxinv2 = een./ux2;
    xlinv2 = een./xl2;
    plam = p0 + P'*lam ;
    qlam = q0 + Q'*lam ;
    gvec = P*uxinv1 + Q*xlinv1;
    GG = P*spdiags(uxinv2,0,n,n) - Q*spdiags(xlinv2,0,n,n);
    dpsidx = plam./ux2 - qlam./xl2 ;
    delx = dpsidx - epsvecn./(x-alfa) + epsvecn./(beta-x);
    dely = c + d.*y - lam - epsvecm./y;
    delz = a0 - a'*lam - epsi/z;
    dellam = gvec - a*z - y - b + epsvecm./lam;
    diagx = plam./ux3 + qlam./xl3;
    diagx = 2*diagx + xsi./(x-alfa) + eta./(beta-x);
    diagxinv = een./diagx;
    diagy = d + mu./y;
    diagyinv = eem./diagy;
    diaglam = s./lam;
    diaglamyi = diaglam+diagyinv;
    if m < n
      blam = dellam + dely./diagy - GG*(delx./diagx);
      bb = [blam' delz]';
      Alam = spdiags(diaglamyi,0,m,m) + GG*spdiags(diagxinv,0,n,n)*GG';
      AA = [Alam     a
            a'    -zet/z ];
      solut = AA\bb;
      dlam = solut(1:m);
      dz = solut(m+1);
      dx = -delx./diagx - (GG'*dlam)./diagx;
    else
      diaglamyiinv = eem./diaglamyi;
      dellamyi = dellam + dely./diagy;
      Axx = spdiags(diagx,0,n,n) + GG'*spdiags(diaglamyiinv,0,m,m)*GG;
      azz = zet/z + a'*(a./diaglamyi);
      axz = -GG'*(a./diaglamyi);
      bx = delx + GG'*(dellamyi./diaglamyi);
      bz  = delz - a'*(dellamyi./diaglamyi);
      AA = [Axx   axz
            axz'  azz ];
      bb = [-bx' -bz]';
      solut = AA\bb;
      dx  = solut(1:n);
      dz = solut(n+1);
      dlam = (GG*dx)./diaglamyi - dz*(a./diaglamyi) + dellamyi./diaglamyi;
    end
%
    dy = -dely./diagy + dlam./diagy;
    dxsi = -xsi + epsvecn./(x-alfa) - (xsi.*dx)./(x-alfa);
    deta = -eta + epsvecn./(beta-x) + (eta.*dx)./(beta-x);
    dmu  = -mu + epsvecm./y - (mu.*dy)./y;
    dzet = -zet + epsi/z - zet*dz/z;
    ds   = -s + epsvecm./lam - (s.*dlam)./lam;
    xx  = [ y'  z  lam'  xsi'  eta'  mu'  zet  s']';
    dxx = [dy' dz dlam' dxsi' deta' dmu' dzet ds']';
%    
    stepxx = -1.01*dxx./xx;
    stmxx  = max(stepxx);
    stepalfa = -1.01*dx./(x-alfa);
    stmalfa = max(stepalfa);
    stepbeta = 1.01*dx./(beta-x);
    stmbeta = max(stepbeta);
    stmalbe  = max(stmalfa,stmbeta);
    stmalbexx = max(stmalbe,stmxx);
    stminv = max(stmalbexx,1);
    steg = 1/stminv;
%
    xold   =   x;
    yold   =   y;
    zold   =   z;
    lamold =  lam;
    xsiold =  xsi;
    etaold =  eta;
    muold  =  mu;
    zetold =  zet;
    sold   =   s;
%
    itto = 0;
    resinew = 2*residunorm;
    while resinew > residunorm & itto < 50
    itto = itto+1;
    x   =   xold + steg*dx;
    y   =   yold + steg*dy;
    z   =   zold + steg*dz;
    lam = lamold + steg*dlam;
    xsi = xsiold + steg*dxsi;
    eta = etaold + steg*deta;
    mu  = muold  + steg*dmu;
    zet = zetold + steg*dzet;
    s   =   sold + steg*ds;
    ux1 = upp-x;
    xl1 = x-low;
    ux2 = ux1.*ux1;
    xl2 = xl1.*xl1;
    uxinv1 = een./ux1;
    xlinv1 = een./xl1;
    plam = p0 + P'*lam ;
    qlam = q0 + Q'*lam ;
    gvec = P*uxinv1 + Q*xlinv1;
    dpsidx = plam./ux2 - qlam./xl2 ;
    rex = dpsidx - xsi + eta;
    rey = c + d.*y - mu - lam;
    rez = a0 - zet - a'*lam;
    relam = gvec - a*z - y + s - b;
    rexsi = xsi.*(x-alfa) - epsvecn;
    reeta = eta.*(beta-x) - epsvecn;
    remu = mu.*y - epsvecm;
    rezet = zet*z - epsi;
    res = lam.*s - epsvecm;
    residu1 = [rex' rey' rez]';
    residu2 = [relam' rexsi' reeta' remu' rezet res']';
    residu = [residu1' residu2']';
    resinew = sqrt(residu'*residu);
    steg = steg/2;
    end
  residunorm=resinew;
  residumax = max(abs(residu));
  steg = 2*steg;
  end
  if ittt > 198
    epsi
    ittt
  end
epsi = 0.1*epsi;
end
xmma   =   x;
ymma   =   y;
zmma   =   z;
lamma =  lam;
xsimma =  xsi;
etamma =  eta;
mumma  =  mu;
zetmma =  zet;
smma   =   s;
%-------------------------------------------------------------

end


function domain(XYZ,LE,rhoVec,U,mes,den,outeriter,kktnorm,vol,U_end,penal)
global num BC 
tri=1;
% number of elements
nel=size(LE,1);

 DELTA=U;
counter1=0;
for i=1:size(XYZ,1)
    for j = 1:size(XYZ,2)
        counter1=counter1+1;
        CDISP(counter1,:)=XYZ(i,j)+DELTA(counter1,1);
        
    end
end

if tri==1;
    
    
    for el = 1:nel
        el;
        val=(LE(el,:));
        n1=val(:,1);
        n2=val(:,2);
        n3=val(:,3);
        NEW =CDISP([2*n1-1;2*n1;2*n2-1;2*n2;2*n3-1;2*n3],1);
        
        xx(:,el) = [NEW(1,1) NEW(3,1) NEW(5,1)]';
        xx_mesh(:,el)=[NEW(1,1) NEW(3,1) NEW(5,1) NEW(1,1)]'
        yy(:,el)= [NEW(2,1) NEW(4,1) NEW(6,1)]';
        yy_mesh(:,el)= [NEW(2,1) NEW(4,1) NEW(6,1) NEW(2,1)]';
    end
end





if tri==0
    
    for el=1:nel
        val=(LE(el,:));
        n1=val(:,4);
        n2=val(:,3);
        NEW =CDISP([2*n1+1;2*n1+2;2*n2+1;2*n2+2; 2*n2-1;2*n2;2*n1-1;2*n1],1);
        
        xx(:,el) = [NEW(1,1) NEW(3,1) NEW(5,1) NEW(7,1)]';
        yy(:,el) = [NEW(2,1) NEW(4,1) NEW(6,1) NEW(8,1)]';
        xx_mesh(:,el) = [NEW(1,1) NEW(3,1) NEW(5,1) NEW(7,1) NEW(1,1)]';
        yy_mesh(:,el) = [NEW(2,1) NEW(4,1) NEW(6,1) NEW(8,1) NEW(2,1)]';
    end
end
fixed_nodes=BC';
% fixed_nodes=[24:68 331:372];
% fixed_nodes=[9:32 53 82:93  518 519 554:575 462:496];
% fixed_nodes=[9 82 83 84 85 86 87 88 89 90 91 92 93 554 555 556 557 558 559 560 ...
%       561 562 563 564 565 566 567 568 569 570 571 ...
%       572 573 574 575 10 11 12 13 14 15 16 17 18 19 20 ...
%       21 22 23 24 25 26 27 28 29 30 31 32 ...
%       462 463 464 465 466 467 468 469 470 471 472 473 474 475 476 477 478 479 480 481 ...
%       482 483 484 485 486 487 488 489 490 ...
%       491 492 493 494 495 496 ];
% fixed_nodes=[ 53 82 89 90 91 92 93  518 519 566 567 568 569 570 571 ... 
%       572 573 574 575  17 18 19 20 ... 
%       21 22 23 24 25 26 27 28 29 30 31 32 ...
%       471 472 473 474 475 476 477 478 479 480 481 ... 
%       482 483 484 485 486 487 488 489 490 ... 
%       491 492 493 494 495 496];
  fixed_nodes=sort(fixed_nodes);
  for i=1:size(fixed_nodes,2)
XYZ_P(i,:)=(XYZ(fixed_nodes(i),:));
  end
% Mesh Plotting
if mes==1
figure(2)
plot(xx_mesh,yy_mesh,'r')

 hold on
 plot (XYZ_P(:,1),XYZ_P(:,2),'*')
% text(XYZ(462,1)+550,XYZ(462,2)+550,'\bf Fixed Boundary ')
% text(XYZ(474,1)-70,XYZ(474,2)-70,'\bf Fixed Boundary ')
% hold on
% % plot(XYZ(1833,1),XYZ(1833,2),'*')
% 
% text(XYZ(1833,1)-5,XYZ(1833,2),' \bf Force\bullet','HorizontalAlignment','right')
% hold on
% % plot(XYZ(52,1),XYZ(52,2),'*')
% 
% text(XYZ(52,1)-25,XYZ(52,2)+10,' \bullet \bf Deflection')
colormap(gray);
set(gcf,'color','w');
grid on
axis equal
end

% Density distribution plotting
if den==1
h1=figure(2)
patch(xx,yy,-rhoVec)
set(gcf,'color','w');
shading flat
colormap(gray);
grid on
 title([num2str(outeriter) ' ' 'kktnorm:' num2str(kktnorm) ' ''volfrac:' num2str(vol) ' ' num2str(U_end) ' ' num2str(penal) ' ' num2str(num)]);
axis equal
drawnow;
saveas(h1,sprintf('FIGURE%d.png',outeriter))
end
end






% function PLOT(XYZ,U,LE)
% 
% % Converged displacement
% DELTA=U;
% 
%      
%      counter1=0;
% for i=1:size(XYZ,1)
%  for j = 1:size(XYZ,2)
%      counter1=counter1+1;
%  CDISP(counter1,:)=XYZ(i,j)+DELTA(counter1,1);
%  
%  end
% end
% 
% 
% figure()
% nel=size(LE,1);
% for el=1:nel
% val=(LE(el,:));
%   n1=val(:,4);
%   n2=val(:,3);
%  
% NEW =CDISP([2*n1+1;2*n1+2;2*n2+1;2*n2+2; 2*n2-1;2*n2;2*n1-1;2*n1],1);
% plot([NEW(1,1) NEW(3,1) NEW(5,1) NEW(7,1) NEW(1,1)],[NEW(2,1) NEW(4,1) NEW(6,1) NEW(8,1) NEW(2,1)],'color','r')
% hold on
% axis equal
%  end
% 
% 
% end
%  

% function PLOT(XYZ,U,LE)
% 
% % Converged displacement
% DELTA=U;
% 
%      
%      counter1=0;
% for i=1:size(XYZ,1)
%  for j = 1:size(XYZ,2)
%      counter1=counter1+1;
%  CDISP(counter1,:)=XYZ(i,j)+DELTA(counter1,1);
%  
%  end
% end
% 
% 
% figure()
% nel=size(LE,1);
% for el=1:nel
% val=(LE(el,:));
%   n1=val(:,1);
%   n2=val(:,2);
%   n3=val(:,3);
% NEW =CDISP([2*n1-1;2*n1;2*n2-1;2*n2;2*n3-1;2*n3],1);
% plot([NEW(1,1) NEW(3,1) NEW(5,1) NEW(1,1)],[NEW(2,1) NEW(4,1) NEW(6,1) NEW(2,1)],'color','r')
% hold on
% axis equal
%  end
% 
% 
% end
% 
% % plotting airfoil
% 
% function plot(XYZ,LE,rhoVec)
% 
%  nel=size(LE,1);
% 
% counter1=0;
% for i=1:size(XYZ,1)
%  for j = 1:size(XYZ,2)
%      counter1=counter1+1;
%  CDISP(counter1,:)=XYZ(i,j);
% %  +DELTA(counter1,1);
%  
%  end
% end
% 
% 
% 
% 
% parfor el = 1:nel
% el
% val=(LE(el,:));
%   n1=val(:,1);
%   n2=val(:,2);
%   n3=val(:,3);
% NEW =CDISP([2*n1-1;2*n1;2*n2-1;2*n2;2*n3-1;2*n3],1);
% 
% xx(:,el) = [NEW(1,1) NEW(3,1) NEW(5,1)]';
%  yy(:,el)= [NEW(2,1) NEW(4,1) NEW(6,1)]';
% 
% end
% 
% figure(1)
% patch(xx,yy,-rhoVec)
%  set(gcf,'color','w');
%  shading flat
%  colormap(gray);
%  grid on
%  axis equal
% drawnow; 
% end

