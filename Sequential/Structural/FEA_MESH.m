
%     close all
%     clear all
%     clc


function [tria, vert] = FEA_MESH
    addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Workbench')
    libpath();
    
    %% Nomenclature
    
    %{
    
    tria :: Node connectivity matri
    vert :: Node coordinates
    %}

    


%% Assign file path to external libraries

    filename = mfilename('fullpath');
    filepath = fileparts( filename );
%% Assign mesh file <name>.msh
    meshfile = ...
         [filepath,'/MESH/airfoil.txt'];

     %[filepath,'/poly-data/airfoil.txt'];
%% Use triread.m to extract node and edge information    
    
   [node,edge] = triread( meshfile );
 
%---------------------------------------------- do size-fun. 
    olfs.dhdx = +0.15;

 
   [vlfs,tlfs, ...
    hlfs] = lfshfn2(node,edge, ...
                    []  ,olfs) ;
    
   [slfs] = idxtri2(vlfs,tlfs) ;
   
%     figure;
%     patch('faces',tlfs(:,1:3),'vertices',vlfs , ...
%         'facevertexcdata' , hlfs, ...
%         'facecolor','interp', ...
%         'edgecolor','none') ;
%     hold on; axis image off;
%     title(['MESH-SIZE: KIND=DELAUNAY, |TRIA|=', ...
%         num2str(size(tlfs,1))]) ;
 
%---------------------------------------------- do mesh-gen.
    hfun = @trihfn2;
   
   [vert,etri, ...
    tria,tnum] = refine2(node,edge,[],[],hfun , ...
                    vlfs,tlfs,slfs,hlfs);
    
%     figure;
%     patch('faces',tria(:,1:3),'vertices',vert, ...
%         'facecolor','w', ...
%         'edgecolor',[.2,.2,.2]) ;
%     hold on; axis image off;
%     patch('faces',edge(:,1:2),'vertices',node, ...
%         'facecolor','w', ...
%         'edgecolor',[.1,.1,.1], ...
%         'linewidth',1.5) ;
%     title(['TRIA-MESH: KIND=DELFRONT, |TRIA|=', ...
%         num2str(size(tria,1))]) ;
%     
%     drawnow;
    
%     set(figure(1),'units','normalized', ...
%         'position',[.05,.50,.30,.35]) ;
%     set(figure(2),'units','normalized', ...
%         'position',[.35,.50,.30,.35]) ;
    
end






