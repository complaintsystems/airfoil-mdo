function [Coord_FEA, dL_FEA, pan_span_FEA,cl,xp,yp] = Aerodynamics(alpha, d_beta)


addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Sequential/Aerodynamics')


% alpha = 1;
% d_beta = zeros(1,340);

[cl,xp,yp,dL,XB,YB,pan_span] = Potential_Solver_CDM(alpha,d_beta);

Coord = [XB YB];

pan_span_low = pan_span(1:(length(xp)/2+1),1);
pan_span_up = pan_span((length(xp)/2)+1:end,1);

dL_low = dL(1:(length(xp)/2+1),1);
dL_up = dL((length(xp)/2)+1:end,1);

X_low = Coord(1:(length(xp)/2+1),1);
Y_low = Coord(1:(length(xp)/2+1),2);


X_up = Coord((length(xp)/2)+1:end,1);
Y_up = Coord((length(xp)/2)+1:end,2);


Coord_FEA = [vertcat(X_up,X_low) vertcat(Y_up,Y_low)];

dL_FEA = [vertcat(dL_up,dL_low)];

pan_span_FEA = [vertcat(pan_span_up,pan_span_low)];

pan_span_FEA((length(xp)/2)+1,:) = [];

dL_FEA((length(xp)/2)+1,:) = [];

Coord_FEA((length(xp)/2)+1,:) = [];

end

