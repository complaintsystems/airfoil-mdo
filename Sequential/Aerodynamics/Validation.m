clear all
close all
clc

%% Define PARSEC variables for NACA0012

p=[0.0146,0.3025,0.06,-0.4928,0.3016,0.06,-0.4848,-0.0039,0.0101, -2.7791,9.2496];

[cl,cd,cmac,xp,yp,cp,cpx,cpy,XB,YB] = Potential_Solver(p,10);

%% Define experiment values ( NACA 0012 turbulence validation website )

[X,Cp] = Expt_AOA10();

figure


sz = 40;
scatter(X,Cp,sz,'MarkerEdgeColor',[0 .5 .5],...
              'MarkerFaceColor',[0 .7 .7],...
              'LineWidth',1.5)
xlabel ('X/C', 'FontWeight', 'Bold', 'FontSize', 18)
ylabel ('CP', 'FontWeight', 'Bold', 'FontSize', 18)
hold on


plot(xp,cp, 'LineWidth', 2)
xlabel ('X/C', 'FontWeight', 'Bold', 'FontSize', 18)
ylabel ('CP', 'FontWeight', 'Bold', 'FontSize', 18)
grid minor
hold off


