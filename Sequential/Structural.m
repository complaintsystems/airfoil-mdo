function [d_beta] = Structural(Coord_FEA, dL_FEA, pan_span_FEA,xp,yp)

addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Sequential/Structural/poly-data')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Sequential/Structural/')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Sequential/Structural/Topology_optimization')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Sequential/Structural/Topology_optimization/functions')
addpath('/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Aero-Structural/Plot')

%% COORDINATE FILE DESCRIPTION

%{

Coord_FEA goes clockwise : Starting from LE towards TE and then downward
                           towards LE

Coord_panel_ctr_FEA_comp goes clockwise : Starting from LE towards TE and then downward
                              towards LE, contains x & y coordinates with
                              cpx and cpy

%}

mesh_gen(Coord_FEA(:,1),Coord_FEA(:,2));

%movefile ('airfoil.txt', 'C:\Users\Prateek\Documents\Repo\Integrated_Airfoil_Design\Aero-Structural\Structural\MESH')
movefile ('airfoil.txt', '/Users/prateekranjan/Git_Repos/Integrated_Airfoil_Design/Sequential/Structural/MESH')
[tri, vert] = FEA_MESH;


%% Aerodynamic and structural node alignment test

if Coord_FEA(1:end-1,1)-vert(1:length(Coord_FEA)-1,1) == 0
    msg = "Aerodynamic nodes and structural nodes are aligned"
end


%% Define fixed nodes

%-------------------------------SPAR NODES--------------------------------%
%%

[cav_x_1, cav_y_1, cav_z_1] = spar_1();
[cav_x_2, cav_y_2, cav_z_2] = spar_2();


fixed_nodes =(length(Coord_FEA):length(Coord_FEA)+length(cav_x_1)+length(cav_x_2)-1)';


fixed_vert = zeros(length(fixed_nodes),2);

fixed_vert(:,1) = vert(fixed_nodes,1);
fixed_vert(:,2) = vert(fixed_nodes,2);

load_nodes = [1:size(Coord_FEA,1)];


%% %%%%%%%%%%%%%%%%%%%%%%%%%%5Define load vertices



 load_nodes = 1:length(Coord_FEA)-1;
%load_nodes = 1:length(Coord_FEA)-1;   % "-1" because of LE node (0,0) removal


load_nodes = load_nodes';

%% Assigning loads to load vertices


load_vert_x = vert((load_nodes),1);
load_vert_y = vert((load_nodes),2);



%% Compute local reference coordinates at load_point

% Re-structure xp yp

xp_low = xp(1:(length(xp)/2+1),1);
yp_low = yp(1:(length(yp)/2+1),1);

xp_up = xp((length(xp)/2)+1:end,1);
yp_up = yp((length(xp)/2)+1:end,1);

xp_rst = vertcat(xp_up, xp_low);
yp_rst = vertcat(yp_up, yp_low);

xp_rst((length(xp)/2)+1,:) = [];
yp_rst((length(xp)/2)+1,:) = [];
 


load_vert = [load_vert_x load_vert_y  load_nodes(:,1)];




% make BC compatible for top opt sub-routine : 

BC = fixed_nodes';

% figure
% trimesh(tri,vert(:,1),vert(:,2))
% axis equal
% xlabel 'X/C'
% ylabel 'Y/C'
% hold on
% sz = 40;
% scatter(fixed_vert(:,1), fixed_vert(:,2),'MarkerEdgeColor',[1 0 0],...
%               'MarkerFaceColor',[1 0 0],...
%               'LineWidth',1.5)
%           axis equal 
% hold on
% 
% scatter(load_vert(:,1),load_vert(:,2),'MarkerEdgeColor',[0 .5 .5],...
%               'MarkerFaceColor',[0 1 0],...
%               'LineWidth',1.5)
% hold off


%% Apply weight to each cpx


% T2 :: Transformation matrix that transfers the load from aerodynamic mesh
% to structural nodes ---> Fx_node,F_y_node = N.* Cp1x,Cp1y

% Create matrix T2 


T2  =zeros(2*length(load_vert),2*length(load_vert));

% Populate diagonal elements

for i = 1:length(T2)
        T2(i,i) = 0.5;
end
 

V2 = diag(ones(1,length(T2)-2),-2);


T2 = T2 + V2;

T2(1,2*length(load_vert)-1) = 0.5; % Accounts for double x forces on LE Node by last panel  
T2(2,2*length(load_vert)) = 0.5;   % Accounts for double y forces on LE Node by last panel  



Load = zeros(2*length(dL_FEA),1);

Load(1:2:end) = 0;
Load(2:2:end) = dL_FEA;


F_ext= T2* Load;

Fx = F_ext(1:2:end);
Fy = F_ext(2:2:end);



load_vert(:,4) = Fx;
load_vert(:,5) = -1.*Fy;

%% Finite Element Analysis and Topology Optimization

[U] = FEA_tutorial(tri,vert,BC,load_vert);

%% Transformating matrix

T1 = zeros(length(dL_FEA),2*length(dL_FEA));
%T1 = zeros(4,8);

for i = 1:size(T1,1)
    for j =2*i
        T1(i,j) = 0.5;
    end
end
 
for i = 1:size(T1,1)
    idx(i,1) = find(T1(i,:)==0.5);
end
  
idx(1) = [];


for i = 1:size(T1,1)-1
    T1(i,idx(i)) = 0.5;
end


T1(size(T1,1),2) = 0.5;



U_OML = U(1:2*length(dL_FEA),1);

B = T1*U_OML;

%d_beta = B./pan_span_FEA;

d_beta = B;

d_beta = d_beta';


%% Perform post-processing 

% Extract Ux and Uy components

U = U';

Ux = U(1:2:end);          
Uy = U(2:2:end); 

Ux = Ux';
Uy = Uy';


deform_x = Ux(:,1) + vert(:,1);
deform_y = Uy(:,1) + vert(:,2);


D = [deform_x deform_y];

%% Post Processing :: Displacement

figure
subplot(2,1,1)
plot(D(1:length(Coord_FEA)-1,1),D(1:length(Coord_FEA)-1,2), 'lineWidth',2)
axis equal 
hold on
plot(vert(1:length(Coord_FEA)-1,1),vert(1:length(Coord_FEA)-1,2), 'lineWidth',2)
grid minor
hold off

subplot(2,1,2)
plot(D(1:length(Coord_FEA)-1,1),D(1:length(Coord_FEA)-1,2), 'lineWidth',2)
axis equal 
hold on
plot(vert(1:length(Coord_FEA)-1,1),vert(1:length(Coord_FEA)-1,2), 'lineWidth',2)
grid minor
hold off
axis([0.35 1 -0.1 0.1]);
legend('Deformed', 'Original')


end

