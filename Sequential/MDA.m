clear all 
close all 
clc


alpha = 1;
d_beta = zeros(1,340);


%kkttol  = 1e-3;                               % Convergence tolerance
%kktnorm = kkttol + 1;
%outeriter  = 0;

%while outeriter < 20
 %   outeriter = outeriter + 1;
    
    
[Coord_FEA, dL_FEA, pan_span_FEA,cl,xp,yp] =  Aerodynamics(alpha, d_beta);


[d_beta] = Structural(Coord_FEA, dL_FEA, pan_span_FEA,xp,yp);



   disp([' ************************************* ']);
   disp([' outiter: ' sprintf('%4i', outeriter) ' cl: ' sprintf('%6.4e', cl)]);
   disp([' ************************************* ']);

%



