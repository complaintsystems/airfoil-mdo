function [F,G] = objectiveF(p)
    Re = 3000000;
    alpha  = 2;
    iter = 3000;
    increment = 0.1;
    wl = 0.0004;
    wd = 0.0006;
    cl_star = 0.3;
    cd_star = 0.004;
    
    
    [name,X,Y] = ffd_opt(p,'airfoil.txt');
    [CL,CD,CM] = callxfoil(name,alpha,iter,Re);
    F = wl*(1-CL/cl_star)^2+wd*(1-CD/cd_star)^2;
    [dcl,dcd,dcm] = gradient(Re,alpha,iter,p,increment);
    G = zeros(1,length(p));
    for i = 1:length(p)
    G(i) = 2*wl*(1-CL/cl_star)/(-cl_star)*dcl(i)...
        +2*wd*(1-CD/cd_star)/(-cd_star)*dcd(i);
    end
end