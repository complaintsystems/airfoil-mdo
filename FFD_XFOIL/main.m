
parameters=[0.1*ones(1,9),-0.1*ones(1,8)];
Re = 3000000;
alpha  = 2;
iter = 1500;
UB = [0.3*ones(1,9),0.05*ones(1,8)];
LB = [0*ones(1,9),-0.2*ones(1,8)];
options = optimoptions(@fmincon, ...
    'Display','iter','Algorithm','active-set','SpecifyObjectiveGradient',true,...
    'PlotFcn', {@optimplotfval},'FunctionTolerance',1e-5);

[xopt,fopt] = fmincon('objectiveF',parameters,[],[],[],[],LB,UB,'nlcon',options);






