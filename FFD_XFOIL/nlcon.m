function [c,ceq] = nlcon(p)

%define nonlinear constraints in the fasion that:
%c<=0;
%ceq==0;
p = [0.1,p(1:9),-0.1,p(10:17),-0.1-(0.1-p(9))];
for i = 1:(length(p)/2-1)
c(i) = -(p(i)-p(i+10))+0.15;

end
ceq = [];
end