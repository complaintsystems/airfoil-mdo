function[dcl,dcd,dcm] = gradient(Re,alpha,iter,p,increment)
    dcl = zeros(1,17);
    dcd = zeros(1,17);
    dcm = zeros(1,17);
    
    [nameb,Xb,Yb] = ffd_opt(p,'airfoil.txt');
    [cl_b,cd_b,cm_b] = callxfoil(nameb,alpha,iter,Re);
    named = strings([1,length(p)]);
    for i = 1:17
        named(i) = "airfoil"+string(i)+".txt";
    end
    for i = 1:17
        param = p;
        param(i) = p(i)+increment*(p(i)+0.05-0.1);
        [named(i),Xd,Yd] = ffd_opt(param,named(i));
        [cl_t,cd_t,cm_t] = callxfoil(named(i),alpha,iter,Re);
        dcl(i) = (cl_t-cl_b)/(increment*(p(i)+0.05-0.1));
        dcd(i) = (cd_t-cd_b)/(increment*(p(i)+0.05-0.1));
        dcm(i) = (cm_t-cm_b)/(increment*(p(i)+0.05-0.1));
        pause(0.05)
    end

end
